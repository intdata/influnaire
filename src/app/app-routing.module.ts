import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from "./shared/guard/influencer/auth.guard";
import { AuthGuardBrandOwner } from "./shared/guard/brand-owner/auth.guard";

const routes: Routes = [
{
    path: '',
    loadChildren: './frontend/frontend.module#FrontendModule',
},
{
    path: 'brand-owner',
    loadChildren: './brand-owner/brand-owner.module#BrandOwnerModule',
    canActivate: [AuthGuardBrandOwner]
},
{
    path: 'influencers',
    loadChildren: './influencers/influencers.module#InfluencersModule',
    canActivate: [AuthGuard]
},
{ path: '', redirectTo: '/comming-soon', pathMatch: 'full'},
{ path: '**', redirectTo: '/', pathMatch: 'full'}
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            routes, 
            {
                scrollPositionRestoration: 'enabled', 
                //onSameUrlNavigation: 'reload'
                //useHash: true
            }
        )
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }