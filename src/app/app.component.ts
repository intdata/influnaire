import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { NgwWowService } from 'ngx-wow';
import { Subscription } from 'rxjs/Subscription';
import { filter, map } from 'rxjs/operators';
import { SiteSettingsService } from './shared/settings.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  // keep refs to subscription to be abble to unsubscribe later
  // (NOTE: unless you want to be notified when an item is revealed by WOW,
  //  you absolutely don't need this line and related, for the library to work
  // only the call to `this.wowService.init()` at some point is necessary
  private wowSubscription: Subscription;
  countStart: any;
  settingsData:any;
  constructor(
    private router: Router,
    private wowService: NgwWowService,
    private settingsService: SiteSettingsService
    ) {
    // Reload WoW animations when done navigating to page,
    // but you are free to call it whenever/wherever you like
    router.events.subscribe((val) => {
      if(val instanceof NavigationEnd){
        this.wowService.init();
      }
    });

    this.siteSettings()

  }

  ngOnInit() {
    // you can subscribe to WOW observable to react when an element is revealed
    this.wowSubscription = this.wowService.itemRevealed$.subscribe(
      (item: HTMLElement) => {
        this.countStart = 10;
        // do whatever you want with revealed element
      });
  }

  ngOnDestroy() {
    // unsubscribe (if necessary) to WOW observable to prevent memory leaks
    this.wowSubscription.unsubscribe();
  }

  /**
   * Site settings for the gobal settings from the admin
   */
  siteSettings(){
    this.settingsService.getSettings().subscribe(
      (response: any) => {
          
          if (response.responseCode === 200) {
            var resp = response.data
            var settingData = {}
            if(resp.length){
              for(let i=0; i<resp.length; i++){
                settingData[resp[i].setting_label] = resp[i].setting_value
              }
            }
            this.settingsData = settingData;         
          } else {
            console.log('get page error: ', response.responseDetails);               
          }
      },
      error => {
          console.log('get page error: ', error);
      }
    );
  }
  
  getSettingData(label=''){
    
    if(label){
      return this.settingsData[label]
    }
  }

}
