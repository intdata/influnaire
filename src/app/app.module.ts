import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule,APP_INITIALIZER } from '@angular/core';
import { Constant } from './constant';
import { HttpModule } from '@angular/http';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgwWowModule } from 'ngx-wow';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { AuthGuard } from "./shared/guard/influencer/auth.guard";
import { AuthGuardLogin } from "./shared/guard/common/auth.guard.login";
import { AuthService } from './influencers/services/auth.service';
import { AuthServiceBrandOwner } from './brand-owner/services/auth.service';
import { CryptoProvider } from './shared/crytoEncrypt.service';

import { DeactivateGuard } from './shared/guard/brand-owner/deactive.guard';

import { TabsModule } from 'ngx-bootstrap/tabs';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';

import { SharedVariable } from './shared/shared-variable.service';
import { SiteSettingsService } from './shared/settings.service';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpModule,
    NgwWowModule,
    NgbModule.forRoot(),
    ToastrModule.forRoot(),
    TabsModule.forRoot(),
    AccordionModule.forRoot(),
    ModalModule.forRoot(),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: [
      TabsModule,
      AccordionModule,
      ToastrModule,
      TooltipModule,
      ModalModule,
      TranslateModule
  ],
  providers: [
    Constant,
    AuthGuard,
    AuthGuardLogin,
    AuthService   ,
    AuthServiceBrandOwner,
    DeactivateGuard,
    CryptoProvider,
    SharedVariable,
    SiteSettingsService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }