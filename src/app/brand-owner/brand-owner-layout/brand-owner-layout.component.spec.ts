import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandOwnerLayoutComponent } from './brand-owner-layout.component';

describe('BrandOwnerLayoutComponent', () => {
  let component: BrandOwnerLayoutComponent;
  let fixture: ComponentFixture<BrandOwnerLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrandOwnerLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrandOwnerLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
