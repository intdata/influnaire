import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrandOwnerLayoutComponent } from './brand-owner-layout/brand-owner-layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { InfluencerSearchComponent } from './influencer-search/influencer-search.component';
import { InfluencerSavedComponent } from './influencer-saved/influencer-saved.component';
import { CampaignComponent } from './campaign/campaign.component';
import { CampaignManagementComponent } from './campaign-management/campaign-management.component';
import { CampaignManagementDetailsComponent } from './campaign-management-details/campaign-management-details.component';
import { CampaignListComponent } from './campaign-list/campaign-list.component';
import { MessagesComponent } from './messages/messages.component';
import { MessageDetailsComponent } from './message-details/message-details.component';
import { HelpComponent } from './help/help.component';
import { CompareInfluencersComponent } from './compare-influencers/compare-influencers.component';
import { ProfileComponent } from './profile/profile.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { CampaignDetailsComponent } from './campaign-details/campaign-details.component';
import { LogoutComponent } from './logout/logout.component';

import { DeactivateGuard } from './../shared/guard/brand-owner/deactive.guard';


const routes: Routes = [
  { path: '', component: BrandOwnerLayoutComponent, 
    children:[
      { path: '', component: DashboardComponent},
      { path: 'logout', component: LogoutComponent},
      { path: 'influencer-search', component: InfluencerSearchComponent},
      { path: 'influencer-saved', component: InfluencerSavedComponent},
      { path: 'campaign', component: CampaignComponent},
      { path: 'campaign-management/:step', component: CampaignManagementComponent, canDeactivate: [DeactivateGuard]},
      { path: 'campaign-management/:step/:campaignId', component: CampaignManagementComponent, canDeactivate: [DeactivateGuard]},
      { path: 'campaign-list', component: CampaignListComponent},
      { path: 'messages', component: MessagesComponent},
      { path: 'message-detail/:campaign/:influncer/:brandowner', component: MessageDetailsComponent},
      { path: 'help', component: HelpComponent},
      { path: 'compare/:length', component: CompareInfluencersComponent},
      { path: 'profile', component: ProfileComponent },
      { path: 'notifications', component: NotificationsComponent },
      { path: 'campaign-details/:id', component: CampaignDetailsComponent },
      { path: 'campaign-management-details/:id', component: CampaignManagementDetailsComponent },
      
    ]

  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BrandOwnerRoutingModule { }
