import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HttpClient } from "@angular/common/http";

import { BrandOwnerRoutingModule } from './brand-owner-routing.module';
import { BrandOwnerLayoutComponent } from './brand-owner-layout/brand-owner-layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import { InfluencerSearchComponent } from './influencer-search/influencer-search.component';
import { InfluencerSavedComponent } from './influencer-saved/influencer-saved.component';
import { CampaignComponent } from './campaign/campaign.component';
import { CampaignManagementComponent } from './campaign-management/campaign-management.component';
import { MessagesComponent } from './messages/messages.component';
import { MessageDetailsComponent } from './message-details/message-details.component';
import { HelpComponent } from './help/help.component';
import { CompareInfluencersComponent } from './compare-influencers/compare-influencers.component';
import { CampaignListComponent } from './campaign-list/campaign-list.component';
import { NgbModule, NgbTabsetModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { ProfileComponent } from './profile/profile.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { CampaignManagementDetailsComponent } from './campaign-management-details/campaign-management-details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { Observable } from 'rxjs';
// import { map } from 'rxjs/operators';
import { ChartsModule } from 'ng2-charts';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { SharedPipesModule } from "./pipes/shared-pipes.module";
import { CampaignDetailsComponent } from './campaign-details/campaign-details.component';
import { LogoutComponent } from './logout/logout.component';
import { FlatpickrModule } from 'angularx-flatpickr';

// services
import { UserService } from "./services/user.service";
import { MessageService } from "./services/message.service";
import { CountryService } from "./services/country.service";
import { IndustryService } from "./services/industry.service";
import { HelpCenterService } from "./services/help-center.service";
import { NotificationsService } from "./services/notifications.service";
import { PlatformService } from "./services/platform.service";
import { ProposalService } from "./services/proposal.service";
import { CampaignAssign } from "./services/campaign-assign.service";
import { BrandService } from "./services/brand.service";
import { SettingService } from "./services/setting.service";
import { CurrencyService } from "./services/currency.service";
import { SharedService } from "./services/shared.service";
import { CampaignAnalysisService } from "./services/campaign-analysis.service";
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/lang/brand-owner/', '.json');
}

import { CampaignService } from "./services/campaign.service";
import { InfluncerService } from "./services/influncer.service";
import {NgxPaginationModule} from 'ngx-pagination';

import { NgxPayPalModule } from 'ngx-paypal';

@NgModule({
  imports: [
    CommonModule,
    BrandOwnerRoutingModule,
    NgbTabsetModule,
    NgbDropdownModule,
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    HttpClientModule,
    NgxPaginationModule,
    NgbModule,
    AccordionModule.forRoot(),
    SharedPipesModule,
    NgxPayPalModule,
    FlatpickrModule.forRoot(),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    BrandOwnerLayoutComponent,
    DashboardComponent,
    SidebarComponent,
    HeaderComponent,
    InfluencerSearchComponent,
    InfluencerSavedComponent,
    CampaignComponent,
    CampaignManagementComponent,
    CampaignManagementDetailsComponent,
    MessagesComponent,
    MessageDetailsComponent,
    HelpComponent,
    CompareInfluencersComponent,
    CampaignListComponent,
    ProfileComponent,
    NotificationsComponent,
    CampaignDetailsComponent,
    LogoutComponent,
    
  ],
  providers:[
    UserService,
    CountryService,
    IndustryService,
    HelpCenterService,
    CampaignService,
    InfluncerService,
    MessageService,
    NotificationsService,
    PlatformService,
    CampaignAssign,
    ProposalService,
    BrandService,
    SettingService,
    CurrencyService,
    SharedService,
    CampaignAnalysisService
  ]

})
export class BrandOwnerModule { }
