import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { SingleDataSet, Label, Color, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import { CampaignAnalysisService } from "./../services/campaign-analysis.service";
import { CampaignService } from "./../services/campaign.service";
import { Router, NavigationEnd, ActivatedRoute, RoutesRecognized } from '@angular/router';

@Component({
  selector: 'app-campaign-details',
  templateUrl: './campaign-details.component.html',
  styleUrls: ['./campaign-details.component.scss']
})
export class CampaignDetailsComponent implements OnInit {

  investerList: any = [];
  followerList: any = [];
  likeList:any = [];
  soacialList:any = [];

  @ViewChild('doughnutCanvas') doughnutCanvas:ElementRef
  @ViewChild('doughnutCanvas1') doughnutCanvas1:ElementRef
  @ViewChild('doughnutCanvas2') doughnutCanvas2:ElementRef

  campaignId:number
  public campaign:any =[];
  public totalInfluncer = 0

  public influncerImage:any;
  // doughnut
  public doughnutDataColors: any[];
  public doughnutChartType: ChartType = 'doughnut';
  public doughnutChartLegend = false;
  public doughnutChartPlugins = [];
  public amountInvested = 0;
  public paid_pct =0;
  public pending_pct =0;
  public invested:any;

  public doughnutChartOptions: ChartOptions = {};
  public doughnutChartLabels: Label[];
  public doughnutChartData: SingleDataSet;

  public doughnutChartOptions1: ChartOptions = {};
  public doughnutChartLabels1: Label[];
  public doughnutChartData1: SingleDataSet;

  public doughnutChartOptions2: ChartOptions = {};
  public doughnutChartLabels2: Label[];
  public doughnutChartData2: SingleDataSet;

  //Bar chart for like & comment

  // area line chart
  public lineChartData: ChartDataSets[];
  public lineChartData1: ChartDataSets[];
  public lineChartLabels: Label[];
  public lineChartOptions: (ChartOptions) = {
    responsive: true,
    tooltips:{
      enabled: true
    },
    elements: {
        line: {
            tension: 0
        }
    },
    scales:{
      yAxes: [{
        gridLines: {
          display: false,
        },
        ticks: {
          display: true,
          beginAtZero: false
        }
      }],
    }
  };
  public lineChartColors: Color[] = [
    {
      borderColor: '#8dc964',
      backgroundColor: 'rgba(177, 239, 158,0.3)',
    },
  ];
  public lineChartColors1: Color[] = [
    {
      borderColor: '#f79636',
      backgroundColor: 'rgba(252, 187, 174,0.3)',
    },
  ];
  public lineChartLegend = false;
  public lineChartType = 'line';
  public lineChartPlugins = [];

  // bar chart
  public barChartOptions: ChartOptions = {
    responsive: true,
    tooltips:{
      enabled: true
    },
    scales:{
      yAxes: [{
        gridLines: {
          display: false,
        },
        ticks: {
          display: true,
          beginAtZero: false
        }
      }],
    }
  };
  public barDataColors: Color[];
  public barChartType: ChartType = 'bar';
  public barChartLegend = false;
  public barChartPlugins = [];
  public barChartData: ChartDataSets[];
  public barChartLabels: Label[];

  constructor(
    private campaignAnalysisService:CampaignAnalysisService,
    private CampaignService:CampaignService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {

    this.route.params.subscribe( params => {
      this.campaignId = params['id'];
      
    } );
    // doughnut
    let me = this;
    // doughnut chart for Individual Return
    var ctx1 = this.doughnutCanvas1.nativeElement.getContext("2d");
    this.doughnutChartData1 = [20, 80];
    this.doughnutChartLabels1 = ['Amount Invested', 'Return'];
    this.doughnutChartOptions1 = {
      responsive: true,
      rotation: Math.PI / 2,
      cutoutPercentage: 95,
      tooltips:{
        enabled:false
      },
      hover:{
        animationDuration: 0
      },
      legend: {
        position: 'left',
        labels: {
            //fontColor: "white",
            boxWidth: 7,
            padding: 5,
            usePointStyle: true
        }
      }
    };
    this.doughnutChartOptions1.animation = { 
      //duration: 0,
      onComplete: function() {
        me.centerLabel(ctx1, "$120K", "ON $80K", "#F29D45", "#45a5e0", me.doughnutCanvas1);
      }
    }

    // line chart labels
    this.lineChartLabels = ['Week 1', 'Week 2', 'Week 3', 'Week 4'];

    // line chart for followers reached
    this.lineChartData = [
      { data: [168, 176, 228, 260], label: 'Followers Reached' },
    ];
    
    // line chart for like & comments
    this.lineChartData1 = [
      { data: [57, 123, 210, 234], label: 'Likes & Comments' },
    ];

    
    this.followerList=[
      {
        img: 'infl-search-pic1.jpg',
        name: 'John S',
        date: '01st Apr',
        amount: '11'
      },
      {
        img: 'infl-search-pic2.jpg',
        name: 'Jasmin M',
        date: '02nd Apr',
        amount: '3'
      },
      {
        img: 'infl-search-pic3.jpg',
        name: 'Amelia G',
        date: '02nd Apr',
        amount: '33'
      },
      {
        img: 'infl-search-pic4.jpg',
        name: 'John S',
        date: '01st Apr',
        amount: '44'
      },
      {
        img: 'infl-search-pic5.jpg',
        name: 'John S',
        date: '01st Apr',
        amount: '10'
      }
    ],
    this.likeList=[
      {
        img: 'infl-search-pic1.jpg',
        name: 'John S',
        date: '01st Apr',
        like: '11',
        comment:'10'
      },
      {
        img: 'infl-search-pic2.jpg',
        name: 'John S',
        date: '01st Apr',
        like: '02',
        comment:'05'
      },
      {
        img: 'infl-search-pic3.jpg',
        name: 'John S',
        date: '01st Apr',
        like: '11',
        comment:'10'
      },
      {
        img: 'infl-search-pic4.jpg',
        name: 'John S',
        date: '01st Apr',
        like: '21',
        comment:'15'
      },
      {
        img: 'infl-search-pic4.jpg',
        name: 'John S',
        date: '01st Apr',
        like: '11',
        comment:'10'
      }
    ]
    /**
     * Api Calls
     */
    this.getCampaignById()
    this.getAnalysis()
    this.getPlatformComparisonAnalysis()

    // doughnut chart for Individual Media Value
        
    
  }

  /**
   * Get Campaign Influncer Status
   */
  getInfluncerStatus(){
    
  }
  /**
   * Method to add lebel at the 
   * middle of the chart
   * @param ctx 
   */
  centerLabel(ctx, middleText1, middleText2, textColor1, textColor2, doughnutCanvas) {
       var width = doughnutCanvas.nativeElement.clientWidth,
           height = doughnutCanvas.nativeElement.clientHeight;

       var fontSize = (height / 50).toFixed(2);
       ctx.font = fontSize + "em Verdana";
       ctx.textBaseline = "middle";
       ctx.fillStyle = textColor1;

       var text = middleText1,
           textX = Math.round((width - ctx.measureText(text).width) / 2),
           textY = height / 2 - 10;

       ctx.fillText(text, textX, textY, 200);

       var fontSize = (height / 110).toFixed(2);
       ctx.font = fontSize + "em Verdana";
       ctx.textBaseline = "middle";
       ctx.fillStyle = textColor2;

       var text = middleText2,
           textX = Math.round((width - ctx.measureText(text).width) / 2),
           textY = height / 2 + 15;

       ctx.fillText(text, textX, textY, 200);
       ctx.restore();
   }

/**
 * Get Campaign by id
 */
getCampaignById(){
  this.CampaignService.getDataById(this.campaignId).subscribe(
    (response: any) => {
        
        if (response.responseCode === 200) {
          this.campaign = response.data
          this.likeCommentCountGraph(response.data)
          this.totalInfluncer = response.data.CampaignAssigns.length       
        } else {
            console.log('get page error: ', response.responseDetails);
             
        }
    },
    error => {
        console.log('get page error: ', error);
    }
  );
}
/**
 * Get all campaigns and associaed influncers data
 */
getAnalysis(){
  this.doughnutDataColors = [{ backgroundColor: ['rgba(30, 169, 224, 0.8)', 'rgba(255,165,0,0.9)']}];
  this.doughnutChartData = [0, 0];
  this.doughnutChartData2 = [0, 0];
  this.doughnutChartLabels2 = ['Amount Invested', 'Media Value'];
  this.campaignAnalysisService.getPaymentAnalysis(this.campaignId).subscribe(
    (response: any) => {
        
        if (response.responseCode === 200) {
          var payment_data =  response.data;
          var paid = 0
          var pending = 0;
          var total =0;
          for(let i=0; i<payment_data.length; i++){
              if(payment_data[i].BrandOwnerAdminTransaction && payment_data[i].BrandOwnerAdminTransaction.InfluencerAdminTransaction){
                paid += parseFloat(payment_data[i].BrandOwnerAdminTransaction.InfluencerAdminTransaction.total_amount)
                total += parseFloat(payment_data[i].BrandOwnerAdminTransaction.InfluencerAdminTransaction.total_amount)
                var influncerData = {}
                var influncer = payment_data[i].BrandOwnerAdminTransaction.InfluencerAdminTransaction.Influencer;
                influncerData['img'] = response.influencerImagePath + influncer.picture
                influncerData['name'] = influncer.first_name +' '+influncer.last_name
                influncerData['date'] = payment_data[i].BrandOwnerAdminTransaction.InfluencerAdminTransaction.manual_payout_on
                influncerData['amount'] = payment_data[i].BrandOwnerAdminTransaction.InfluencerAdminTransaction.total_amount
                this.investerList.push(influncerData) ;
              }else{
                pending += parseFloat(payment_data[i].BrandOwnerAdminTransaction.total_amount)
                total += parseFloat(payment_data[i].BrandOwnerAdminTransaction.total_amount)
              }
          }
          //For total media value graph
          let me = this;
          this.amountInvested = paid          
          this.doughnutChartOptions2 = {
            responsive: true,
            rotation: Math.PI / 2,
            cutoutPercentage: 95,
            tooltips:{
              enabled:false
            },
            hover:{
              animationDuration: 0
            },
            legend: {
              position: 'left',
              labels: {
                  //fontColor: "white",
                  boxWidth: 7,
                  padding: 5,
                  usePointStyle: true
              }
            }
          };
          var ctx2 = this.doughnutCanvas2.nativeElement.getContext("2d");
          var paid_pct2 = Math.round((paid/parseFloat(paid+this.campaign.total_amount_invested))*100) 
          var pending_pct2 = Math.round((this.campaign.total_amount_invested/parseFloat(paid+this.campaign.total_amount_invested))*100)
          this.doughnutChartData2 = [paid_pct2, pending_pct2];
          this.doughnutChartOptions2.animation = { 
            //duration: 0,
            onComplete: function() {
              me.centerLabel(ctx2, "$"+paid, "ON $"+me.campaign.total_amount_invested, "#F29D45", "#45a5e0", me.doughnutCanvas2);
            }
          }
          
          //analysis first chart
          var paid_pct = Math.round((paid/total)*100) 
          var pending_pct = Math.round((pending/total)*100)
          
          this.doughnutChartOptions = {
            responsive: true,
            rotation: Math.PI / 2,
            cutoutPercentage: 95,
            tooltips:{
              enabled:false
            },
            hover:{
              animationDuration: 0
            },
            legend: {
              position: 'left',
              labels: {
                  //fontColor: "white",
                  boxWidth: 7,
                  padding: 5,
                  usePointStyle: true
              }
            }
          };

          // doughnut chart for Total Amount Invested
          this.paid_pct = paid_pct
          this.pending_pct = pending_pct
          var ctx = this.doughnutCanvas.nativeElement.getContext("2d");
          this.doughnutChartData = [paid_pct, pending_pct];
          this.doughnutChartLabels = ['%'+paid_pct, '% Of Amount Invested'];
          this.doughnutChartOptions.animation = { 
            //duration: 0,
            onComplete: function() {
              me.centerLabel(ctx, paid_pct+"%", "OF 100%", "#45a5e0", "#F29D45", me.doughnutCanvas);
            }
          }        
        } else {
            console.log('get page error: ', response.responseDetails);
             
        }
    },
    error => {
        console.log('get page error: ', error);
    }
  );
}

/**
 * Get Influncers follower analysis
 */
getFollowerAnalysis(){
  this.campaignAnalysisService.getFollowerAnalysis(this.campaignId).subscribe(
    (response: any) => {
        
        if (response.responseCode === 200) {
          console.log(response.data)         
        } else {
            console.log('get page error: ', response.responseDetails);
             
        }
    },
    error => {
        console.log('get page error: ', error);
    }
  );
}

/**
 * Get Influncers like and comment analysis
 */
getLikeCommentAnalysis(){
  this.campaignAnalysisService.getLikeCommentAnalysis(this.campaignId).subscribe(
    (response: any) => {
        
        if (response.responseCode === 200) {
          console.log(response.data)         
        } else {
            console.log('get page error: ', response.responseDetails);
             
        }
    },
    error => {
        console.log('get page error: ', error);
    }
  );
}

/**
 * Platform Comparison Analysis
 */
getPlatformComparisonAnalysis(){

  this.barChartLabels = ['FACEBOOK', 'INSTAGRAM', 'YOUTUBE'];
    this.barChartData = [
      { data: [0, 0, 0], label: 'Platform Comparison' }
    ];
    this.barDataColors = [{ 
        backgroundColor: [
          'rgba(164, 188, 234,0.8)', 
          'rgba(63, 114, 155,0.8)', 
          'rgba(252, 187, 174,0.8)'
        ]
      }];
  this.campaignAnalysisService.getPlatformComparisonAnalysis(this.campaignId).subscribe(
    (response: any) => {
        
        if (response.responseCode === 200) {
          var platformAssign = response.data
          if(platformAssign.length){
            var facebook=0
            var Youtube=0
            var instagram=0

            for(var i = 0; i < platformAssign.length; i++){
              var paltformData = {}
              var influncer = platformAssign[i].Influencer;
              paltformData['img'] = response.influencerImagePath + influncer.picture;
              paltformData['name'] = influncer.first_name+' '+influncer.last_name
              paltformData['social'] = platformAssign[i].Platform.class_name
              this.soacialList.push(paltformData)
              if(platformAssign[i].Platform.class_name == 'facebook'){
                facebook += 1;
              }else if(platformAssign[i].Platform.class_name == 'youtube'){
                Youtube += 1
              }else if(platformAssign[i].Platform.class_name == 'instagram'){
                instagram += 1
              }
            }
            this.barChartData = [
              { data: [facebook, instagram, Youtube], label: 'Platform Comparison' }
            ];
          }       
        } else {
            console.log('get page error: ', response.responseDetails);
             
        }
    },
    error => {
        console.log('get page error: ', error);
    }
  );
}

/**
 * calculate like Count social media wise for bar chart
 */
likeCommentCountGraph(campaign){
  var data = campaign.CampaignAssigns
  var likePlatforms = []
  var socialCntData = {}
  var engagementRate = {}
  if(data.length){
    for(let c=0; c<data.length; c++){
      var platformAssignData = data[c].Influencer.influencerPlatformMaps

      if(platformAssignData.length){
        for (let p=0;p<platformAssignData.length;p++ ){

          if(socialCntData[platformAssignData[p].Platform.name]){ 
            engagementRate[platformAssignData[p].Platform.name] += platformAssignData[p].total_like?parseInt(platformAssignData[p].engagement_ratio):0;           
            socialCntData[platformAssignData[p].Platform.name] += platformAssignData[p].total_like?parseInt(platformAssignData[p].total_like):0;
            socialCntData[platformAssignData[p].Platform.name] += platformAssignData[p].total_comments?parseInt(platformAssignData[p].total_comments):0;
          }else{
            if(likePlatforms.indexOf(platformAssignData[p].Platform.name) == -1){
              likePlatforms.push(platformAssignData[p].Platform.name)
            }
            engagementRate[platformAssignData[p].Platform.name] = platformAssignData[p].total_like?parseInt(platformAssignData[p].engagement_ratio):0;
            socialCntData[platformAssignData[p].Platform.name] = platformAssignData[p].total_like?platformAssignData[p].total_like:0;
            socialCntData[platformAssignData[p].Platform.name] += platformAssignData[p].total_comments?parseInt(platformAssignData[p].total_comments):0;
          }
        }
      }
    }
    console.log(engagementRate)
    console.log(likePlatforms)
    console.log(socialCntData)
  }
  console.log('+++++++++++++++++++++++')
}
 

}
