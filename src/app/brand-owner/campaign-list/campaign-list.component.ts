import { Component, OnInit, TemplateRef, Host } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute, RoutesRecognized } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { CampaignService } from "./../services/campaign.service";
import { PlatformService } from "./../services/platform.service";
import { InfluncerService } from "./../services/influncer.service";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { SharedVariable } from './../../shared/shared-variable.service';
import { AppComponent } from './../../app.component';

@Component({
  selector: 'app-campaign-list',
  templateUrl: './campaign-list.component.html',
  styleUrls: ['./campaign-list.component.scss']
})
export class CampaignListComponent implements OnInit {

  campaigns:any = []
  pastCampaigns:any = []

  campaignConfig: any;
  pastCampaignConfig: any;
  public allPlatforms: any = [];
  public allPaths: any={};
  public modalRef: BsModalRef;
  public influencerImagePath:any;
  public influncerDetail: any ={};
  public campaignProgress:any = {};

  constructor(
    @Host() private app: AppComponent,
    private PlatformService: PlatformService,
    private toastr: ToastrService,
    private CampaignService:CampaignService,
    private InfluncerService: InfluncerService,
    private modalService: BsModalService,
    private sharedVariable: SharedVariable
  ) {}

  ngOnInit() {
    
    this.getCampaign('past')
    this.getCampaign('current')
    this.campaignConfig = {
      id: 'campaigns',
      itemsPerPage: this.app.getSettingData('pagination'),
      currentPage: 1,
      totalItems: this.campaigns.length,
      responsive: "true"
    };
    this.pastCampaignConfig = {
      id: 'pastCampaigns',
      itemsPerPage: this.app.getSettingData('pagination'),
      currentPage: 1,
      totalItems: this.pastCampaigns.length
    };

    this.getplatforms();
    //Receive shared service data
    this.campaignProgress = this.sharedVariable.getCampaingProgress
  }

  campaignPageChanged(event){
    this.campaignConfig.currentPage = event;
  }

  pastCampaignPageChanged(event){
    this.pastCampaignConfig.currentPage = event;
  }
  
  /**
   * Method to get campaign
   * @param type 
   */
  getCampaign(type){
    this.CampaignService.getData(type).subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
              if(type == 'past'){
                this.pastCampaigns = response.data;
              }else{
                this.campaigns = response.data;
              }
              this.allPaths = (response.allPaths)?response.allPaths:{}
              //console.log('Total Campaign'+this.campaigns.length)
              //console.log()
          } else {
              console.log('get page error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get page error: ', error);
      }
    );
  }

   /**
   * Get Platform List
   */
  getplatforms(){
    this.PlatformService.getPlatforms().subscribe(
      (response: any) => {
                  
          if (response.responseCode === 200) {
            this.allPlatforms = response.data;
          } else {
              console.log('Campaign create error: ', response.responseDetails);
          }
      },
      error => {
          console.log('Campaign Create error: ', error);
      }
     );
  }

   /**
   * Open Influncer Detail modal
   */
  influncerDetailModal(influencerDetailsModal: TemplateRef<any>, influncerId){
    console.log(influncerId)
    this.getInfluncerById(influncerId)
    this.openModal(influencerDetailsModal, "customModal modal-xlg");
  }

  /**
   * Get Influncer detail by Id
   */
  getInfluncerById(influncerId){
    this.InfluncerService.getInfluncerById(influncerId).subscribe(
      (response: any) => {
          
          if (response.responseCode === 200) {
            this.influncerDetail = response.data;
            this.influencerImagePath=(response.influencerImagePath)?response.influencerImagePath:''          
          } else {
              console.log('get page error: ', response.responseDetails);
               
          }
      },
      error => {
          console.log('get page error: ', error);
      }
    );
  }

   /**
   * calculate the sum of all social media followers
   */
  getTotalFollowers(platform){
    var sum = 0; 
    for(let i=0; i<platform.length; i++){
      sum += platform[i].followers
    }

    return sum;
  }

  /**
   * Medthod to open modal function
   * @param template
   * @param classR
   */
  openModal(template: TemplateRef<any>, classR: string) {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: "customModal " + classR })
    );
  }

  /**
   * Method to close modal
   */
  closeModal() {
    this.modalService._hideModal(1);
  }

}
