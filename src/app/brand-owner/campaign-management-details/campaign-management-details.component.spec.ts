import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignManagementDetailsComponent } from './campaign-management-details.component';

describe('CampaignManagementDetailsComponent', () => {
  let component: CampaignManagementDetailsComponent;
  let fixture: ComponentFixture<CampaignManagementDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignManagementDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignManagementDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
