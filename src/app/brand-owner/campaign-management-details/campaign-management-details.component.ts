import { Component, OnInit, ElementRef, TemplateRef, ViewChild, AfterViewChecked } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute, RoutesRecognized } from '@angular/router';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl, AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { Constant } from './../../constant';
import { MessageService } from "./../services/message.service";
import { CampaignService } from "./../services/campaign.service";
import { CampaignAssign } from "./../services/campaign-assign.service";
import { InfluncerService } from "./../services/influncer.service";
import { PlatformService } from "./../services/platform.service";
import { ProposalService } from "./../services/proposal.service";
import { SettingService } from "./../services/setting.service";
import { ToastrService } from 'ngx-toastr';


// declare let paypal: any;
import {
  IPayPalConfig,
  ICreateOrderRequest
} from 'ngx-paypal';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-campaign-management-details',
  templateUrl: './campaign-management-details.component.html',
  styleUrls: ['./campaign-management-details.component.scss']
})
export class CampaignManagementDetailsComponent implements OnInit {
  @ViewChild('fileInput') fileInput;
  public progressArr: Array<any> = [];
  public modalRef: BsModalRef;
  public selectedItemObj;
  public selectedInfluncerId: any = '';
  public status: any = '';
  public campaignHistory: any[] = [];
  public proposalData: any = []
  public allPlatforms: any = [];
  public campaign: any = []
  public id: string;
  public assignIds:any=[];
  public influncerMessageData: any = [];
  public messageForm: FormGroup;
  public msubmitted = false;
  public messagefilePath = '';
  public imageFile = ['jpg', 'jpeg', 'png']
  public docFile = ['txt', 'csv', 'xls', 'doc', 'docx']
  public pdfFile = ['pdf']
  public statusList = { 'PENDING': 'Pending', 'ACCEPT': 'Request Accepted', 'REJECT': 'Request Not Accepted', 'SUBMITTED': 'Proposal Received', 'CONFIRM': 'Proposal Accepted', 'REFUSE': 'Proposal Not Accepted', 'CONTENT_SUBMITTED': 'Content Received', 'CONTENT_APPROVED': 'Content Approved', 'PAID': 'Paid' };
  public allPaths: any = {};
  public influncerMessageSlice: any = [];
  public payPalConfig?: IPayPalConfig;
  addScript: boolean = false;
  paypalLoad: boolean = true;
  finalAmount: number = 1;
  //paypal dynamic values
  public totalAmount: number;
  public grossAmount: string;
  public adminCommissionPertg: number = 0;
  public adminCommissionAmt: number = 0;
  public totaladminCommissionAmt: number = 0;
  public paypalChargePertg: number = 0;
  public paypalChargeAmnt: number = 0;
  public influencerAmount: number = 0;
  public assignedInfluencerDtl: any = [];
  public sitesetting;
  public campaignStatus: string;
  public influencerImagePath: any;
  public influncerDetail: any = {};
  public dataArr: any = {};
  public settingsArr: Array<any> = [];
  public showPaypal: boolean = false;
  public showPayout: boolean = false;
  public brandOwnerTransId: number = 0;
  public loading: boolean;
  public transactionId: string;
  public defaultCurrencyId: number;
  public currency: string = '';//default currency
  public currencyFeePertg: number = 0;
  public currencyFeenAmt: number = 0;
  public brandCurrencyId: number;


  constructor(
    private route: ActivatedRoute,
    private CampaignService: CampaignService,
    private CampaignAssign: CampaignAssign,
    private modalService: BsModalService,
    private InfluncerService: InfluncerService,
    private PlatformService: PlatformService,
    private ProposalService: ProposalService,
    private MessageService: MessageService,
    private formBuilder: FormBuilder,
    private SettingService: SettingService,
    private toastr: ToastrService,
    private router: Router,
  ) { }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.id = params['id'];
      console.log(this.getCampaign(this.id));
      //console.log(this.getassignedInfluencer(this.id));

    });
    this.setCommission();
    this.getplatforms();
    //console.log(Date.now());
    //this.initConfig();
    // if (!this.addScript) {
    //   this.addPaypalScript().then(() => {
    //     paypal.Button.render(this.paypalConfig, '#paypal-checkout-btn');
    //     this.paypalLoad = false;
    //   })
    // }

  }

  private initConfig(): void {
    let totalAmountPaypal: number = this.totalAmount;
    let influencerAmountPaypal: number = this.influencerAmount;
    let adminCommissionAmtPaypal: number = this.adminCommissionAmt;
    let paypalChargeAmntPaypal: number = this.paypalChargeAmnt;
    let totaladminCommissionAmt: number = this.totaladminCommissionAmt;
    console.log(this.currency);
    
    this.payPalConfig = {
      currency: this.currency,
      clientId: Constant.clientId,
      createOrderOnClient: (data) => <ICreateOrderRequest>{
        intent: 'CAPTURE',
        purchase_units: [{
          amount: {
            currency_code: this.currency,
            value: totalAmountPaypal.toFixed(2).toString(),
            breakdown: {
              item_total: {
                currency_code: this.currency,
                value: totalAmountPaypal.toFixed(2).toString(),
              }
            }
          },
          items: [
            {
              name: 'Influencer Payment',
              quantity: '1',
              category: 'DIGITAL_GOODS',
              unit_amount: {
                currency_code: this.currency,
                value: influencerAmountPaypal.toFixed(2).toString(),
              },
            },
            {
              name: 'Admin Commission',
              quantity: '1',
              category: 'DIGITAL_GOODS',
              unit_amount: {
                currency_code: this.currency,
                value: totaladminCommissionAmt.toFixed(2).toString(),
              },
            },
            /*{
              name: 'other Commission',
              quantity: '1',
              category: 'DIGITAL_GOODS',
              unit_amount: {
                  currency_code: this.currency,
                  value: paypalChargeAmntPaypal.toFixed(2).toString(),
              },
            },*/
          ]
        }]
      },
      advanced: {
        commit: 'true'
      },
      style: {
        label: 'pay',
        layout: 'vertical',
        color: "blue",
        shape: "pill",
        size: "responsive"
      },
      onApprove: (data, actions) => {
        console.log('onApprove1 - transaction was approved, but not authorized', data, actions);
        actions.order.get().then(details => {
          console.log('onApprove2 - you can get full order details inside onApprove: ', details);
          //console.log(details.purchase_units);
          //console.log(this.totalAmount);
          let dataArr: any = {};
          this.transactionId = details.id;
          dataArr.invoice_number = Date.now();
          dataArr.brand_owner_id = this.dataArr.brand_owner_id;
          dataArr.campaign_assign_id = this.dataArr.campaign_assign_id;
          dataArr.transaction_id = details.id;
          dataArr.total_amount = this.totalAmount.toFixed(2);
          dataArr.net_amount = this.influencerAmount.toFixed(2);
          dataArr.gross_amount = this.totalAmount.toFixed(2);
          dataArr.admin_commission = this.adminCommissionAmt.toFixed(2);
          dataArr.admin_commission_pertg = this.adminCommissionPertg;
          dataArr.paypal_charge_brandowner_pertg = this.paypalChargePertg;
          dataArr.paypal_charge_brandowner_amt = this.paypalChargeAmnt;
          dataArr.payment_type = 'Paypal';
          dataArr.payment_status = details.status;
          dataArr.transaction_details = details;
          dataArr.default_currency_id = this.defaultCurrencyId;
          dataArr.brand_currency_id = this.brandCurrencyId;
          dataArr.currency_fee_pertg = this.currencyFeePertg
          dataArr.currency_fee_amt = this.currencyFeenAmt;
          this.CampaignService.brandownerPayment(dataArr).subscribe(
            (response: any) => {
              //console.log(response);
              if (response.responseCode === 200) {
                console.log(response);
                //this.toastr.success('Payment successfull', 'Success!');
                //this.router.navigate(["brand-owner/campaign-list"]);
              } else {
                console.log('get page error: ', response.responseDetails);
                //this.toastr.error('Please try again', 'Error!');
                //this.router.navigate(["brand-owner/campaign-list"]);
              }
            },
            error => {
              console.log('get page error: ', error);
              //this.toastr.error('Please try again', 'Error!');
              //this.router.navigate(["brand-owner/campaign-list"]);
            }
          );

        });

      },
      onClientAuthorization: (data) => {
        console.log('onClientAuthorization - you should probably inform your server about completed transaction at this point', data);
        //this.showSuccess = true;
        //console.log(this.totalAmount);
        //save data to order
        console.log(data);
        let dataArr: any = {};
        dataArr.transaction_id = this.transactionId;
        dataArr.status = data.status;
        this.CampaignService.updateBrandownerPayment(dataArr).subscribe(
          (response: any) => {
            //console.log(response);
            if (response.responseCode === 200) {
              console.log(response);
              this.toastr.success('Payment successfull', 'Success!');
              this.router.navigate(["brand-owner/campaign-list"]);
            } else {
              console.log('get page error: ', response.responseDetails);
              this.toastr.error('Please try again', 'Error!');
              this.router.navigate(["brand-owner/campaign-list"]);
            }
          },
          error => {
            console.log('get page error: ', error);
            //this.toastr.error('Please try again', 'Error!');
            //this.router.navigate(["brand-owner/campaign-list"]);
          }
        );

      },
      onCancel: (data, actions) => {
        console.log('OnCancel', data, actions);
        //this.showCancel = true;

      },
      onError: err => {
        console.log('OnError', err);
        //this.showError = true;
        this.toastr.error('Please try again', 'Error!');
        this.router.navigate(["brand-owner/campaign-list"]);
      },
      onClick: (data, actions) => {
        //console.log('onClick', data, actions);
        //console.log(data);
      },
    };
  }

  /**
 * call payout
 * @param id 
 */
  doPayout(id, event: any) {
    event.target.disabled = true;
    this.loading = true;
    let influencerDeductionPercentage = this.settingsArr['influencer_deduction_percentage'];
    let influencerPaypalCharge = this.settingsArr['influencer_paypal_charge'];
    let influencerId = this.selectedInfluncerId;
    this.CampaignService.startPayout(id, influencerDeductionPercentage, influencerPaypalCharge, influencerId).subscribe(
      (response: any) => {
        //console.log(response);
        if (response.responseCode === 200) {
          //this.campaign = response.data;
          this.loading = false;
          this.toastr.success('Payment has been released successfully.', 'Success!');
          this.router.navigate(["brand-owner/campaign-list"]);
        } else {
          this.loading = false;
          console.log('get page error: ', response.responseDetails);
          this.toastr.error('Please try again', 'Error!');
          this.router.navigate(["brand-owner/campaign-list"]);
        }
      },
      error => {
        this.loading = false;
        console.log('get page error: ', error);
        this.toastr.error('Please try again', 'Error!');
        this.router.navigate(["brand-owner/campaign-list"]);
      }
    );
  }

  /**
   * Get campaign data by id
   * @param id 
   */
  getCampaign(id) {
    this.CampaignService.getDataById(id).subscribe(
      (response: any) => {
        //console.log(response);
        if (response.responseCode === 200) {
          this.campaign = response.data;
          if (response.data && response.data.CampaignAssigns.length) {
            var assigns = response.data.CampaignAssigns
            for(let c = 0; c < assigns.length; c++){
              this.assignIds.push(assigns[c]['id'])
            }
            this.selectedInfluncerId = response.data.CampaignAssigns[0].influencer_id;
            this.getInfluncerData(response.data.CampaignAssigns[0].influencer_id, [], response.data.CampaignAssigns[0].status);
            this.selectedItemObj = response.data.CampaignAssigns[0];
            //console.log(this.selectedItemObj);
            this.allPaths = (response.allPaths) ? response.allPaths : {};
          }

        } else {
          console.log('get page error: ', response.responseDetails);
        }
      },
      error => {
        console.log('get page error: ', error);
      }
    );
  }
  /**
   * Set all commissions from settings
   * @param id 
   */
  setCommission() {
    //let settingsArr : Array<any> = [];
    this.SettingService.getAllsettings().subscribe(
      (response: any) => {
        //console.log(response);
        if (response.responseCode === 200) {
          this.sitesetting = response.data;
          let defaultCurrency = response.defaultCurrency;
          //console.log(defaultCurrency);
          if (this.sitesetting.length > 0) {
            for (let i = 0; i <= this.sitesetting.length - 1; i++) {
              this.settingsArr[this.sitesetting[i].setting_label] = this.sitesetting[i].setting_value;
              //console.log(this.sitesetting[i].setting_value);
            }
            this.adminCommissionPertg = Number(this.settingsArr['admin_commission']);
            this.paypalChargePertg = this.settingsArr['paypal_charge'];
            this.defaultCurrencyId = this.settingsArr['currency'];
            this.currency = defaultCurrency?defaultCurrency.code:'';
            //this.currencyFeePertg = defaultCurrency.online_currency_fee;
            //console.log("adminCommissionPertg "+this.adminCommissionPertg);
            //console.log("paypalChargePertg "+this.paypalChargePertg);
            console.log("defaultCurrencyId " + this.defaultCurrencyId);
            console.log("currency " + this.currency);

          }

        } else {
          console.log('get page error: ', response.responseDetails);
        }
      },
      error => {
        console.log('get page error: ', error);
      }
    );
  }

  /**
   * Get campaign data by id
   * @param id 
   */
  /*getassignedInfluencer(id){
    this.CampaignService.getInfluncerdata(id,influencerId).subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
            this.campaign = response.data;
            if(response.data && response.data.CampaignAssigns.length){
              this.selectedInfluncerId = response.data.CampaignAssigns[0].influencer_id;
              this.getInfluncerData(response.data.CampaignAssigns[0].influencer_id, [],response.data.CampaignAssigns[0].status);              
              this.selectedItemObj = response.data.CampaignAssigns[0];
              this.allPaths = (response.allPaths) ? response.allPaths: {};
            }
            
          } else {
              console.log('get page error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get page error: ', error);
      }
    );
  }*/

  /**
   * Get Influncer Detail
  */
  getInfluncerData(influncer_id, selectedInfluncer = [], status = '') {

    this.status = status;
    //console.log(status)
    this.selectedItemObj = selectedInfluncer;
    this.selectedInfluncerId = influncer_id;
    ///console.log(influncer_id);
    this.CampaignService.getInfluncerdata(this.id, influncer_id).subscribe(
      (response: any) => {
        console.log(response);
        if (response.responseCode === 200) {
          console.log(response);
          this.campaignHistory = response.campaignHistory;
          this.influncerMessageData = response.data;
          this.influncerMessageSlice = this.influncerMessageData.slice(0, 10);
          this.messagefilePath = response.filePath;
          this.assignedInfluencerDtl = response.campaignAssign;
          let brandCurrencyDetail = response.brandCurrencyPertg;
          this.brandCurrencyId = brandCurrencyDetail.id;
          //this.currency = brandCurrencyDetail.code;
          //console.log(this.currency);
          this.campaignStatus = this.assignedInfluencerDtl.status;
          //set paypal button
          if (this.campaignStatus == 'CONFIRM' || this.campaignStatus == 'CONTENT_SUBMITTED' || this.campaignStatus == 'CONTENT_APPROVED') {
            if (response.campaignAssign.BrandOwnerAdminTransaction === null) {
              this.showPaypal = true;
            }
            else {
              this.showPaypal = false;
            }
          } else {
            this.showPaypal = false;
          }
          //set payout button
          if (this.campaignStatus == 'CONTENT_APPROVED') {
            if (response.campaignAssign.BrandOwnerAdminTransaction != null) {
              if (response.campaignAssign.BrandOwnerAdminTransaction.InfluencerAdminTransaction === null) {
                this.brandOwnerTransId = response.campaignAssign.BrandOwnerAdminTransaction.id;
                this.showPayout = true;
              }
              else {
                this.showPayout = false;
              }
            } else {
              this.showPayout = false;
            }
          }
          else {
            this.showPayout = false;
          }

          //console.log(this.showPayout);
          //actual amount claimed by an influencer
          this.influencerAmount = (response.campaignAssign.proposal != null) ? parseFloat(response.campaignAssign.proposal.price) : 0;
          if (this.influencerAmount > 0) {

            //calculate admin commission
            this.adminCommissionAmt = Number(this.influencerAmount) * Number(this.adminCommissionPertg / 100);
            //console.log('ADMIN COMMISSION AMT '+this.adminCommissionAmt);
            let paypalPercentage = this.paypalChargePertg;

            this.paypalChargeAmnt = (Number(this.influencerAmount) + Number(this.adminCommissionAmt)) * Number(paypalPercentage / 100);
            //console.log('ADMIN PAYPAL CHARGE AMT '+this.paypalChargeAmnt);

            //check the currency fee is applied or not
            //console.log('defaultCurrencyId'+this.defaultCurrencyId);
            //console.log('brandCurrencyId'+this.brandCurrencyId);
            if (this.defaultCurrencyId == this.brandCurrencyId) {
              this.currencyFeePertg = 0;
              this.totalAmount = Number(this.influencerAmount) + Number(this.adminCommissionAmt) + Number(this.paypalChargeAmnt);
              //console.log('TOTAL AMT '+this.totalAmount);
              this.totaladminCommissionAmt = Number(this.adminCommissionAmt) + Number(this.paypalChargeAmnt);
            }else{
              this.currencyFeePertg = brandCurrencyDetail.online_currency_fee;
              let total = Number(this.influencerAmount) + Number(this.adminCommissionAmt) + Number(this.paypalChargeAmnt);
              let currencyFeeAmt = Number(total) * Number(this.currencyFeePertg / 100);
              this.currencyFeenAmt = currencyFeeAmt;
              this.totalAmount = Number(this.influencerAmount) + Number(this.adminCommissionAmt) + Number(this.paypalChargeAmnt) + Number(currencyFeeAmt);
              this.totaladminCommissionAmt = Number(this.adminCommissionAmt) + Number(this.paypalChargeAmnt) + Number(this.currencyFeenAmt);
            }
            //console.log('ADMIN COMMISSION + paypal '+this.totaladminCommissionAmt);
            //console.log('INFLUENCER AMT '+this.influencerAmount);
            this.initConfig();
          }
          //console.log(this.totalAmount);
          //console.log(this.influencerAmount);
          //console.log(this.adminCommissionPertg);
          //console.log(this.adminCommissionAmt);
          //console.log(this.paypalChargeAmnt);
          //console.log(response.data[0].brand_owner_id);
          //set default values to add brandowner transaction
          //this.dataArr.brand_owner_id = response.data[0].brand_owner_id;
          this.dataArr.brand_owner_id = response.campainDetail.brand_owner_id;
          //console.log(this.dataArr.brand_owner_id);
          this.dataArr.campaign_assign_id = response.campaignAssign.id;
          this.dataArr.payment_type = 'Paypal';
          //this.dataArr.payment_status = 'Paid';
        } else {
          console.log('get page error: ', response.responseDetails);
        }
      },
      error => {
        console.log('get page error: ', error);
      }
    );
  }

  /**
   * Get Platform List
   */
  getplatforms() {
    this.PlatformService.getPlatforms().subscribe(
      (response: any) => {

        if (response.responseCode === 200) {
          this.allPlatforms = response.data;
        } else {
          console.log('Campaign create error: ', response.responseDetails);
        }
      },
      error => {
        console.log('Campaign Create error: ', error);
      }
    );
  }
  /**
   * Accept Proposal
   */
  acceptProposal(proposal_id, invitation_id) {

    this.ProposalService.proposalAcceptReject(proposal_id, invitation_id, 'CONFIRM').subscribe(
      (response: any) => {
        //console.log(response);

        if (response.responseCode === 200) {
          this.getCampaign(this.id)
          this.closeModal();
        } else {
          console.log('get page error: ', response.responseDetails);
        }
      },
      error => {
        console.log('get page error: ', error);
      }
    );
  }

  /**
   * Reject Proposal
   */
  rejectProposal(proposal_id, invitation_id) {
    this.ProposalService.proposalAcceptReject(proposal_id, invitation_id, 'REFUSE').subscribe(
      (response: any) => {
        //console.log(response);

        if (response.responseCode === 200) {
          this.getCampaign(this.id)
          this.closeModal();
        } else {
          console.log('get page error: ', response.responseDetails);
        }
      },
      error => {
        console.log('get page error: ', error);
      }
    );
  }

  /**
   * Accept Content By brandOwner
   */
  acceptContent(influncer_id) {
    this.CampaignAssign.changeStatus(this.id, influncer_id, 'CONTENT_APPROVED').subscribe(
      (response: any) => {
        if (response.responseCode === 200) {
          this.status = response.data;
        } else {
          console.log('get page error: ', response.responseDetails);
        }
      },
      error => {
        console.log('get page error: ', error);
      }
    );
  }

  /**
    * Method to open accept modal
    * @param acceptInvitationModal 
    * @param invitationId 
    */
  acceptModal(acceptInvitationModal: TemplateRef<any>, invitation_id, campaign_id) {
    this.openModal(acceptInvitationModal, "faq_popup modal-dialog modal-md modal-dialog-centered");
    this.InfluncerService.getPropoaslData(invitation_id, campaign_id).subscribe(
      (response: any) => {
        //console.log(response);

        if (response.responseCode === 200) {
          this.proposalData = response.data;
        } else {
          console.log('get page error: ', response.responseDetails);
        }
      },
      error => {
        console.log('get page error: ', error);
      }
    );
  }

  /**
   * Confirm Job completed 
   */
  confirmJobComplete(confirmCompleteJob: TemplateRef<any>){
    this.openModal(confirmCompleteJob, "msg_popup modal-dialog modal-md modal-dialog-centered");
  }

  /**
   * Save the job as complete
   */
  jobCompleteSaved(ratingReviewModal: TemplateRef<any>){
    this.CampaignService.completingJob(this.id).subscribe(
      (response: any) => {
        if (response.responseCode === 200) {
          this.closeModal()
          this.openModal(ratingReviewModal, "msg_popup modal-dialog modal-md modal-dialog-centered");
        } else {
          console.log('get page error: ', response.responseDetails);
        }
      },
      error => {
        console.log('get page error: ', error);
      }
    );
    console.log('this message is saved')
  }

  /**
    * Method to open accept modal
    */
  messageModal(messageListModal: TemplateRef<any>) {
    this.openModal(messageListModal, "msg_popup modal-dialog modal-lg modal-dialog-centered");
  }

  /**
   * Medthod to open modal function
   * @param template
   * @param classR
   */
  openModal(template: TemplateRef<any>, classR: string) {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: classR })
    );
  }

  /**
    * Open Influncer Detail modal
    */
  influncerDetailModal(influencerDetailsModal: TemplateRef<any>, influncerId) {
    console.log(influncerId)
    this.getInfluncerById(influncerId)
    this.openModal(influencerDetailsModal, "customModal modal-xlg");
  }

  /**
   * Get Influncer detail by Id
   */
  getInfluncerById(influncerId) {
    this.InfluncerService.getInfluncerById(influncerId).subscribe(
      (response: any) => {

        if (response.responseCode === 200) {
          this.influncerDetail = response.data;
          this.influencerImagePath = (response.influencerImagePath) ? response.influencerImagePath : ''
        } else {
          console.log('get page error: ', response.responseDetails);

        }
      },
      error => {
        console.log('get page error: ', error);
      }
    );
  }

  /**
   * Save Rating 
   */
  savedRating(data){
    this.CampaignAssign.saveRating(data.value).subscribe(
      (response: any) => {

        if (response.responseCode === 200) {
          this.toastr.success('Campaign Completed', 'Success!');
          this.closeModal()
        } else {
          console.log('get page error: ', response.responseDetails);

        }
      },
      error => {
        console.log('get page error: ', error);
      }
    );
  }
  /**
   * Method to close modal
   */
  closeModal() {
    this.modalService._hideModal(1);
  }

  selectItem(idx) {
    this.selectedItemObj = this.progressArr[idx];
    this.progressArr.filter(x => x.checked = false);
    this.progressArr[idx].checked = true;
  }

  isEmpty(obj) {
    return (Object.getOwnPropertyNames(obj).length === 0);
  }
}
