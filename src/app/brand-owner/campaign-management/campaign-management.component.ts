import { Component, OnInit, ViewChild, TemplateRef, HostListener } from '@angular/core';
import { AstMemoryEfficientTransformer } from '@angular/compiler';
import { Router, ActivatedRoute, RoutesRecognized  } from '@angular/router';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl, AsyncValidatorFn, AbstractControl, ValidationErrors, FormArray } from '@angular/forms';
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
//import {NgbDateAdapter, NgbDateStruct, NgbDateNativeAdapter} from '@ng-bootstrap/ng-bootstrap';
import { CampaignService } from "./../services/campaign.service";
import { InfluncerService } from "./../services/influncer.service";
import { CountryService } from "./../services/country.service";
import { BrandService } from "./../services/brand.service";
import { ToastrService } from 'ngx-toastr';
import { filter, pairwise } from 'rxjs/operators';
import { SharedService } from "./../services/shared.service";

import { NgbDatepickerConfig, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateFRParserFormatter } from "./ngb-date-fr-parser-formatter"

@Component({
  selector: 'app-campaign-management',
  templateUrl: './campaign-management.component.html',
  styleUrls: ['./campaign-management.component.scss'],
  providers: [{provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter}]
})
export class CampaignManagementComponent implements OnInit {
  
  @ViewChild('fileInput') fileInput;
  @ViewChild('confirmRedirect') confirmRedirects;
  public show: boolean = true;
  public show2: boolean = false;
  public show3: boolean = false;
  public show4: boolean = false;
  public show5: boolean = false;
  imageUrl: string;
  private campaignId:string;
  public totalCompareArr:Array<any> = [];
  allCountries: any = [];
  model1: Date;
  model2: Date;

  public social = {
    fbChecked : false,
    twChecked : false,
    uChecked : false,
    inChecked : false,
  };
  objectKeys = Object.keys;

  public allPlatforms: any = [];
  public selectedPlatformIds: any = [];
  public sendPlatformIds: any = [];
  public selectedPlatformIdCount: any = 0;

  public active = false;
  public togglee: boolean = false;
  public togglee2: boolean = false;
  public togglee3: boolean = false;
  public togglee4: boolean = false;

  f1submitted = false;
  f2submitted = false;
  f3submitted = false;
  f4submitted = false;

  tldError1 = false;
  tldError2 = false;
  tldError3 = false;
  public draft = false;

  campaignForm_1: FormGroup;
  campaignForm_2: FormGroup;
  campaignForm_3: FormGroup;
  public allBrands: any = [];
  countryLists: any = [];
  platformLists:any=[];
  step: number;
  selectedInfluncersId:any = (localStorage.getItem('selectedInfluncers'))?localStorage.getItem('selectedInfluncers').split(","):[];
  selectedInfluncersImages:any = (localStorage.getItem('selectedInfluncerImage'))?JSON.parse(localStorage.getItem('selectedInfluncerImage')):[];
  selectedInfluncersData: any = [];
  private removeInfluncerId
  public modalRef: BsModalRef;
  fileToUpload: File = null;
  campaign_name;
  start_date;
  influencerImagePath:any;
  isBlock = ''
  isRedirectModalOpen =false
  currentUrl = ''
  public fileLabel = 'Attach Image / File';

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private route : ActivatedRoute,
    private CampaignService: CampaignService,
    private InfluncerService: InfluncerService,
    private CountryService: CountryService,
    private formBuilder: FormBuilder,
    private BrandService:BrandService,
    private modalService: BsModalService,
    private sharedService:SharedService,
    config: NgbDatepickerConfig 
  ) { 
    router.events
    .pipe(filter((evt: any) => evt instanceof RoutesRecognized), pairwise())
    .subscribe((events: RoutesRecognized[]) => {
      this.currentUrl = events[1].urlAfterRedirects;
      
      if(sharedService.getConfRedirect()){
        this.confirmPageChange(this.confirmRedirects)
      }
    });
    this.imageUrl='';
    const currentDate = new Date();
    config.minDate = {year:currentDate.getFullYear(), month:currentDate.getMonth()+1, day: currentDate.getDate()};
    config.maxDate = {year: 2099, month: 12, day: 31};
    config.outsideDays = 'hidden';
  }

  ngOnInit() {
    //Set Confirmation
    this.sharedService.setConfRedirect(true)

    this.route.params.subscribe( params => {
      this.step = params['step'];
      this.campaignId = params['campaignId']?params['campaignId']:'';
      if(this.step == 4){ 
        this.isBlock = '' 
        if(this.campaignId){
          localStorage.setItem('campaign_created_id', this.campaignId);
          this.getInfluncersByCampaign(this.campaignId);
          this.draft = true;
        }else{
          this.getSelectedInfluncer()
        }      
        this.goToaddInfluners()
      }else{
        this.isBlock = 'block'
        if(localStorage.getItem('campaign_created_id')){
          localStorage.removeItem('campaign_created_id');
        }
      }
    } );
    localStorage.setItem('isBlockUrl', this.isBlock)
    this.getcountries();
    this.getplatforms();
    //Get all brands
    this.getBrandList();

    /** First Step form initialize */
    this.campaignForm_1 = this.formBuilder.group({
      campaignName: ['', [Validators.required]],
      startDate: ['', [Validators.required]],
      dueDate: ['', [Validators.required]],
      brand: ['', [Validators.required]],
      launchDate: ['', [Validators.required]],
      serviceType: ['', [Validators.required]],
    });

     // set default values
    this.campaignForm_1.controls["brand"].setValue("");

    /** Third Step form initialize */
    this.campaignForm_3 = this.formBuilder.group({
      platforms: new FormArray([]),
      invitationExpieryDate: ['', [Validators.required]],
      invitationMessage: ['', [Validators.required]],
      campaignDetails: ['', [Validators.required]],
      contentConcept: ['', [Validators.required]],
      files: [''],
      productUrl: ['', [Validators.required, Validators.pattern('(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?')]],
      country: ['', [Validators.required]],
      budget: ['', [Validators.required, Validators.pattern('^[0-9.,]+$')]],
    });
    //console.log(this.campaignForm_3.value.platforms)
    // set default values
    this.campaignForm_3.controls["country"].setValue('');
    //this.campaignForm_3.controls["platforms"].setValue([]);

    this.campaignForm_2 = new FormGroup({
      objectControl: new FormControl('', <any>Validators.required),
      repostLD: new FormControl('', <any>Validators.required),
      cocreatedLD: new FormControl('', <any>Validators.required),
      eventLD: new FormControl('', <any>Validators.required),
      terms: new FormControl(false, <any>Validators.required)
    });

    
    //console.log(localStorage.getItem('selectedInfluncers'))
    // /////////////////////////
    // this.show = false;
    // this.show2 = false;
    // this.show3 = true;
    // this.show4 = false;
    // this.active = true;
    // this.togglee = true;
    // this.togglee2 = false;
    // this.togglee3 = false;
    ///////////////////////////  
    this.setObjectValidators();

  }

  /**
   * Show the file count
   */
  countFile(file) {
    this.fileLabel = file.length+' file selected';
  }
  
  /**
   * First Step Form Validation
   */
  get f1() { return this.campaignForm_1.controls; }
  /**
   * Second Step Form Validation
   */
  get f2() { return this.campaignForm_2.controls; }
  /**
   * Third Step Form Validation
   */
  get f3() { return this.campaignForm_3.controls; }
  /**
   * Date Picker
   */
  get today() {
    return new Date();
  }

  buildForm() {
    this.campaignForm_2 = this.formBuilder.group({
      objectControl: [null, [Validators.required]],
      repostLD: [null, [Validators.required]],
      cocreatedLD: [null, [Validators.required]],
      eventLD: [null, [Validators.required]],
    });
  }

  setObjectValidators() {
    const repostControl = this.campaignForm_2.get('repostLD');
    const cocreatedControl = this.campaignForm_2.get('cocreatedLD');
    const eventControl = this.campaignForm_2.get('eventLD');

    this.campaignForm_2.get('objectControl').valueChanges
      .subscribe(objectValue => {
        //console.log('objectValue=>',objectValue)
        if (objectValue === 'CR') {
          repostControl.setValidators([Validators.required]);
          cocreatedControl.setValidators(null);
          eventControl.setValidators(null);
          this.tldError1 = true;
          this.tldError2 = false;
          this.tldError3 = false;
        }

        if (objectValue === 'CC') {
          repostControl.setValidators(null);
          cocreatedControl.setValidators([Validators.required]);
          eventControl.setValidators(null);
          this.tldError1 = false;
          this.tldError2 = true;
          this.tldError3 = false;
        }

        if (objectValue === 'AE') {
          repostControl.setValidators(null);
          cocreatedControl.setValidators(null);
          eventControl.setValidators([Validators.required]);
          this.tldError1 = false;
          this.tldError2 = false;
          this.tldError3 = true;
        }

        repostControl.updateValueAndValidity();
        cocreatedControl.updateValueAndValidity();
        eventControl.updateValueAndValidity();
      });
  }

  checkObjectValidators() {
    const repostControl = this.campaignForm_2.get('repostLD');
    const cocreatedControl = this.campaignForm_2.get('cocreatedLD');
    const eventControl = this.campaignForm_2.get('eventLD');
    const objectValue = this.campaignForm_2.get('objectControl').value;
    //console.log('objectValue=>',objectValue)

        if (objectValue === 'CR') {
          repostControl.setValidators([Validators.required]);
          cocreatedControl.setValidators(null);
          eventControl.setValidators(null);
          this.tldError1 = true;
          this.tldError2 = false;
          this.tldError3 = false;
        }

        if (objectValue === 'CC') {
          repostControl.setValidators(null);
          cocreatedControl.setValidators([Validators.required]);
          eventControl.setValidators(null);

          this.tldError1 = false;
          this.tldError2 = true;
          this.tldError3 = false;
        }

        if (objectValue === 'AE') {
          repostControl.setValidators(null);
          cocreatedControl.setValidators(null);
          eventControl.setValidators([Validators.required]);

          this.tldError1 = false;
          this.tldError2 = false;
          this.tldError3 = true;
        }

        repostControl.updateValueAndValidity();
        cocreatedControl.updateValueAndValidity();
        eventControl.updateValueAndValidity();
      
  }

  onFileChange($event) {
    let file = $event.target.files[0]; // <--- File Object for future use.
   // this.campaignForm_3.controls['files'].setValue(file ? file.name : ''); // <-- Set Value for Validation
  }
  /**
   * get Country List
   */

  getcountries(){
    this.CountryService.getAllCountries().subscribe(
      (response: any) => {
                  
          if (response.responseCode === 200) {
            this.countryLists = response.countries
          } else {
              console.log('Campaign create error: ', response.responseDetails);
          }
      },
      error => {
          console.log('Campaign Create error: ', error);
      }
     );  
  }

  /**
   * Get Platform List
   */
  getplatforms(){
    this.CampaignService.getPlatforms().subscribe(
      (response: any) => {
                  
          if (response.responseCode === 200) {
            this.platformLists = response.data
            this.allPlatforms = response.data;
            this.addCheckboxes();
            console.log(this.allPlatforms)
          } else {
              console.log('Campaign create error: ', response.responseDetails);
          }
      },
      error => {
          console.log('Campaign Create error: ', error);
      }
     );
  }

  /**
   * Get Selected Influncer for fourth step 
   */
  getSelectedInfluncer(){
    this.selectedInfluncersData = this.selectedInfluncersImages
    //console.log(this.selectedInfluncersData)
  }

  /**
   * 
   * @param type Get influncer by campaign Id
   */

  getInfluncersByCampaign(campaingId){
    this.InfluncerService.getInfluncerByCampaign(campaingId).subscribe(
      (response: any) => {
                  
          if (response.responseCode === 200) {
            this.selectedInfluncersData = response.data
            this.influencerImagePath =response.influencerImagePath
            console.log(this.selectedInfluncersData)
          } else {
              console.log('Campaign create error: ', response.responseDetails);
          }
      },
      error => {
          console.log('Campaign Create error: ', error);
      }
    );
  }
  /**
   * remove Confirmation modal
   */
  removeSavedModal(confirmRemovedSaved, removeInfluncerId){
    this.removeInfluncerId = removeInfluncerId;
    this.openModal(confirmRemovedSaved, "modal-sm");
  }
  /**
   * remove Selected Influncer
   */
  removeSelectedInfluncer(){
    
    let infIndex = this.selectedInfluncersId.indexOf(this.removeInfluncerId.toString())
    let imgIndex = this.selectedInfluncersImages.findIndex(obj => obj.id==this.removeInfluncerId);
    
    this.selectedInfluncersId.splice(infIndex, 1)
    this.selectedInfluncersImages.splice(imgIndex, 1)
    localStorage.setItem('selectedInfluncers', this.selectedInfluncersId);
    localStorage.setItem('selectedInfluncerImage', JSON.stringify(this.selectedInfluncersImages));

    this.closeModal()
  }
  /**
   * Save Influncer Data
   */
  saveInfluncer(type){
    if(!this.f4submitted){
      this.f4submitted = true;
      this.sharedService.setConfRedirect(false)
      this.CampaignService.assinInfluncer(this.selectedInfluncersId, localStorage.getItem('campaign_created_id'), type).subscribe(
        (response: any) => {
                    
            if (response.responseCode === 200) {              
              //localStorage.removeItem('redrictModal')
              if(localStorage.getItem('selectedInfluncers')){
                localStorage.removeItem("selectedInfluncers");
                localStorage.removeItem("selectedInfluncerImage");
                localStorage.removeItem("campaign_created_id");
                localStorage.removeItem("stepCompleted");
              }
              this.router.navigate(['/brand-owner/campaign-list'])
              this.toastr.success("Campaign Created Successfully.", 'Success!');
            } else {
                console.log('Campaign create error: ', response.responseDetails);
            }
        },
        error => {
            console.log('Campaign Create error: ', error);
        }
      );
    }    
  }
  /**
   * First Step Submit
   */
  toggle() {
    this.f1submitted = true;
    if (this.campaignForm_1.invalid) {
      return;
    }
    var tentativeLaunchDate = this.campaignForm_1.value.launchDate
    this.campaignForm_2.controls.repostLD.setValue({
      year: tentativeLaunchDate.year,
      month: tentativeLaunchDate.month,
      day: tentativeLaunchDate.day
    });
    this.campaignForm_2.controls.cocreatedLD.setValue({
      year: tentativeLaunchDate.year,
      month: tentativeLaunchDate.month,
      day: tentativeLaunchDate.day
    });
    this.campaignForm_2.controls.eventLD.setValue({
      year: tentativeLaunchDate.year,
      month: tentativeLaunchDate.month,
      day: tentativeLaunchDate.day
    });
      this.CampaignService.createCampaign(this.campaignForm_1.value).subscribe(
          (response: any) => {
              console.log(response.data);
              
              if (response.responseCode === 200) {
                  localStorage.setItem('stepCompleted', '1');

                  if(!localStorage.getItem('campaign_created_id')){
                    localStorage.setItem('campaign_created_id', response.data.id);
                  }
                  console.log(localStorage.getItem('campaign_created_id'));
                  this.show = false;
                  this.show2 = true;
                  this.show3 = false;
                  this.show4 = false;
                  this.togglee = true;
                  this.togglee2 = false;
                  this.togglee3 = false;
              } else {
                this.toastr.error("campaign not saved");
                  console.log('Campaign create error: ', response.responseDetails);
              }
          },
          error => {
            this.toastr.error("campaign not saved");
              console.log('Campaign Create error: ', error);
          }
      );  
      window.scroll(0,0);
  }
  /**
   * Second Step Submit
   */
  toggle2() {
    this.f2submitted = true;
    if (this.campaignForm_2.invalid || !this.campaignForm_2.value.terms) {
      return;
    }
    // call service to get country list
      this.CampaignService.updateCampaignSecond(this.campaignForm_2.value).subscribe(
        (response: any) => {
            
            if (response.responseCode === 200) {
                localStorage.setItem('stepCompleted', '2');
                this.show = false;
                this.show2 = false;
                this.show3 = true;
                this.show4 = false;
                this.active = true;
                this.togglee = true;
                this.togglee2 = true;
                this.togglee3 = false;
            } else {
              this.toastr.error("campaign not saved");
                console.log('Campaign create error: ', response.responseDetails);
            }
        },
        error => {
          this.toastr.error("campaign not saved");
            console.log('Campaign Create error: ', error);
        }
    );
    window.scroll(0,0);
  }
  /**
   * Third Step Submit
   */
  toggle3() {
    localStorage.setItem('stepCompleted', '3');
    
    this.f3submitted = true;
    //console.log(this.campaignForm_3.value)
    if (this.campaignForm_3.invalid) {
      return;
    }
    // call service to get country list
    const formData = new FormData();
    const fileBrowser = this.fileInput.nativeElement;
    /*if (fileBrowser.files && fileBrowser.files[0]) {
      formData.append('up_file[]', fileBrowser.files[0]);
    }*/

    if(fileBrowser.files.length){
      for(let i=0 ; i < fileBrowser.files.length ; i++)
      formData.append('photos[]', fileBrowser.files[i],fileBrowser.files[i].name);
     // formData.append('photos[]', file, file.name);
    }

    var exp_date = this.campaignForm_3.value.invitationExpieryDate;
    formData.append('invitationExpieryDate',exp_date.year+'-'+exp_date.month+'-'+exp_date.day);
    formData.append('platforms',this.sendPlatformIds);
    formData.append('invitationMessage',this.campaignForm_3.value.invitationMessage);
    formData.append('campaignDetails',this.campaignForm_3.value.campaignDetails);
    formData.append('contentConcept',this.campaignForm_3.value.contentConcept);
    formData.append('files',this.campaignForm_3.value.files);
    formData.append('productUrl',this.campaignForm_3.value.productUrl);
    formData.append('country',this.campaignForm_3.value.country);
    formData.append('budget',this.campaignForm_3.value.budget);
    formData.append('id',localStorage.getItem('campaign_created_id'));
    this.sharedService.setConfRedirect(false)
      this.CampaignService.updateCampaignThird(formData).subscribe(
          (response: any) => {
            this.isBlock = ''
            localStorage.setItem('isBlockUrl', this.isBlock)
            //localStorage.removeItem('redrictModal')
              if (response.responseCode === 200) {
                if(localStorage.getItem('selectedInfluncers')){
                  this.goToaddInfluners()
                  this.getSelectedInfluncer()
                }else{
                  this.show = false;
                  this.show2 = false;
                  this.show3 = false;
                  this.show4 = true;
                  this.togglee = true;
                  this.togglee2 = true;
                  this.togglee3 = true;
                }
              } else {
                this.toastr.error("campaign not saved");
                  console.log('Campaign update error: ', response.responseDetails);
              }
          },
          error => {
            this.toastr.error("campaign not saved");
              console.log('Campaign update error: ', error);
          }
      ); 
      window.scroll(0,0);
  }

  goToaddInfluners() {
    this.sharedService.setConfRedirect(false)
    //localStorage.removeItem('redrictModal')
    this.show = false;
    this.show2 = false;
    this.show3 = false;
    this.show4 = false;
    this.show5 = true;
    this.togglee = true;
    this.togglee2 = true;
    this.togglee3 = true;
    this.togglee4 = true;
  }

  /**
   * Remove from draft modal
   */
  removeFromDraftModal(confirmRemovedDraft){
    this.openModal(confirmRemovedDraft, "modal-sm");
  }

  /**
   * Remove a influncer from draft
   */
  removeFromDraft(){
    this.sharedService.setConfRedirect(false)
    this.CampaignService.removeDraft(this.campaignId).subscribe(
      (response: any) => {
        //localStorage.removeItem('redrictModal')
          if (response.responseCode === 200) {
            this.closeModal()
            this.router.navigate(['/brand-owner/campaign-list'])
            this.toastr.success("Campaign Deleted Successfully.", 'Success!');
          } else {
            
            this.toastr.info("campaign not deleted");
            //console.log('Campaign deleted error: ', response.responseDetails);
          }
      },
      error => {
        this.toastr.error("Not Removed");
          //console.log('Campaign update error: ', error);
      }
  );
  }

  back1() {
    this.show = true;
    this.show2 = false;
    this.show3 = false;
    this.show4 = false;
    this.togglee = false;
    this.togglee2 = false;
    this.togglee3 = false;
    
  }

  back2() {
    this.show = false;
    this.show2 = true;
    this.show3 = false;
    this.show4 = false;
    this.togglee = true;
    this.togglee2 = false;
    this.togglee3 = false;
    this.checkObjectValidators();
  }
  onOpenChange(t) {
    console.log(this.social)
  }

  objectLength(obj) {
    return Object.keys(obj);
  }
  // onImagechange(event: any) {
	// 	const type = event.target.files[0].type.split('/')[1];
	// 	if (type === 'jpeg' || type === 'png' || type === 'jpg') {
	// 		if (event.target.files && event.target.files[0]) {
	// 			const img = new Image();
	// 			img.src = window.URL.createObjectURL(event.target.files[0]);
	// 			const reader = new FileReader();
	// 			const self = this;
	// 			img.onload = function () {
	// 				const width = img.naturalWidth, height = img.naturalHeight;
	// 				console.log(width, height);
	// 				window.URL.revokeObjectURL(img.src);

	// 				reader.onload = (e: any) => {
	// 					if (width <= 200 && height <= 57) {

	// 						self.imageUrl = e.target.result;
	// 					} else {
	// 						///self.toast.error('Please upload the logo having a dimension within 200px & 57px only');
	// 					}
	// 				};
	// 				reader.readAsDataURL(event.target.files[0]);
	// 			};
	// 		} else {
	// 		///	this.toast.error('Uplaod Image Only');
	// 		}
	// 	}
  // }
  /**
   * Method to add platforms in control
   */
  private addCheckboxes() {
      this.allPlatforms.forEach((o, i) => {
      //const control = new FormControl(i === 0); // if first item set to true, else false
      const control = new FormControl(false);
      (this.f3.platforms as FormArray).push(control);
      });
  }
  
  /**
   * Method on click on checkbox
   * @param t 
   */
  onCheckBoxClick(t,i) {
      this.selectedPlatformIds[i] = t.target.checked ? true : false;
      if(t.target.checked){
        this.sendPlatformIds[i] = this.allPlatforms[i].id
      }else{
        this.sendPlatformIds[i]=false
      }
      //console.log(this.sendPlatformIds)
      if(t.target.checked){
          this.selectedPlatformIdCount++;
      } else{
          this.selectedPlatformIdCount--;
      }

      if(this.selectedPlatformIdCount < 0){
          this.selectedPlatformIdCount = 0;
      }
  }

   /**
   * Get all brands
   */
  getBrandList(){
    this.BrandService.getBrandOfOwnerList().subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
              this.allBrands = response.data;
          } else {
              console.log('get country list error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get country list error: ', error);
      }
    );
  }
  getAllCountries(){
    // call service to get country list
    this.CountryService.getAllCountries().subscribe(
        (response: any) => {
            //console.log(response);
            
            if (response.responseCode === 200) {
                this.allCountries = response.countries;
            } else {
                console.log('get country list error: ', response.responseDetails);
            }
        },
        error => {
            console.log('get country list error: ', error);
        }
    );
}

confirmPageChange(confirmRedirect){
  console.log(localStorage.getItem('redrictModal'))
  if(!this.isRedirectModalOpen){
    this.openModal(confirmRedirect, "modal-sm");
    this.isRedirectModalOpen = true
  }
  
}

redirectToOtherPage(){
  this.closeModal();
  localStorage.removeItem("isBlockUrl");
  localStorage.removeItem('redrictModal')
  this.router.navigate([this.currentUrl]);
}

redirectHide(){
  this.isRedirectModalOpen = false;
  this.closeModal();
}
 /**
   * Medthod to open modal function
   * @param template
   * @param classR
   */
  openModal(template: TemplateRef<any>, classR: string) {
    this.modalRef = this.modalService.show(
    template,
    Object.assign({}, { class: "customModal " + classR })
    );
  }

  /**
   * Method to close modal
   */
  closeModal() {
      this.modalService._hideModal(1);
  }

}
