import { Component, OnInit, TemplateRef, Host } from '@angular/core';
import { CampaignService } from "./../services/campaign.service";
import { ToastrService } from 'ngx-toastr';
import { InfluncerService } from "./../services/influncer.service";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { AppComponent } from './../../app.component';

@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.scss']
})
export class CampaignComponent implements OnInit {

  campaigns: any=[];
  campaignConfig: any;
  public allPaths: any={};
  public modalRef: BsModalRef;
  public influencerImagePath:any;
  public influncerDetail: any ={};

  constructor(
    @Host() private app: AppComponent,
    private CampaignService:CampaignService,
    private toastr: ToastrService,
    private InfluncerService: InfluncerService,
    private modalService: BsModalService
    ) { }

  ngOnInit() {
    this.getCampaign('')
    this.campaignConfig = {
      itemsPerPage: this.app.getSettingData('pagination'),
      currentPage: 1,
      totalItems: this.campaigns.count
    };
  }
  /**---------Pagination Page change  --------------*/
  campaignPageChanged(event){
    this.campaignConfig.currentPage = event;
  }
   /**---Get All Caqmpaing  ----------*/
   getCampaign(type){
    this.CampaignService.getData(type).subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
            this.campaigns = response.data;
            this.allPaths = (response.allPaths)?response.allPaths:{}
          } else {
              console.log('get page error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get page error: ', error);
      }
      
  );
  }

  /**
   * Open Influncer Detail modal
   */
  influncerDetailModal(influencerDetailsModal: TemplateRef<any>, influncerId){
    console.log(influncerId)
    this.getInfluncerById(influncerId)
    this.openModal(influencerDetailsModal, "customModal modal-xlg");
  }

  /**
   * Get Influncer detail by Id
   */
  getInfluncerById(influncerId){
    this.InfluncerService.getInfluncerById(influncerId).subscribe(
      (response: any) => {
          
          if (response.responseCode === 200) {
            this.influncerDetail = response.data;
            this.influencerImagePath=(response.influencerImagePath)?response.influencerImagePath:''          
          } else {
              console.log('get page error: ', response.responseDetails);
               
          }
      },
      error => {
          console.log('get page error: ', error);
      }
    );
  }

   /**
   * calculate the sum of all social media followers
   */
  getTotalFollowers(platform){
    var sum = 0; 
    for(let i=0; i<platform.length; i++){
      sum += platform[i].followers
    }

    return sum;
  }
  /**
   * Medthod to open modal function
   * @param template
   * @param classR
   */
  openModal(template: TemplateRef<any>, classR: string) {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: "customModal " + classR })
    );
  }

  /**
   * Method to close modal
   */
  closeModal() {
    this.modalService._hideModal(1);
  }

}
