import { Component, OnInit, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { InfluncerService } from "./../services/influncer.service";
import { SharedService } from "./../services/shared.service";
declare var window: any;
declare var FB: any;
@Component({
  selector: 'app-compare-influencers',
  templateUrl: './compare-influencers.component.html',
  styleUrls: ['./compare-influencers.component.scss']
})
export class CompareInfluencersComponent implements OnInit {
  
  public totalCompareArr: Array<any> = [];
  public totalCompare: Array<any> = [];
  public expandCampaign: boolean;
  public selectedInfluncersData:any;
  public influencerImagePath: any;
  public fbAccess:any;
  public selectedInfluncersIds;
  @ViewChild('expanFixd') expanFixd: ElementRef;
  @ViewChild('contentPdf') contentPdf: ElementRef;
  constructor(
    private route: ActivatedRoute,
    private renderer: Renderer2,
    private InfluncerService: InfluncerService,
    private sharedService:SharedService,
    private _location: Location
  ) { 
    this.expandCampaign = false;
    
  }

  ngOnInit() {
    this.selectedInfluncersIds = this.sharedService.compareInfluncer
    this.getSelectedInfluncer()
    // console.log(this.route.snapshot.paramMap.get("length"))
    const totalCompareCount = Number(this.selectedInfluncersIds.length);
    for (let i = 0; i < totalCompareCount; i++) {
      this.totalCompareArr.push({
        campaign: false
      });
    } 
   this.fbAccess = [
      {'id':1227107547472387, 'token':'EAAGJIniZCDSkBAErxdSBfxBXNhiS5pd3yC8Bq4Y10waN0Aeym6PLzsRz6XJIN4X7xAO17BfKLDlM9bJhZAhd6AzVqQdJr8UNiyEY64qho5pk8rQT1LgJ3G9nsZCDMir5eHUQbcDmffXtd2P5b9Q5wZCeDlHN1gk0IZBQZAnp8OJxIU1LmF7JVeYrZBU6AOZAYf0ZD'},   
     // {'id':101095481383009, 'token':'EAAGJIniZCDSkBABpx9iuUkZAZC5kOqLX9I4RzpCDnAqKzj0SYSkR2XgBkSlO0zouhioqTZC05oZCEG3PFCoFg3PxpzFEe8zVmOcMKqe1Jl8ZAM9Pp1piA4FC1t3KZAZCLZAfXFcmTpATx7U6woppFI6i7CNBIcSOYgctZAIT2CzW3gV3pap0vWLF3TrblJHGr0B9vL0ll9FY9sZBZBLX8VNiZAMtYGdtZCU4pMRTqNe641VZCc23fP0MBrHVSgFxH2WuSpTYzwZD'},   
    ]
    
  }



  test(){
    console.log('test function call')
  }
  /**
   * Get Selected Influncer Data
   */
  getSelectedInfluncer(){
    this.InfluncerService.getSelectedInfluncerDetail(this.selectedInfluncersIds).subscribe(
      (response: any) => {
                  
          if (response.responseCode === 200) {
            this.selectedInfluncersData = response.data
            this.influencerImagePath = response.influencerImagePath
            for(let i=0;i<this.selectedInfluncersData.length;i++){
              console.log(i)
              //this.getFbData(1227107547472387,'EAAGJIniZCDSkBANVH7b56UPeV07RIQxOL5ZBtoZBKv3ODe469OOI17rSyExZC8ewaJD3jPCTncVZA5sjDcV4Aa8ql93uhjjiPVAablyurlAr3RPea91n5QzHSMyVC47qFoLIwacgAV7R5YKR6YmBAi0h8eOnPYnZAZBXOONEcNgJTd8kduXYRgETTtWaKZCn1nhAweZAv2L4HXqPEjUppGZAVGWG7MVKsOZC4exuFIIRrVKdQZDZD')
            }
          } else {
              console.log('Campaign create error: ', response.responseDetails);
          }
      },
      error => {
          console.log('Campaign Create error: ', error);
      }
    );
  }

  /**
   * Create the pdf 
   */
  captureScreenPdf(){

    this.InfluncerService.createPdf(this.selectedInfluncersIds).subscribe(
      (response: any) => {
                  
          if (response.responseCode === 200) {
            //this.selectedInfluncersData = response.data
            //this.influencerImagePath = response.influencerImagePath
            //window.location.href=response.data;
            window.open(response.data, "_blank");
            //console.log(response.data)
          } else {
              console.log('Campaign create error: ', response.responseDetails);
          }
      },
      error => {
          console.log('Campaign Create error: ', error);
      }
    );

  }

  addTocompare(indx) {
    this.totalCompareArr[indx].campaign = true;
    this.totalCompare = this.totalCompareArr.filter(x => x.campaign === true);
  }

  back(){
    this._location.back();
  }

  openCampaign() {
    if (!this.expandCampaign && this.totalCompare.length > 0) {
      this.renderer.addClass(this.expanFixd.nativeElement, 'activeSlide');
      this.expandCampaign = true;
    } else {
      this.renderer.removeClass(this.expanFixd.nativeElement, 'activeSlide');
      this.expandCampaign = false;
    }
  }
}
