import { Component, OnInit, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import { ChartType, ChartOptions } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import { CampaignService } from "./../services/campaign.service";
import { InfluncerService } from "./../services/influncer.service";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";

import { PlatformService } from "./../services/platform.service";
import { SharedVariable } from './../../shared/shared-variable.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  //Search variable
  CampaignNameSearch:any = ''
  SearchMonth:any = ''
  // Pie
  public pieChartOptions: ChartOptions = {};
  public pieChartLabels: Label[] = ['Inprogress campaigns', 'Published campaigns', 'Draft campaigns', 'Announced campaigns'];
  public pieChartData: SingleDataSet;
  public pieChartDataColors: any[];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public PastCampaignNameSearch: any = '';
  public searchTextOnGoingCampaign: any = '';
  public searchTextPublishedCampaign: any = '';
  public filterPlatformOnGoingCampaign: any = '';
  public filterPlatformPublishedCampaign: any = '';
  public campaigns: any = [];
  public pastCampaigns: any = [];
  public allPlatforms: any = [];
  public totalCampaign: any = 0;
  public influencerImagePath:any;
  public influncerDetail: any ={};
  
  @ViewChild('doughnutCanvas') doughnutCanvas:ElementRef

  // doughnut
  public doughnutChartOptions: ChartOptions = {};
  public doughnutChartLabels: Label[] = ['Ongoing campaigns', 'Published campaigns'];
  public doughnutChartData: SingleDataSet;
  public doughnutDataColors: any[];
  public doughnutChartType: ChartType = 'doughnut';
  public doughnutChartLegend = true;
  public doughnutChartPlugins = [];
  //detail and graph count
  public date = new Date();
  public totalInfluncer:any = [];
  public totalDraftCampaign:Number = 0;
  public totalOngoingCampaign:Number = 0;
  public totalPublishedCampaign:Number = 0;
  public platforms:any = []
  public allPaths: any={};
  public modalRef: BsModalRef;
  public onGoingCampaignDate: any = new Date();
  public pastCampaignDate: any = new Date();
  public campaignProgress:any = {};

  constructor(
    private CampaignService: CampaignService,
    private PlatformService: PlatformService,
    private InfluncerService: InfluncerService,
    private modalService: BsModalService,
    private sharedVariable: SharedVariable
    ) { }

  ngOnInit() {
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];;
    var date = new Date();
    this.getInfluncerTotal()
    this.getCampaignStatusWise()
    this.getCampaign('past', {}, '', months[date.getMonth()] + ' ' + date.getFullYear())
    this.getCampaign('current', {}, '', months[date.getMonth()] + ' ' + date.getFullYear())
    this.getplatforms();

    this.pieChartData = [];
    this.pieChartDataColors = [
      {
        backgroundColor: [
          'rgba(30, 169, 224, 0.8)',
          'rgba(255,165,0,0.9)',
          'rgba(139, 136, 136, 0.9)',
        ]
      }
    ];
    this.pieChartOptions = {
      responsive: true,
      rotation: Math.PI / 2,
      legend: {
        position: 'left',
        labels: {
            //fontColor: "white",
            boxWidth: 7,
            padding: 5,
            usePointStyle: true
        }
      }
    }

    // doughnut
    var ctx = this.doughnutCanvas.nativeElement.getContext("2d");
    let me = this;
    this.doughnutChartData = [];
    this.doughnutDataColors = [{ backgroundColor: ['rgba(255,165,0,0.9)', 'rgba(30, 169, 224, 0.8)']}];
    
    this.doughnutChartOptions = {
      responsive: true,
      rotation: 1 * Math.PI,
      circumference: 1 * Math.PI,
      cutoutPercentage: 80,
      tooltips:{
        enabled:true
      },
      animation:{ 
        //duration: 0,
        onComplete: function() {
          me.centerLabel(ctx);
        }
      },
      hover:{
        animationDuration: 0
      },
      legend: {
        position: 'left',
        labels: {
            //fontColor: "white",
            boxWidth: 7,
            padding: 5,
            usePointStyle: true
        }
      }
    };

    //Receive shared service data
    this.campaignProgress = this.sharedVariable.getCampaingProgress
  }

    /**
   * Get dash board counted data
   */
  getInfluncerTotal(){
    this.InfluncerService.totalInfluncer().subscribe(
      (response: any) => {
          if (response.responseCode === 200) {
            this.totalInfluncer = (response.data && response.data.count) ? response.data.count : 0;
          } else {
              //console.log('get page error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get page error: ', error);
      }
    );
  }

  /**
   * Get Category wise campaigm(Draft, In progress, Published) 
   */
  getCampaignStatusWise(){
    this.CampaignService.statusWiseCount().subscribe(
      (response: any) => {
          if (response.responseCode === 200) {
            if(response.data && response.data.length >0){
              var total = 0
                for(var i =0; i<response.data.length; i++){
                  if(response.data[i]['status'] == 'DRAFT'){
                    this.totalDraftCampaign = response.data[i]['campaignCnt'];
                    var d1 = response.data[i]['campaignCnt'];
                    this.pieChartData[2] = d1;
                    //total += d1;
                    //this.doughnutChartData[2] =total;
                  }else if(response.data[i]['status'] == 'IN_PROGRESS'){
                    this.totalOngoingCampaign = response.data[i]['campaignCnt'];
                    var d2 = response.data[i]['campaignCnt'];
                    this.pieChartData[0] = d2;
                    total += d2;
                    this.doughnutChartData[0] = d2;
                  }else if(response.data[i]['status'] == 'ANNOUNCED'){
                    this.totalOngoingCampaign = response.data[i]['campaignCnt'];
                    var d4 = response.data[i]['campaignCnt'];
                    this.pieChartData[3] = d4;
                    total += d4;
                    this.doughnutChartData[0] = d4;
                  }else  if(response.data[i]['status'] == 'PUBLISHED'){
                    var d3  = response.data[i]['campaignCnt'];
                    this.pieChartData[1] = d3;
                    total += d3;
                    this.doughnutChartData[1] =d3;
                  }
                }
                this.totalCampaign = total;   
            } 
          } else {
              console.log('get page error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get page error: ', error);
      }
    );
  }
  
  /**
   * Method to filter with platform
   */
  updateOnGoingCampaign(){
    let platformId = this.filterPlatformOnGoingCampaign;
    let monthYear = this.onGoingCampaignDate;
    this.getCampaign('current', {}, platformId ,monthYear)
  }

  /**
   * Method to filter with platform
   */
  updatePublishedCampaign(){
    let platformId = this.filterPlatformPublishedCampaign;
    let monthYear = this.pastCampaignDate
    this.getCampaign('past', {}, platformId, monthYear)
  }

  /**
   * Method to get campaign
   * @param type 
   * @param filter 
   * @param limit 
   * @param platformFilter 
   */
  getCampaign(type, filter = {}, platformFilter = '', monthYear ='' ){
    this.CampaignService.getData(type, filter, platformFilter, monthYear).subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
              if(type == 'past'){
                this.pastCampaigns = response.data;
              }else{
                this.campaigns = response.data;
              }
              console.log(this.campaigns);
              
              this.allPaths = (response.allPaths)?response.allPaths:{}
              //this.allPaths = response.allPaths;
          } else {
              console.log('get page error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get page error: ', error);
      }
    );
  }

  /**
   * Get Platform List
   */
  getplatforms(){
    this.PlatformService.getPlatformsNormal().subscribe(
      (response: any) => {      
          if (response.responseCode === 200) {
            this.allPlatforms = response.data;
          } else {
              console.log('Campaign create error: ', response.responseDetails);
          }
      },
      error => {
          console.log('Campaign Create error: ', error);
      }
     );
  }

  /**
   * Method to add lebel at the 
   * middle of the chart
   * @param ctx 
   */
  centerLabel(ctx) {
    //   Chart.types.Doughnut.prototype.draw.apply(this, arguments);

    var width = this.doughnutCanvas.nativeElement.clientWidth,
    height = this.doughnutCanvas.nativeElement.clientHeight;
    let legendWidth = 68;

    var fontSize = (height / 80).toFixed(2);
    ctx.font = fontSize + "em Verdana";
    ctx.textBaseline = "top";
    ctx.fillStyle = "grey";

    var text = "Total",
        textX = Math.round((width - ctx.measureText(text).width) / 2) + legendWidth,
        textY = height / 2 - 10;

    ctx.fillText(text, textX, textY, 200);

    var fontSize = (height / 90).toFixed(2);
    ctx.font = fontSize + "em Verdana";
    ctx.textBaseline = "bottom";
    ctx.fillStyle = "grey";

    var text1 = this.totalCampaign,
        textX = Math.round((width - ctx.measureText(text1).width) / 2) + legendWidth,
        textY = height / 2 + 40; 

    ctx.fillText(text1, textX, textY, 200);
    ctx.restore();
   }


   /**
   * Open Influncer Detail modal
   */
  influncerDetailModal(influencerDetailsModal: TemplateRef<any>, influncerId){
    console.log(influncerId)
    this.getInfluncerById(influncerId)
    this.openModal(influencerDetailsModal, "customModal modal-xlg");
  }

  /**
   * Get Influncer detail by Id
   */
  getInfluncerById(influncerId){
    this.InfluncerService.getInfluncerById(influncerId).subscribe(
      (response: any) => {
          
          if (response.responseCode === 200) {
            this.influncerDetail = response.data;
            this.influencerImagePath=(response.influencerImagePath)?response.influencerImagePath:''          
          } else {
              console.log('get page error: ', response.responseDetails);
               
          }
      },
      error => {
          console.log('get page error: ', error);
      }
    );
  }

   /**
   * calculate the sum of all social media followers
   */
  getTotalFollowers(platform){
    var sum = 0; 
    for(let i=0; i<platform.length; i++){
      sum += platform[i].followers
    }

    return sum;
  }
  
  /**
   * Medthod to open modal function
   * @param template
   * @param classR
   */
  openModal(template: TemplateRef<any>, classR: string) {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: "customModal " + classR })
    );
  }

  /**
   * Method to close modal
   */
  closeModal() {
    this.modalService._hideModal(1);
  }

  // onGoingCampaigns: [
  //   {
  //     "campainName": "ABCRX",
  //     "influencerName": [
  //       'assets/front_end/images/testimonial_img.jpg',
  //       'assets/front_end/images/testimonial_img.jpg'
  //     ],
  //     "platForm": ""
  //   }
  // ]

}
