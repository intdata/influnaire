import { Component, OnInit, Output, EventEmitter, HostListener, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import {smoothlyMenu} from "../app.helpers";
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";
import { filter, pairwise } from 'rxjs/operators';
import { SidebarComponent } from './../sidebar/sidebar.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public userName: string = '';
  public profileImage: string = '';
  previousUrl: string;
  
  // public isMenuCollapsed = 1;
  // public toggleMenu: boolean = false;
  @Output() change: EventEmitter<boolean> = new EventEmitter();
  @HostListener('click')
  click() {
      this.cryptoProvider.toggle();
      this.change.emit(true);
  }

 

  constructor(
    private cryptoProvider: CryptoProvider,
    private router: Router,
    private activateRote :ActivatedRoute,
  ) {


    router.events.subscribe((val) => {
      // see also 
      if(val instanceof NavigationEnd){
        this.cryptoProvider.hide();
        //this.cryptoProvider.toggle();
        // this.change.emit(true);
      }
    }); 

    // router.events.subscribe((val) => {
    //   if(val instanceof NavigationEnd){
    //     console.log(val);
    //   }
    // });
    
    // router.events
    // .filter(event => event instanceof NavigationEnd)
    // .subscribe(e => {
    //   console.log('prev:', e);
    //   //this.previousUrl = e.url;
    // });

    // router.events
    // .pipe(filter((evt: any) => evt instanceof RoutesRecognized), pairwise())
    // .subscribe((events: RoutesRecognized[]) => {
    //   console.log('previous url', events[0].urlAfterRedirects);
    //   console.log('current url', events[1].urlAfterRedirects);
    // });

   }


  ngOnInit() {
    this.userName = this.cryptoProvider.decrypt(localStorage.getItem('name'));
    this.profileImage = this.cryptoProvider.decrypt(localStorage.getItem('profileImage'));
  }
  
//   toggleNavigation(): void {
//     // jQuery("body").toggleClass("mini-navbar");
//     this.isOpen = !this.isOpen;

//     smoothlyMenu();
    
// }
logout() {
    localStorage.clear();
    // location.href='http://to_login_page';
}

// mobileNav(){
//   this.toggleMenu = true;
// }


}
 