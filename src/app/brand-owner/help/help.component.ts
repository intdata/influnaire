import { Component, OnInit, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";

// services
import { HelpCenterService } from './../services/help-center.service';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent implements OnInit {

    public allQuestionAnswer: any = [];
    public searchText: any = '';
    public modalRef: BsModalRef;
    public talkToUsForm: FormGroup;
    public submittedTalkToUsInfoForm: boolean = false;

    constructor(
        private toastr: ToastrService,
        private modalService: BsModalService,
        private helpCenterService: HelpCenterService,
        private formBuilder: FormBuilder, 
        public cryptoProvider: CryptoProvider
    ) {}

    ngOnInit() {
        this.talkToUsForm = this.formBuilder.group({
            type: ['', [Validators.required]],
            name: ['', [Validators.required]],
            email: ['', [Validators.required, Validators.pattern(/^[a-z0-9!#$%&'*+/=?^_`{|}~-].{0,63}(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]{1})*@(?:[a-z](?:[a-z0-9-].{0,252}[a-z0-9])?\.)+([a-z]{2,})$/)]],
            subject: ['', [Validators.required]],
            message: ['', [Validators.required]],
        });

        this.talkToUsForm.controls['name'].setValue(this.cryptoProvider.decrypt(localStorage.getItem('name')));
        this.talkToUsForm.controls['email'].setValue(localStorage.getItem('email'));
        this.talkToUsForm.controls['type'].setValue('brandOwner');

        this.getAllHelp(); // get all help data
    }

    // convenience getter for easy access to form fields
    get fTalkToUsInfo() {
        return this.talkToUsForm.controls;
    }

    /**
     * Method to get all questions and answers
     */
    getAllHelp(){
        // call service to get questions and answers list
        this.helpCenterService.getAllQuestionAnswer('en').subscribe(
            (response: any) => {
                //console.log(response);
                
                if (response.responseCode === 200) {
                    this.allQuestionAnswer = response.faq;
                    //this.tog()
                } else {
                    this.toastr.info(response.responseDetails, 'Info!');
                }
            },
            error => {
                this.toastr.info(error, 'Info!');
            }
        );
    }

    /**
     * Method to send talk to us data
     * @param talkToUsFormData 
     */
    saveTalkToUsAction(talkToUsFormData: FormGroup){
        this.submittedTalkToUsInfoForm = true;

        if(this.talkToUsForm.invalid){
            return;
        }

        // call service to sign up
        this.helpCenterService.talkToUsSubmit(talkToUsFormData).subscribe(
            (response: any) => {
                //console.log(response);
                
                if (response.responseCode === 200) {
                    this.toastr.success(response.responseDetails, 'Success!'); 
                    this.talkToUsForm.reset();
                    this.closeModal();
                } else {
                    this.toastr.info(response.responseDetails, 'Info!');
                }
                this.submittedTalkToUsInfoForm = false;
            },
            error => {
                this.toastr.error('Something went wrong.', 'Error!');
                this.submittedTalkToUsInfoForm = false;
            });
    }

    /**
     * Method to open change password modal
     * @param changeTalkToUsModal 
     */
    changeTalkToUsModal(talkToUsModal: TemplateRef<any>){
        this.openModal(talkToUsModal, "faq_popup modal-md");
    }

    /**
     * Medthod to open modal function
     * @param template
     * @param classR
     */
    openModal(template: TemplateRef<any>, classR: string) {
        this.modalRef = this.modalService.show(
        template,
        Object.assign({}, { class: "customModal " + classR })
        );
    }

    /**
     * Method to close modal
     */
    closeModal() {
        this.modalService._hideModal(1);
    }

}
