import { Component, OnInit, ViewChild, ElementRef, Renderer2, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { InfluncerService } from "./../services/influncer.service";
import { SharedService } from "./../services/shared.service";

@Component({
  selector: 'app-influencer-saved',
  templateUrl: './influencer-saved.component.html',
  styleUrls: ['./influencer-saved.component.scss']
})
export class InfluencerSavedComponent implements OnInit {

  public compareItems: Array<any>;
  public compareArr: Array<any>;
  public expandMode: boolean;
  public expandCampaign: boolean;
  influncers: any = [];
  influencerImagePath:any;
  public addToCompare: any=[];
  public addToCompareImage: any=[];
  totalCampaign: number =0;
  public modalRef: BsModalRef;
  public savedInfluncerId;
  step = (localStorage.getItem('stepCompleted'))?localStorage.getItem('stepCompleted'):1;
  public addedinfluncer: any = (localStorage.getItem('selectedInfluncers'))?localStorage.getItem('selectedInfluncers').split(","):[];
  public influncersImage:any=(localStorage.getItem('selectedInfluncerImage'))?JSON.parse(localStorage.getItem('selectedInfluncerImage')):[];
  public influncerDetail =[];
  public removeName:any =''

  @ViewChild('expanDiv') expanDiv: ElementRef;
  @ViewChild('expanFixd') expanFixd: ElementRef;
 
  constructor(
    private renderer: Renderer2,
    private route: Router,
    private modalService: BsModalService,
    private InfluncerService: InfluncerService,
    private sharedService:SharedService
  ) {
    this.expandMode = false;
    this.expandCampaign = false;
  }

  ngOnInit() {
    localStorage.setItem('searchCompare', this.addToCompare);
    this._staticCompareArray();
    this.getSavedInfluncer()
    if(this.addedinfluncer.length){
      this.totalCampaign = this.addedinfluncer.length;
    }
  }
  _staticCompareArray() {
    this.compareItems = [];
    this.compareArr = [];
    for (let i = 0; i < 4; i++) {
      this.compareItems.push({
        added: false,
        campaign: false,
      })
    }
  }

  /**
   * Get All Saved influncers for the brand Owner
   */
  getSavedInfluncer(){
    this.InfluncerService.getSavedInfluncers().subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
              this.influncers = response.data;
              this.influencerImagePath=(response.influencerImagePath)?response.influencerImagePath:''
              console.log(this.influencerImagePath)
          } else {
              console.log('get page error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get page error: ', error);
      }
   );
  }

  removeSavedModal(confirmRemovedSaved, savedInfluncerId, firstName, lastName){
    this.removeName = firstName+' '+lastName;
    this.savedInfluncerId = savedInfluncerId;
    this.openModal(confirmRemovedSaved, "modal-sm");
  }

  /**
   * Remove selected influncer form campaign
   */
  removeSelectedCampaign(indx){
    console.log(indx);
    this.addedinfluncer.splice(indx, 1)
    this.influncersImage.splice(indx, 1)
    this.totalCampaign = this.addedinfluncer.length;
    localStorage.setItem('selectedInfluncers', this.addedinfluncer);
    localStorage.setItem('selectedInfluncerImage', this.influncersImage);
  }
  

  /** Removed Saved influncer */
  removeSaved(){
    let savedInfluncerId = this.savedInfluncerId;
    this.InfluncerService.removeSavedInfluncer(savedInfluncerId).subscribe(
      (response: any) => {
          
          if (response.responseCode === 200) {
              this.getSavedInfluncer();
              this.closeModal();
          } else {
              console.log('get page error: ', response.responseDetails);
              this.closeModal();
          }
      },
      error => {
          console.log('get page error: ', error);
          this.closeModal();
      }
    );
  }

  /**
   * Add influncer to campaing 
   */
  addCampaign(indx, img, fname, lname){
    //console.log(this.addedinfluncer)
    if(this.addedinfluncer.indexOf(indx) == -1 && this.addedinfluncer.length <4){
      let influncer_data ={}
      influncer_data['id'] = indx;
      influncer_data['img'] = img;
      influncer_data['first_name'] = fname;
      influncer_data['last_name'] = lname;
      this.addedinfluncer.push(indx)
      this.influncersImage.push(influncer_data)
      localStorage.setItem('selectedInfluncers', this.addedinfluncer);
      localStorage.setItem('selectedInfluncerImage', JSON.stringify(this.influncersImage));
    }    
    this.totalCampaign = this.addedinfluncer.length;    
  }

  /**
   * Remove selected influncer form campaign
   */
  removeCampaignSelectedInfluncer(indx){
    
    let infIndex = this.addedinfluncer.indexOf(indx.toString())
    let imgIndex = this.influncersImage.findIndex(obj => obj.id==indx);
    
    this.addedinfluncer.splice(infIndex, 1)
    this.influncersImage.splice(imgIndex, 1)
    this.totalCampaign = this.addedinfluncer.length;
    localStorage.setItem('selectedInfluncers', this.addedinfluncer);
    localStorage.setItem('selectedInfluncerImage', JSON.stringify(this.influncersImage));
  }

  addCompare(indx, first_name, last_name, image) {
    if(this.addToCompare.indexOf(indx) ==-1 && this.addToCompare.length <4){
      this.addToCompare.push(indx)
      let influncer_data ={}
      influncer_data['id'] = indx;
      influncer_data['img'] = image;
      influncer_data['first_name'] = first_name;
      influncer_data['last_name'] = last_name;
      this.addToCompareImage.push(influncer_data)
      this.sharedService.compareInfluncer = this.addToCompare
    }
  }

  /**Remove From Compare */
  removeCompare(indx){
    let infIndex = this.addToCompare.indexOf(indx.toString())
    let imgIndex = this.addToCompareImage.findIndex(obj => obj.id==indx);
    this.addToCompare.splice(infIndex, 1)
    this.addToCompareImage.splice(imgIndex, 1)
    this.sharedService.compareInfluncer = this.addToCompare
  }

  get totalCompare() {
    return this.compareItems.filter(added => added.added).length;
  }
  toggleSlide() {
    if (!this.expandMode) {
      this.renderer.setStyle(this.expanDiv.nativeElement, 'margin-right', `0`);
      this.expandMode = true;
    } else {
      this.renderer.setStyle(this.expanDiv.nativeElement, 'margin-right', `-225px`);
      this.expandMode = false;
    }
  }

  openCampaign() {
    if (!this.expandCampaign && this.totalCampaign > 0) {
      this.renderer.addClass(this.expanFixd.nativeElement, 'activeSlide');
      this.expandCampaign = true;
    } else {
      this.renderer.removeClass(this.expanFixd.nativeElement, 'activeSlide');
      this.expandCampaign = false;
    }
  }

   /**
   * Open Influncer Detail modal
   */
  influncerDetailModal(influencerDetailsModal: TemplateRef<any>, influncerId){
    console.log(influncerId)
    this.getInfluncerById(influncerId)
    this.openModal(influencerDetailsModal, "customModal modal-xlg");
  }

  /**
   * Get Influncer detail by Id
   */
  getInfluncerById(influncerId){
    this.InfluncerService.getInfluncerById(influncerId).subscribe(
      (response: any) => {
          
          if (response.responseCode === 200) {
            this.influncerDetail = response.data;
            this.influencerImagePath=(response.influencerImagePath)?response.influencerImagePath:''          
          } else {
              console.log('get page error: ', response.responseDetails);
               
          }
      },
      error => {
          console.log('get page error: ', error);
      }
    );
  }

  /**
   * calculate the sum of all social media followers
   */
  getTotalFollowers(platform){
    var sum = 0; 
    for(let i=0; i<platform.length; i++){
      sum += platform[i].followers
    }

    return sum;
  }

  /**
   * Medthod to open modal function
   * @param template
   * @param classR
   */
  openModal(template: TemplateRef<any>, classR: string) {
    this.modalRef = this.modalService.show(
    template,
    Object.assign({}, { class: "customModal " + classR })
    );
  }

  /**
   * Method to close modal
   */
  closeModal() {
      this.modalService._hideModal(1);
  }

}
