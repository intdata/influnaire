import { Component, OnInit, ViewChild, ElementRef, Renderer2, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { InfluncerService } from "./../services/influncer.service";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { PlatformService } from "./../services/platform.service";
import { SharedService } from "./../services/shared.service";
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl, AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-influencer-search',
  templateUrl: './influencer-search.component.html',
  styleUrls: ['./influencer-search.component.scss']
})
export class InfluencerSearchComponent implements OnInit {

  
  public compareItems: Array<any>;
  public compareArr: Array<any>;
  public expandMode: boolean;
  public expandCampaign: boolean;
  public modalRef: BsModalRef;
  @ViewChild('expanDiv') expanDiv: ElementRef;
  @ViewChild('expanFixd') expanFixd: ElementRef;
  influncers: any = [];
  countries: any = [];
  industries: any = [];
  public addToCompare: any=[];
  public addToCompareImage: any=[];
  public allPlatforms: any = [];
  public distPlatform:any =[];
  public addedinfluncer: any = (localStorage.getItem('selectedInfluncers'))?localStorage.getItem('selectedInfluncers').split(","):[];
  public influncersImage:any=(localStorage.getItem('selectedInfluncerImage'))?JSON.parse(localStorage.getItem('selectedInfluncerImage')):[];
  totalCampaign: number =0;
  step = (localStorage.getItem('stepCompleted'))?localStorage.getItem('stepCompleted'):1;
  influencerImagePath:any;
  public influncerDetail: any ={};
  public influencerPlatformMaps:any;

  //Sav button Hide sections
  public hideInfluncers = []
  
  searchForm = new FormGroup({
    email: new FormControl(''),
    gender: new FormControl('null'),
    age: new FormControl('null'),
    num_followers: new FormControl('null'),
    category: new FormControl('null'),
    country: new FormControl('null'),
    nationality: new FormControl('null'),
    platform_targeted: new FormControl('null'),
  });

  constructor(
    private renderer: Renderer2,
    private route: Router,
    private InfluncerService: InfluncerService,
    private PlatformService:PlatformService,
    private modalService: BsModalService,
    private sharedService:SharedService
  ) {
    this.expandMode = false;
    this.expandCampaign = false;
  }

  ngOnInit() {

    console.log(this.influncersImage)
    this.countryList();
    this.industryList();
    this.filterInfluncer()
    this._staticCompareArray();
    this.getSavedInfluncers();
    this.getplatforms();
    if(this.addedinfluncer.length){
      this.totalCampaign = this.addedinfluncer.length;
    }
    if(this.step != 1){
      this.sharedService.setConfRedirect(false)
    }
  }
  clearSearch(){
    this.searchForm = new FormGroup({
      email: new FormControl(''),
      gender: new FormControl('null'),
      age: new FormControl('null'),
      num_followers: new FormControl('null'),
      category: new FormControl('null'),
      country: new FormControl('null'),
      nationality: new FormControl('null'),
      platform_targeted: new FormControl('null'),
    });
    //this.filterInfluncer()
  }
  //get f() { return this.searchForm.controls; }
  countryList(){
    this.InfluncerService.countries().subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
              this.countries = response.countries;
              //console.log(response.countries)
          } else {
              console.log('get page error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get page error: ', error);
      }
   );
  }
  industryList(){
    this.InfluncerService.industries().subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
              this.industries = response.industries;
              //console.log(response.countries)
          } else {
              console.log('get page error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get page error: ', error);
      }
   );
  }

  /**
   * Get Platform List
   */
  getplatforms(){
    this.PlatformService.getPlatformsNormal().subscribe(
      (response: any) => {
                  
          if (response.responseCode === 200) {
            this.allPlatforms = response.data;
            if(this.allPlatforms.length){
              for(let i=0; i< this.allPlatforms.length; i++){
                this.distPlatform[this.allPlatforms[i].id]=this.allPlatforms[i]
              }
            }
            console.log(this.distPlatform);
          } else {
              console.log('Campaign create error: ', response.responseDetails);
          }
      },
      error => {
          console.log('Campaign Create error: ', error);
      }
     );
  }

  filterInfluncer(){
    var data = this.searchForm.value;
    this.InfluncerService.getData(data).subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
              this.influncers = response.data;
              this.influencerImagePath=(response.influencerImagePath)?response.influencerImagePath:''
              this.clearSearch();
              console.log(this.influencerImagePath)
          } else {
              console.log('get page error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get page error: ', error);
      }
   );
    //console.log(this.searchForm.value);
  }
  _staticCompareArray() {
    this.compareItems = [];
    this.compareArr = [];
    for (let i = 0; i < 4; i++) {
      this.compareItems.push({
        added: false,
        campaign: false,
      })
    }
  }

  /**
   * Add influncer to campaing 
   */
  addCampaign(indx, img, fname, lname){
    //console.log(this.addedinfluncer)
    if(this.addedinfluncer.indexOf(indx) == -1 && this.addedinfluncer.length <4){
      let influncer_data ={}
      influncer_data['id'] = indx;
      influncer_data['img'] = img;
      influncer_data['first_name'] = fname;
      influncer_data['last_name'] = lname;
      this.addedinfluncer.push(indx)
      this.influncersImage.push(influncer_data)
      localStorage.setItem('selectedInfluncers', this.addedinfluncer);
      localStorage.setItem('selectedInfluncerImage', JSON.stringify(this.influncersImage));
    }    
    this.totalCampaign = this.addedinfluncer.length;    
  }

  /**
   * Remove selected influncer form campaign
   */
  removeCampaignSelectedInfluncer(indx){
    
    let infIndex = this.addedinfluncer.indexOf(indx.toString())
    let imgIndex = this.influncersImage.findIndex(obj => obj.id==indx);
    
    this.addedinfluncer.splice(infIndex, 1)
    this.influncersImage.splice(imgIndex, 1)
    this.totalCampaign = this.addedinfluncer.length;
    localStorage.setItem('selectedInfluncers', this.addedinfluncer);
    localStorage.setItem('selectedInfluncerImage', JSON.stringify(this.influncersImage));
  }

  /**
   * Save influncer for the brandowner
   * @param indx 
   */
  saveInfluncer(influncer_id = ''){
    this.hideInfluncers.push(influncer_id)
    this.InfluncerService.saveInfluncer(influncer_id).subscribe(
      (response: any) => {
          
          if (response.responseCode === 200) {
              this.getSavedInfluncers()
          } else {
              console.log('get page error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get page error: ', error);
      }
    );
  }
  
  /**
   * Get Already Save Influncers 
  */
  getSavedInfluncers(){

    this.InfluncerService.getSavedInfluncerIds().subscribe(
      (response: any) => {
          
          if (response.responseCode === 200) {
            var data = response.data;
            if(data.length){
              for(let i=0; i< data.length;i++){
                this.hideInfluncers.push(data[i].influencer_id)
              }
            }
          } else {
              console.log('get page error: ', response.responseDetails);
               
          }
      },
      error => {
          console.log('get page error: ', error);
      }
    );
  }

  addCompare(indx, first_name, last_name, image) {
    if(this.addToCompare.indexOf(indx) ==-1 && this.addToCompare.length <4){
      this.addToCompare.push(indx)
      let influncer_data ={}
      influncer_data['id'] = indx;
      influncer_data['img'] = image;
      influncer_data['first_name'] = first_name;
      influncer_data['last_name'] = last_name;
      this.addToCompareImage.push(influncer_data)
      this.sharedService.compareInfluncer = this.addToCompare
    }
  }

  /**Remove From Compare */
  removeCompare(indx){
    let infIndex = this.addToCompare.indexOf(indx.toString())
    let imgIndex = this.addToCompareImage.findIndex(obj => obj.id==indx);
    this.addToCompare.splice(infIndex, 1)
    this.addToCompareImage.splice(imgIndex, 1)
    this.sharedService.compareInfluncer = this.addToCompare
  }

  get totalCompare() {
    return this.compareItems.length;
  }
  toggleSlide() {
    if (!this.expandMode) {
      this.renderer.setStyle(this.expanDiv.nativeElement, 'margin-right', `0`);
      this.expandMode = true;
    } else {
      this.renderer.setStyle(this.expanDiv.nativeElement, 'margin-right', `-225px`);
      this.expandMode = false;
    }
  }

  openCampaign() {
    if (!this.expandCampaign && this.totalCampaign > 0) {
      this.renderer.addClass(this.expanFixd.nativeElement, 'activeSlide');
      this.expandCampaign = true;
    } else {
      this.renderer.removeClass(this.expanFixd.nativeElement, 'activeSlide');
      this.expandCampaign = false;
    }
  }

   /**
   * Open Influncer Detail modal
   */
  influncerDetailModal(influencerDetailsModal: TemplateRef<any>, influncerId){
    this.getInfluncerById(influncerId)
    this.openModal(influencerDetailsModal, "customModal modal-xlg");
  }

  /**
   * calculate the sum of all social media followers
   */
  getTotalFollowers(platform){
    var sum = 0; 
    for(let i=0; i<platform.length; i++){
      sum += platform[i].followers
    }

    return sum;
  }

  /**
   * Get Influncer detail by Id
   */
  getInfluncerById(influncerId){
    this.InfluncerService.getInfluncerById(influncerId).subscribe(
      (response: any) => {
          
          if (response.responseCode === 200) {
            console.log(response.data)
            this.influncerDetail = response.data;
            this.influencerPlatformMaps = response.data.influencerPlatformMaps
            this.influencerImagePath=(response.influencerImagePath)?response.influencerImagePath:''          
          } else {
              console.log('get page error: ', response.responseDetails);
               
          }
      },
      error => {
          console.log('get page error: ', error);
      }
    );
  }

  /**
   * Medthod to open modal function
   * @param template
   * @param classR
   */
  openModal(template: TemplateRef<any>, classR: string) {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: "customModal " + classR })
    );
  }

  /**
   * Method to close modal
   */
  closeModal() {
    this.modalService._hideModal(1);
  }

}
