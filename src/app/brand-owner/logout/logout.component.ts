import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {
  submitted = false;

  constructor(
      private router: Router,
      private toastr: ToastrService,
    ) {}

  ngOnInit() {
    //this.toastr.success("You have successfully loged out.", 'Success!');
    localStorage.setItem("isLoggedinBrandOwner", "false");
    this.router.navigate(['/login']);
  }
}


