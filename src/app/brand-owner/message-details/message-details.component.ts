import { Component, OnInit, ViewChild  } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, ControlContainer, FormGroup, FormControl, AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { MessageService } from "./../services/message.service";
import { CampaignAssign } from "./../services/campaign-assign.service";

@Component({
  selector: 'app-message-details',
  templateUrl: './message-details.component.html',
  styleUrls: ['./message-details.component.scss']
})
export class MessageDetailsComponent implements OnInit {
  @ViewChild('fileInput') fileInput;
  public influncerMessages: any = [];
  public influencerImagePath:string;
  public brandOwnerImagePath:string;
  public filePath:string;
  public msubmitted = false;
  public campaign:any ='';
  public status:any ='';
  public influncer:any ='';
  public brandowner:any ='';
  messageForm: FormGroup;
  public imageFile = ['jpg', 'jpeg', 'png']
  public docFile = ['txt', 'csv', 'xls', 'doc', 'docx']
  public pdfFile = ['pdf']
  public allowStatus = ['ACCEPT', 'SUBMITTED', 'CONFIRM', 'CONTENT_SUBMITTED', 'CONTENT_APPROVED', 'PAID']

  constructor(
    private MessageService: MessageService,
    private CampaignAssign: CampaignAssign,
    private route : ActivatedRoute,
    private formBuilder: FormBuilder
    ) { }

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.campaign = params['campaign'];
      this.influncer =  params['influncer'];
      this.brandowner =  params['brandowner'];
      this.influncerMessage(params['campaign'], params['influncer'])
    } );
    this.messageForm = this.formBuilder.group({
      message: ['', [Validators.required]],
      files: ['']
   });
   this.getCampaignStatus(this.campaign, this.influncer)
      
  }

  get m() { return this.messageForm.controls; }
  /**
   * Get message of an ifluncer for a campaign
   */
  influncerMessage(campaign = '', influncer = ''){
    this.MessageService.getInfluncerMessage(campaign, influncer).subscribe(
      (response: any) => {
                  
          if (response.responseCode === 200) {
            this.influncerMessages = response.data
            this.brandOwnerImagePath = response.brandOwnerImagePath
            this.influencerImagePath = response.influencerImagePath
            this.filePath = response.filePath
            //console.log(response.data)
          } else {
              console.log('Campaign create error: ', response.responseDetails);
          }
      },
      error => {
          console.log('Campaign Create error: ', error);
      }
     ); 
  }

  /**
   * Get Campaign Status
   */
  getCampaignStatus(campaign = '', influncer = ''){
    this.CampaignAssign.getCampaignAssignStatus(campaign, influncer).subscribe(
      (response: any) => {
                  
          if (response.responseCode === 200) {
            this.status = response.data
            //console.log(response.data)
          } else {
              console.log('Campaign create error: ', response.responseDetails);
          }
        });
  }
  
  /**
   * Save message after message enter
   */
  save_message(){
    this.msubmitted=true
    if (this.messageForm.invalid) {
      return;
    }
    const formData = new FormData();
    const fileBrowser = this.fileInput.nativeElement;
      formData.append('up_file', '');
    if (fileBrowser.files && fileBrowser.files[0]) {
      formData.append('up_file', fileBrowser.files[0]);
    }
    formData.append('message',this.messageForm.value.message);
    formData.append('campaign_id',this.campaign);
    formData.append('influencer_id',this.influncer);
    formData.append('brand_owner_id',this.brandowner);
    formData.append('message_from','BrandOwner');
    //console.log(this.messageForm.value)
    this.MessageService.saveMessage(formData).subscribe(
      (response: any) => {
                  
          if (response.responseCode === 200) {
            this.influncerMessage(this.campaign, this.influncer)
            this.msubmitted=false
            this.messageForm.controls["message"].setValue('');
            this.messageForm.controls["files"].setValue('');
            console.log(response.data)
          } else {
              console.log('Campaign create error: ', response.responseDetails);
          }
      },
      error => {
          console.log('Campaign Create error: ', error);
      }
     ); 
  }
}
