import { Component, OnInit, Host } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from "./../services/message.service";
import { AppComponent } from './../../app.component';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {
  public latestMessages:any = [];
  public messCnt:any = [];
  messageConfig: any;
  brandOwnerImagePath:any=''
  influencerImagePath:any = '';
  testData:any

  constructor(
    @Host() private app: AppComponent,
    private MessageService: MessageService
    ) { }

  ngOnInit() {
    
    this.messageConfig = {
      itemsPerPage: this.app.getSettingData('pagination'),
      currentPage: 1,
      totalItems: this.latestMessages.count
    };
    this.getMessageCount()
    this.getLatestMessage()
    
  }

  /**
   * pagination page changed
   */
  messagePageChanged(event){
    this.messageConfig.currentPage = event;
  }

  /**
   * Get Latest Message
   */
  getLatestMessage(){
    this.MessageService.getLatestMessage().subscribe(
      (response: any) => {
                  
          if (response.responseCode === 200) {
            this.latestMessages = response.data
            this.brandOwnerImagePath =(response.brandOwnerImagePath)?response.brandOwnerImagePath:''
            this.influencerImagePath =(response.influencerImagePath)?response.influencerImagePath:''
          } else {
              console.log('Campaign create error: ', response.responseDetails);
          }
      },
      error => {
          console.log('Campaign Create error: ', error);
      }
     );  
  }

  /**
   * Get the message count of individual influncer 
   */
  getMessageCount(){
    this.MessageService.getMessageCount().subscribe(
      (response: any) => {
                  
          if (response.responseCode === 200) {
            var messsageData = {}
            var resData = response.data; 
            for (var i =0; i< response.data.length;i++){
              var d = []
              var c = (resData[i]['commentCnt'])?resData[i]['commentCnt']:0;
              d[resData[i]['influencer_id']] = c
              if(messsageData.hasOwnProperty(resData[i]['campaign_id'])){
                messsageData[resData[i]['campaign_id']][resData[i]['influencer_id']]=c;
              }else{
                messsageData[resData[i]['campaign_id']] =d;
              }
            }
            console.log(messsageData)
            this.messCnt = messsageData
          } else {
              console.log('Campaign create error: ', response.responseDetails);
          }
      },
      error => {
          console.log('Campaign Create error: ', error);
      }
     );  
  }

  test(index){
    this.testData=index
    return '<b>'+index+'</b>';
  }

}
