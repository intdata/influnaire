import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from './../../frontend/must-match.validator';
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";

// services
import { UserService } from "./../services/user.service";
import { CountryService } from "./../services/country.service";
import { IndustryService } from "./../services/industry.service";
import { PlatformService } from "./../services/platform.service";
import { BrandService } from "./../services/brand.service";
import { CurrencyService } from "./../services/currency.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  public showPersonalEditForm:boolean = false;
  public showCompanyEditForm:boolean = false;
  public personalInfo: any = [];
  public companyInfo: any = [];
  //public currencyList: any = [];
  public manageBrandForm: FormGroup;
  public changePasswordForm: FormGroup;
  public changePersonalInfoForm: FormGroup;
  public changeCompanyInfoForm: FormGroup;
  public modalRef: BsModalRef;
  public submittedChangePasswordForm: boolean = false;
  public submittedPersonalInfoForm: boolean = false;
  public submittedCompanyInfoForm: boolean = false;
  public submittedBrandInfoForm: boolean = false;
  public logo;
  public image;
  public brandImagePath;
  public oldBrandImage;
  private filesToUpload: Array<File> = [];
  public allCountries: any[] = [];
  public allIndustries: any[] = [];
  public allPlatforms: any[] = [];
  public allBrands: any = [];
  public editBrandId: any = '';
  public deleteBrandId: any = '';
  public allCurrencies: any[] = [];
  public deleteBrandName: any = '';

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private modalService: BsModalService,
    private cryptoProvider: CryptoProvider,
    private formBuilder: FormBuilder, 
    private userService: UserService,
    private countryService: CountryService,
    private industryService: IndustryService,
    private PlatformService: PlatformService,
    private BrandService:BrandService,
    private CurrencyService:CurrencyService
  ) {}

  
  ngOnInit() {

    this.changePasswordForm = this.formBuilder.group({
        oldPassword: ['', [Validators.required]],
        newPassword: ['', [Validators.required, Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]).{8,20}$/)]],
        confirmPassword: ['', Validators.compose([Validators.required])],
    }, {
        // check whether our password and confirm password match
        validator: MustMatch('newPassword', 'confirmPassword')
    });

    this.changePersonalInfoForm = this.formBuilder.group({
        profileImage: [],
        firstName: ['', [Validators.required]],
        lastName: ['', [Validators.required]],
        title: ['', [Validators.required]],
        email: ['', [Validators.required, Validators.email]],
        phNo: ['', [Validators.required]],
    });

    this.changeCompanyInfoForm = this.formBuilder.group({
        companyLogo: [],
        companyName: ['', [Validators.required]],
        companyType: ['', [Validators.required]],
        currency: ['', [Validators.required]],
        country: ['', [Validators.required]],
        industry: ['', [Validators.required]],
    });

    // get all countries
    this.getAllCountries();
        
    // get all industries
    this.getAllIndustries();

    //get all platforms
    this.getAllPlatforms();

    //Get all brands
    this.getAllBrands();

    // get all currencies
    this.getAllCurrencies();

    // set default values
    this.changeCompanyInfoForm.controls["companyType"].setValue('');

    // call service to get profile info
    this.userService.getBrandOwnerInfo().subscribe(
        (response: any) => {
            //console.log(response);
            
            if (response.responseCode === 200) {
              this.personalInfo = response.data.personalData;
              this.companyInfo = response.data.companyData;
              console.log(this.companyInfo);
              //this.currency = response.data.currencyData;

              // set personal edit form data
              this.changePersonalInfoForm.controls["firstName"].setValue(
                this.personalInfo.firstName
              );
              this.changePersonalInfoForm.controls["lastName"].setValue(
                this.personalInfo.lastName
              );
              this.changePersonalInfoForm.controls["title"].setValue(
                this.personalInfo.title
              );
              this.changePersonalInfoForm.controls["email"].setValue(
                this.personalInfo.email
              );
              this.changePersonalInfoForm.controls["phNo"].setValue(
                this.personalInfo.personalPhNo
              );
              this.image = this.personalInfo.photo;

              // set company edit form data
              this.changeCompanyInfoForm.controls["companyName"].setValue(
                this.companyInfo.companyName
              );
              this.changeCompanyInfoForm.controls["currency"].setValue(
                //this.companyInfo.currency
                this.companyInfo.currency
              );
              this.changeCompanyInfoForm.controls["industry"].setValue(
                this.companyInfo.industry
              );
              this.changeCompanyInfoForm.controls["country"].setValue(
                this.companyInfo.countryCode
              );
              this.changeCompanyInfoForm.controls["companyType"].setValue(
                this.companyInfo.companyType
              );
              this.logo = this.companyInfo.companyLogo;

            } else {
                this.toastr.info(response.responseDetails, 'Info!');
            }
        },
        error => {
            this.toastr.error('Something went wrong.', 'Error!');
        }
    );
  }

  editCompanyInfo(){
    console.log(this.companyInfo.currency);
    this.showCompanyEditForm = !this.showCompanyEditForm;
    if(!this.showCompanyEditForm){
      this.logo = this.companyInfo.companyLogo;
      this.changeCompanyInfoForm.controls['companyLogo'].setValue('');
    }

    this.changeCompanyInfoForm.controls["companyName"].setValue(
      this.companyInfo.companyName
    );
    this.changeCompanyInfoForm.controls["currency"].setValue(
      //this.companyInfo.currency
      this.companyInfo.currency
    );
    this.changeCompanyInfoForm.controls["industry"].setValue(
      this.companyInfo.industry
    );
    this.changeCompanyInfoForm.controls["country"].setValue(
      this.companyInfo.countryCode
    );
    this.changeCompanyInfoForm.controls["companyType"].setValue(
      this.companyInfo.companyType
    );
  }

  editPersonalInfo(){
    this.showPersonalEditForm = !this.showPersonalEditForm;
    if(!this.showPersonalEditForm){
      this.image = this.personalInfo.photo;
      this.changePersonalInfoForm.controls['profileImage'].setValue('');
    }
    this.changePersonalInfoForm.controls["firstName"].setValue(
      this.personalInfo.firstName
    );
    this.changePersonalInfoForm.controls["lastName"].setValue(
      this.personalInfo.lastName
    );
    this.changePersonalInfoForm.controls["title"].setValue(
      this.personalInfo.title
    );
    this.changePersonalInfoForm.controls["email"].setValue(
      this.personalInfo.email
    );
    this.changePersonalInfoForm.controls["phNo"].setValue(
      this.personalInfo.personalPhNo
    );
  }

  /**
   * Method to get control on browse
   * @param $event 
   * @param type 
   */
  changeListener($event: any, type = "logo"): void {
    if ($event) this.readThis($event.target, type);
  }

  /**
   * Method to instant image load on browse
   * @param inputValue 
   * @param type 
   */
  readThis(inputValue: any, type): void {
    try{
      var file: File = inputValue.files[0];
      this.filesToUpload = <Array<File>>inputValue.files;
      var myReader: FileReader = new FileReader();

      myReader.onloadend = e => {
        if (type == "image") {
          this.image = myReader.result;
          //this.personalInfo.photo = this.image; //for image browes preview
        } else {
          this.logo = myReader.result;
          //this.companyInfo.companyLogo = this.logo; //for logo browes preview
        }
      };
      //if(myReader.result)
      myReader.readAsDataURL(file);
    } catch(e){
      if (type == "image") {
        this.image = this.personalInfo.photo;
      } else{
        this.logo = this.companyInfo.companyLogo;
      }
    }
  }


  /**
   * Method to open change password modal
   * @param changePasswordModal 
   */
  changePasswordModal(changePasswordModal: TemplateRef<any>){
    this.openModal(changePasswordModal, "customModal modal-md");
  }

  // convenience getter for easy access to form fields
  get fChangePassword() {
    return this.changePasswordForm.controls;
  }
  get fPersonalInfo() {
    return this.changePersonalInfoForm.controls;
  }
  get fCompanyInfo() {
    return this.changeCompanyInfoForm.controls;
  }
  get fBrandInfo() {
    return this.manageBrandForm.controls;
  }
  get fCurrencyInfo() {
    return this.changeCompanyInfoForm.controls;
  }

  /**
   * Method to change personal info
   * @param changePersonalInfoForm 
   */
  savePersonalInfoFormAction(changePersonalInfoForm: FormGroup){
    this.submittedPersonalInfoForm = true;

    if(this.changePersonalInfoForm.invalid){
      return;
    }

    let profileData: any = new FormData();
    const files: Array<File> = this.filesToUpload;

    for (let key in changePersonalInfoForm) {
      if (key == "profileImage") {
        if (files.length > 0) {
          profileData.append(key, files[0], files[0]['name']);
        } else {
          profileData.append(key, "");
        }
      } else {
        profileData.append(key, changePersonalInfoForm[key]);
      }
    }

    // call service to sign up
    this.userService.changePersonalInfo(profileData).subscribe(
        (response: any) => {
            //console.log(response);
            
            if (response.responseCode === 200) {
                this.toastr.success(response.responseDetails, 'Success!');   
                this.personalInfo = response.data.personalData;
                this.companyInfo = response.data.companyData;

                // set form
                this.changePersonalInfoForm.controls["profileImage"].setValue('');
                this.changePersonalInfoForm.controls["firstName"].setValue(
                  this.personalInfo.firstName
                );
                this.changePersonalInfoForm.controls["lastName"].setValue(
                  this.personalInfo.lastName
                );
                this.changePersonalInfoForm.controls["title"].setValue(
                  this.personalInfo.title
                );
                this.changePersonalInfoForm.controls["email"].setValue(
                  this.personalInfo.email
                );
                this.changePersonalInfoForm.controls["phNo"].setValue(
                  this.personalInfo.personalPhNo
                );

                // reset local storage
                localStorage.setItem("name", this.cryptoProvider.encrypt(this.personalInfo.firstName));
                localStorage.setItem("profileImage", this.cryptoProvider.encrypt(this.personalInfo.photo));

                this.showPersonalEditForm = false;
                this.router
                  .navigateByUrl("/", { skipLocationChange: true })
                  .then(() => this.router.navigate(["brand-owner/profile"]));
            } else {
                this.toastr.info(response.responseDetails, 'Info!');
            }
        },
        error => {
            this.toastr.error('Something went wrong.', 'Error!');
        }
    );

  }

  /**
   * Method to change company info
   * @param changeCompanyInfoForm 
   */
  saveCompanyInfoFormAction(changeCompanyInfoForm: FormGroup){
    this.submittedCompanyInfoForm = true;

    if(this.changeCompanyInfoForm.invalid){
      return;
    }

    let companyData: any = new FormData();
    const files: Array<File> = this.filesToUpload;

    for (let key in changeCompanyInfoForm) {
      if (key == "companyLogo") {
        if (files.length > 0) {
          companyData.append(key, files[0], files[0]['name']);
        } else {
          companyData.append(key, "");
        }
      } else {
        companyData.append(key, changeCompanyInfoForm[key]);
      }
    }

    // call service to sign up
    this.userService.changeCompanyInfo(companyData).subscribe(
        (response: any) => {
            //console.log(response);
            
            if (response.responseCode === 200) {
                this.toastr.success(response.responseDetails, 'Success!');   
                this.personalInfo = response.data.personalData;
                this.companyInfo = response.data.companyData;

                // set form
                this.changeCompanyInfoForm.controls["companyLogo"].setValue('');
                this.changeCompanyInfoForm.controls["companyName"].setValue(
                  this.companyInfo.companyName
                );
                this.changeCompanyInfoForm.controls["currency"].setValue(
                  this.companyInfo.currency
                );
                this.changeCompanyInfoForm.controls["industry"].setValue(
                  this.companyInfo.industry
                );
                this.changeCompanyInfoForm.controls["country"].setValue(
                  this.companyInfo.countryCode
                );
                this.changeCompanyInfoForm.controls["companyType"].setValue(
                  this.companyInfo.companyType
                );

                this.showCompanyEditForm = false;
                this.router
                  .navigateByUrl("/DummyComponent", { skipLocationChange: true })
                  .then(() => this.router.navigate(["brand-owner/profile"]));
            } else {
                this.toastr.info(response.responseDetails, 'Info!');
            }
        },
        error => {
            this.toastr.error('Something went wrong.', 'Error!');
        }
    );

  }
  
  /**
   * Method to save new password
   * @param changePasswordForm 
   */
  saveChnagePasswordAction(changePasswordForm: FormGroup){
    this.submittedChangePasswordForm = true;

    //console.log(this.changePasswordForm);
    
    if(this.changePasswordForm.invalid){
      return;
    }
    
    // call service to sign up
    this.userService.changePassword(changePasswordForm).subscribe(
        (response: any) => {
            //console.log(response);
            
            if (response.responseCode === 200) {
                this.toastr.success(response.responseDetails, 'Success!');
                this.changePasswordForm.reset();    
                this.closeModal();
            } else {
                this.toastr.info(response.responseDetails, 'Info!');
            }
            this.submittedChangePasswordForm = false;
        },
        error => {
            this.toastr.error('Something went wrong.', 'Error!');
            this.submittedChangePasswordForm = false;
        }
    );
    
  }

  /**
   * Save Brand DAta
   */
  saveBrandData(saveBrandInfoForm: FormGroup){
    
    this.submittedBrandInfoForm = true
    if(this.manageBrandForm.invalid){
      return;
    }

    let brandData: any = new FormData();
    const files: Array<File> = this.filesToUpload;

    for (let key in saveBrandInfoForm) {
      if (key == "logo") {
        if (files.length > 0) {
          brandData.append(key, files[0], files[0]['name']);
        } else {
          brandData.append(key, "");
        }
      } else {
        brandData.append(key, saveBrandInfoForm[key]);
      }
    }
    if(this.editBrandId){
      brandData.append('id', this.editBrandId);
      brandData.append('oldImage', this.oldBrandImage);
      this.BrandService.editBrands(brandData).subscribe(
        (response: any) => {
            //console.log(response);
            
          if (response.responseCode === 200) {
              this.toastr.success(response.responseDetails, 'Success!');
              //this.manageBrandForm.reset();
              this.manageBrandForm.controls["logo"].setValue('');
              this.manageBrandForm.controls["platform"].setValue('');
              this.manageBrandForm.controls["brandName"].setValue('');
              //set the brand form
              this.manageBrandForm.controls["industry"].setValue(
                this.companyInfo.industry
              );
              this.manageBrandForm.controls["country"].setValue(
                this.companyInfo.countryCode
              );

              this.closeModal();
              this.getAllBrands();
          } else {
              this.toastr.info(response.responseDetails, 'Info!');
          }
          this.submittedBrandInfoForm = false;
          },
          error => {
              this.toastr.error('Something went wrong.', 'Error!');
              this.submittedBrandInfoForm = false;
          }
        );
    }else{
      this.BrandService.saveBrands(brandData ).subscribe(
        (response: any) => {
            //console.log(response);
            
          if (response.responseCode === 200) {
              this.toastr.success(response.responseDetails, 'Success!');
              //this.manageBrandForm.reset(); 
              this.manageBrandForm.controls["logo"].setValue('');
              this.manageBrandForm.controls["platform"].setValue('');
              this.manageBrandForm.controls["brandName"].setValue('');
              //set the brand form
              this.manageBrandForm.controls["industry"].setValue(
                this.companyInfo.industry
              );
              this.manageBrandForm.controls["country"].setValue(
                this.companyInfo.countryCode
              );
              this.getAllBrands();   
              this.closeModal();
          } else {
              this.toastr.info(response.responseDetails, 'Error!');
              this.closeModal();
          }
          this.submittedBrandInfoForm = false;
          },
          error => {
              this.toastr.error('Something went wrong.', 'Error!');
              this.submittedBrandInfoForm = false;
          }
        );
      }
  }
  /**
   * Method to get all active countries
   */
  getAllCountries(){
    // call service to get country list
    this.countryService.getAllCountries().subscribe(
        (response: any) => {
            //console.log(response);
            
            if (response.responseCode === 200) {
                this.allCountries = response.countries;
            } else {
                console.log('get country list error: ', response.responseDetails);
            }
        },
        error => {
            console.log('get country list error: ', error);
        }
    );
  }

  /**
   * Get all brands
   */
  getAllBrands(){
    this.BrandService.getBrandOfOwner().subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
              this.allBrands = response.data;
              this.brandImagePath =response.brandImagePath;
          } else {
              console.log('get country list error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get country list error: ', error);
      }
    );
  }
  /**
   * Get all platfom 
   */
  getAllPlatforms(){
    this.PlatformService.getPlatformsNormal().subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
              this.allPlatforms = response.data;
          } else {
              console.log('get country list error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get country list error: ', error);
      }
  );
  }

  /**
   * Method to get all active industries
   */
  getAllIndustries(){
      // call service to get industry list
      this.industryService.getAllIndustries().subscribe(
          (response: any) => {
              //console.log(response);
              
              if (response.responseCode === 200) {
                  this.allIndustries = response.industries;
              } else {
                  console.log('get industry list error: ', response.responseDetails);
              }
          },
          error => {
              console.log('get industry list error: ', error);
          }
      );
  }

  /**
   * Method to get all currencies
   */
  getAllCurrencies(){
    // call service to get currency list
    this.CurrencyService.getAllCurrencies().subscribe(
        (response: any) => {
            //console.log(response);
            if (response.responseCode === 200) {
                this.allCurrencies = response.currencies;
            } else {
                console.log('get currency list error: ', response.responseDetails);
            }
        },
        error => {
            console.log('get currency list error: ', error);
        }
    );
  }

  /**
   * Open Modal for brand insert
   */
  submitBrandModal(submitBrandModal: TemplateRef<any>){
    this.editBrandId ='';
    //set the brand form
    this.manageBrandForm = this.formBuilder.group({
      logo: [],
      industry: [this.companyInfo.industry, [Validators.required]],
      country: [this.companyInfo.countryCode, [Validators.required]],
      brandName: ['', [Validators.required]],
      platform: ['', [Validators.required]]
    });
    this.openModal(submitBrandModal, "customModal modal-lg");
  }

  /**
   * Open Modal for brand insert
   */
  editBrandModal(submitBrandModal: TemplateRef<any>, brand_id, brand_owner_id, industry_id, country, brand_name, logo, platform_id){
    this.editBrandId = brand_id;
    this.oldBrandImage = logo;
    this.manageBrandForm = this.formBuilder.group({
        logo: [],
        industry: [industry_id, [Validators.required]],
        country: [country, [Validators.required]],
        brandName: [brand_name, [Validators.required]],
        platform: [platform_id, [Validators.required]]
    });
    this.openModal(submitBrandModal, "customModal modal-lg");
  }

  /**
   * Delete Single brand
   */
  deleteBrand(){
    this.BrandService.delete(this.deleteBrandId).subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
              this.getAllBrands();
              this.toastr.success(response.responseDetails, 'Success!');
              this.closeModal();
          } else {
              console.log('get industry list error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get industry list error: ', error);
      }
    );
  }

  /**
   * Delete brand modal
   */
  deleteBrandModal(confirmDeleteBrand, deleteBrandId, deleteBrandName){
    this.deleteBrandId = deleteBrandId;
    this.deleteBrandName = deleteBrandName;
    this.openModal(confirmDeleteBrand, "modal-md");
  }
  /**
   * Medthod to open modal function
   * @param template
   * @param classR
   */
  openModal(template: TemplateRef<any>, classR: string) {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: "customModal " + classR })
    );
  }

  /**
   * Method to close modal
   */
  closeModal() {
    this.modalService._hideModal(1);
  }

}
