import { Injectable, Inject } from '@angular/core';

@Injectable()
export class AuthServiceBrandOwner {

  constructor() {}

  isLoggedIn(){
    let isLoggedIn = localStorage.getItem("isLoggedinBrandOwner");   
    return isLoggedIn == "true" ? true : false;
  }

  isBlock(){
    let isBlocked = localStorage.getItem("isBlockUrl"); 
    localStorage.setItem('redrictModal', 'yes');
    //console.log(isBlocked)
    if(isBlocked == "block"){
      return false
    }else{
      return true;
    }
  }
  
}
