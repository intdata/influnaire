import { Injectable } from '@angular/core';
import { Constant } from './../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";
import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';
import { FormGroup } from '@angular/forms';

declare var $: any;

@Injectable()
export class BrandService {
  constructor(
    private httpClient: HttpClient,
    private cryptoProvider: CryptoProvider
  ) {
  }

  /**
   * get platform List Normal Behaviour
   */
  saveBrands(data: FormGroup): Observable<any>{
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:data,
      url: Constant.API_BASE_URL + '/brand/save'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }

  /**
   * get brand List 
   */
  editBrands(data: FormGroup): Observable<any>{
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:data,
      url: Constant.API_BASE_URL + '/brand/edit'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }

  /**
   * Delete Single Brand
   */
  delete(deleteBrandId){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{id:deleteBrandId},
      url: Constant.API_BASE_URL + '/brand/delete'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }

  /**
   * Get all brands of a Brand Owner
   */
  getBrandOfOwner(){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{},
      url: Constant.API_BASE_URL + '/brand/brand-of-owner'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }

  /**
   * Get all brands of a Brand Owner
   */
  getBrandOfOwnerList(){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{},
      url: Constant.API_BASE_URL + '/brand/brand-of-owner-list'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }
}
