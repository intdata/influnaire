import { TestBed } from '@angular/core/testing';

import { CampaignAnalysisService } from './campaign-analysis.service';

describe('CampaignAnalysisService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CampaignAnalysisService = TestBed.get(CampaignAnalysisService);
    expect(service).toBeTruthy();
  });
});
