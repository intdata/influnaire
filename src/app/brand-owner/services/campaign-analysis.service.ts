import { Injectable } from '@angular/core';
import { Constant } from './../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";

@Injectable({
  providedIn: 'root'
})
export class CampaignAnalysisService {

  constructor(private httpClient: HttpClient, private cryptoProvider: CryptoProvider) { }

  /**
   * Get campaign payment data for analysis 
   */
  getPaymentAnalysis(campaignId){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{campaignId:campaignId},
      url: Constant.API_BASE_URL + '/campaign/payment-analysis'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }

  /**
   * Get campaign payment data for analysis 
   */
  getFollowerAnalysis(campaignId){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{campaignId:campaignId},
      url: Constant.API_BASE_URL + '/campaign/follower-analysis'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }

  /**
   * Get campaign payment data for analysis 
   */
  getLikeCommentAnalysis(campaignId){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{campaignId:campaignId},
      url: Constant.API_BASE_URL + '/campaign/like-comment-analysis'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }

  /**
   * get Platform of assigned influncer
   */
  getPlatformComparisonAnalysis(campaignId){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{campaignId:campaignId},
      url: Constant.API_BASE_URL + '/campaign/platform-count-analysis'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }
  
}
