import { Injectable } from '@angular/core';
import { Constant } from './../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";
import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';

declare var $: any;

@Injectable()
export class CampaignAssign {
  constructor(
    private httpClient: HttpClient,
    private cryptoProvider: CryptoProvider
  ) {
  }

  /**
   * get platform List
   */
  getCampaignAssignStatus(campaign, influncer){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{campaign:campaign, influncer:influncer},
      url: Constant.API_BASE_URL + '/campaign-assign/status'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }

  /**
   * Change campaign Assign status
   */
  changeStatus(campaign_id, influncer_id, status){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{campaign_id:campaign_id, influncer_id:influncer_id, status:status},
      url: Constant.API_BASE_URL + '/campaign-assign/change-status'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }

  /**
   * Save Rating and review for a campaign
   */
  saveRating(data){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{data:data},
      url: Constant.API_BASE_URL + '/campaign-assign/save-rating'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }
}
