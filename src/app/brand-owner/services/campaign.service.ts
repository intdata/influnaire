import { Injectable } from '@angular/core';
import { Constant } from './../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";

@Injectable()
export class CampaignService {

  
  constructor(private httpClient: HttpClient, private cryptoProvider: CryptoProvider) { 
    
  }

  createCampaign(data={}) : Observable<any> {
    console.log(data)
    //return 1;
    if(localStorage.getItem('campaign_created_id')){
     
      const request = {
        headers: {
          apikey: Constant.APIKEY,
          authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
        },
        params:{data:data, id:localStorage.getItem('campaign_created_id')},
        url: Constant.API_BASE_URL + '/campaign/updateCampaignFirst'
      };

      return this.httpClient.post<any>(
        request.url,
        request.params, 
        { headers: request.headers, params:{} }
      );

    }else{
      const request = {
        headers: {
          apikey: Constant.APIKEY,
          authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
        },
        params:data,
        url: Constant.API_BASE_URL + '/campaign/createCampaign'
      };

      return this.httpClient.post<any>(
        request.url,
        request.params, 
        { headers: request.headers, params:{} }
      );
    }
    
  }

  /**
   * Update campaign second step
   */
  updateCampaignSecond(data={}) : Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{data:data, id:localStorage.getItem('campaign_created_id')},
      url: Constant.API_BASE_URL + '/campaign/updateCampaignSecond'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers, params:{} }
    );
  }

  /**
   * Change campaign status to complete
   */
  completingJob(campaign_id){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{campaignId:campaign_id},
      url: Constant.API_BASE_URL + '/campaign/complete-campaign'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers, params:{} }
    );
  }

  /**
   * Status wise count of campaign
   */
  statusWiseCount(){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{},
      url: Constant.API_BASE_URL + '/campaign/status-wise-count'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers, params:{} }
    );
  }

  /**
   * Update campaign third step
   */
  updateCampaignThird(data) : Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:data,
      url: Constant.API_BASE_URL + '/campaign/updateCampaignThird'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers, params:{} }
    );
  }

  /**
   * Get All Campaign
   */
  getData(type, filter ={}, platformFilter = '', monthYear = '' ): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{type:type, filter:filter, platform_id: platformFilter, monthYear:monthYear},
      url: Constant.API_BASE_URL + '/campaign/getAll'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers, params:{} }
    );
  }
  /**
   * 
   * @param id Get campaign by campaignId
   */
  getDataById(id): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{id:id},
      url: Constant.API_BASE_URL + '/campaign/campaignById'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers, params:{} }
    );
  }

  /**
   * 
   * @param id Remove campaign from draft
   */
  removeDraft(id): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{id:id},
      url: Constant.API_BASE_URL + '/campaign/remove-from-draft'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers, params:{} }
    );
  }

  /**
   * 
   */
  assinInfluncer(params={}, campaignId, type){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{influncers: params, campaignId: campaignId, type:type},
      url: Constant.API_BASE_URL + '/campaign/assignInfluncer'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }
  /**
   * Get message and status fo influncer
   */
  getInfluncerdata(campaign_id, influncer_id){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{influncerId: influncer_id, campaignId: campaign_id},
      url: Constant.API_BASE_URL + '/message/getInfluncerDetailMessage'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }
  
  /**
   * get platform List
   */
  getPlatforms(table='', fields={}){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{table, fields},
      url: Constant.API_BASE_URL + '/platform/list'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }

  /**
   * save brandowner order details
   */
  brandownerPayment(orderdetails: {}){
    console.log(orderdetails);
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{orderdetails},
      url: Constant.API_BASE_URL + '/campaign/createBrandOwnerPayment'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }

     /**
   * start payout from super admin to influencer
   */
  startPayout(id,influencerDeductionPercentage,influencerPaypalCharge,influencerId) : Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{id:id,influencerDeductionPercentage:influencerDeductionPercentage,influencerPaypalCharge:influencerPaypalCharge,influencerId:influencerId},
      url: Constant.API_BASE_URL + '/paypal_payout/payout'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers, params:{} }
    );
  }

   /**
   * update brandowner payment status
   */
  updateBrandownerPayment(orderdetails: {}){
    console.log(orderdetails);
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{orderdetails},
      url: Constant.API_BASE_URL + '/campaign/updateBrandOwnerPayment'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }
}

 

