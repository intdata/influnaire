import { Injectable } from '@angular/core';
import { Constant } from './../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";

@Injectable()
export class InfluncerService {

  
  constructor(private httpClient: HttpClient, private cryptoProvider: CryptoProvider) { 

  }

  getData(params={}): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + '/influncer/getAll',
      params: params
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers}
    );
  }

  /**
   * Total influncer count of a brandowner
   * @param params 
   */
  totalInfluncer(params={}): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + '/influncer/getTotalCount',
      params: params
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers}
    );
  }

  getSelectedInfluncer(params={}): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + '/influncer/getSelected',
      params: {ids: params}
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers}
    );
  }

  /**
   * Selected influncers with Related data
   * @param params 
   */
  getSelectedInfluncerDetail(params={}): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + '/influncer/get-selected-with-relations',
      params: {ids: params}
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers}
    );
  }

  countries(params={}): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY
      },
      url: Constant.API_BASE_URL + '/get-countries',
      params: params
    };
    return this.httpClient.get<any>(
      request.url,
      { headers: request.headers}
    );
  }
  industries(params={}): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY
      },
      url: Constant.API_BASE_URL + '/get-industries',
      params: params
    };
    return this.httpClient.get<any>(
      request.url,
      { headers: request.headers}
    );
  }

  /**
   * Get Proposal Data
   */
  getPropoaslData(invitation_id, campaign_id){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + '/influncer/getPropoaslData',
      params: {invitation_id: invitation_id, campaign_id: campaign_id}
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers}
    );
  }

  /**
   * Save Influncer of a brand Owner
   */
  saveInfluncer(influncer_id){

    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + '/influncer/save-influncer-brandowner',
      params: {influncer_id: influncer_id}
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers}
    );
  }

  /**
   * Get Saved Influncers Ids
  */
 getSavedInfluncerIds(){
  const request = {
    headers: {
      apikey: Constant.APIKEY,
      authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
    },
    url: Constant.API_BASE_URL + '/influncer/get-saved-influncer-ids',
    params: {}
  };
  return this.httpClient.post<any>(
    request.url,
    request.params,
    { headers: request.headers}
  );
 }

 /**
  * Get All Saved influncers
  */
 getSavedInfluncers(){
  const request = {
    headers: {
      apikey: Constant.APIKEY,
      authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
    },
    url: Constant.API_BASE_URL + '/influncer/get-saved-influncers',
    params: {}
  };
  return this.httpClient.post<any>(
    request.url,
    request.params,
    { headers: request.headers}
  );
 }

 /**
  * Remove saved influncer
  */
 removeSavedInfluncer(savedInfluncerId){
   
  const request = {
    headers: {
      apikey: Constant.APIKEY,
      authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
    },
    url: Constant.API_BASE_URL + '/influncer/remove-saved-influncers',
    params: {savedInfluncerId : savedInfluncerId}
  };
  return this.httpClient.post<any>(
    request.url,
    request.params,
    { headers: request.headers}
  );
 }

 /**
  * Get influncer By Campaign 
  */
 getInfluncerByCampaign(campaignId){
  const request = {
    headers: {
      apikey: Constant.APIKEY,
      authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
    },
    url: Constant.API_BASE_URL + '/influncer/get-influncers-by-campaign',
    params: {campaignId:campaignId}
  };
  return this.httpClient.post<any>(
    request.url,
    request.params,
    { headers: request.headers}
  );
 }

 /**
  * Get single influncer by id
  */
 getInfluncerById(influncerId){
  const request = {
    headers: {
      apikey: Constant.APIKEY,
      authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
    },
    url: Constant.API_BASE_URL + '/influncer/get-influncer-by-id',
    params: {influncerId : influncerId}
  };
  return this.httpClient.post<any>(
    request.url,
    request.params,
    { headers: request.headers}
  );
 }

  /**
  * Get single influncer by id
  */
 createPdf(params={}){
  const request = {
    headers: {
      apikey: Constant.APIKEY,
      authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
    },
    url: Constant.API_BASE_URL + '/pdf/create',
    params: {ids: params}
  };
  return this.httpClient.post<any>(
    request.url,
    request.params,
    { headers: request.headers}
  );
 }

}
