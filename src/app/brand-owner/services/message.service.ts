import { Injectable } from '@angular/core';
import { Constant } from './../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";

@Injectable()
export class MessageService {

  
  constructor(private httpClient: HttpClient, private cryptoProvider: CryptoProvider) { 
    
  }
  /**
   * Update campaign third step
   */
  getLatestMessage(): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{},
      url: Constant.API_BASE_URL + '/message/getLatestMessage'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers, params:{} }
    );
  }

  /**
   * Get message of a influncer
   */
  getInfluncerMessage(campaign, influncer): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{campaign:campaign, influncer:influncer},
      url: Constant.API_BASE_URL + '/message/getInfluncerMessage'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }
  /**
   * Get influncer message count
   */
  getMessageCount(): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{},
      url: Constant.API_BASE_URL + '/message/influncerMessageCount'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }

  /**
 * save message data with file
 */
saveMessage(data={}) : Observable<any> {
  const request = {
    headers: {
      apikey: Constant.APIKEY,
      authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
    },
    params:data,
    url: Constant.API_BASE_URL + '/message/saveMessage'
  };
  return this.httpClient.post<any>(
    request.url,
    request.params, 
    { headers: request.headers, params:{} }
  );
}


}
