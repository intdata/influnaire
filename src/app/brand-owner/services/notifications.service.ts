import { Injectable, Inject } from '@angular/core';
import { Constant } from './../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';
import { FormGroup } from '@angular/forms';
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";

@Injectable()
export class NotificationsService {
  private ALL_NOTIFICATIONS_POST_URL:string;
  private DELETE_NOTIFICATIONS_POST_URL:string;
  constructor(
    private httpClient: HttpClient,
    private cryptoProvider: CryptoProvider
  ) {
    this.ALL_NOTIFICATIONS_POST_URL = '/notification/get-all-notifications';
    this.DELETE_NOTIFICATIONS_POST_URL = '/notification/delete-notification';
  }

  /**
   * Service used to get all notifications 
   * @returns {Observable<any>}
   * @memberof NotificationsService
   */
  getNotificationList(conditions: any): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + this.ALL_NOTIFICATIONS_POST_URL,
      params: conditions
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }

  /**
   * Service used to delete selected notifications 
   * @returns {Observable<any>}
   * @memberof NotificationsService
   */
  deleteNotification(data: any): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + this.DELETE_NOTIFICATIONS_POST_URL,
      params: data
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }
}
