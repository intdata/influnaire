import { Injectable } from '@angular/core';
import { Constant } from './../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";
import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';

declare var $: any;

@Injectable()
export class PlatformService {
  constructor(
    private httpClient: HttpClient,
    private cryptoProvider: CryptoProvider
  ) {
  }

  /**
   * get platform List
   */
  getPlatforms(){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{},
      url: Constant.API_BASE_URL + '/platform/getDist'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }

  /**
   * get platform List Normal Behaviour
   */
  getPlatformsNormal(){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{},
      url: Constant.API_BASE_URL + '/platform/list'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }
}
