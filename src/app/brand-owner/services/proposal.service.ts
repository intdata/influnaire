import { Injectable } from '@angular/core';
import { Constant } from './../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";
import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';

declare var $: any;

@Injectable()
export class ProposalService {
  constructor(
    private httpClient: HttpClient,
    private cryptoProvider: CryptoProvider
  ) {
  }

  /**
   * Accept or reject proposal
   */
  proposalAcceptReject(proposal_id, invitation_id, type){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{proposal_id:proposal_id, invitation_id:invitation_id, type:type},
      url: Constant.API_BASE_URL + '/proposal/acceptByBrandowner'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }
}
