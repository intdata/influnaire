import { Injectable } from '@angular/core';
import { Constant } from './../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";

declare var $: any;

@Injectable()
export class SettingService {
  private SITE_SETTING_GET_URL:string;
  constructor(
    private httpClient: HttpClient,
    private cryptoProvider: CryptoProvider
  ) {
    this.SITE_SETTING_GET_URL = '/setting/getSetting';
  }

  /**
   * Service used to post influncer sign up form
   * @returns {Observable<any>}
   * @memberof CountryService
   */
  

  getAllsettings(params={}): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + this.SITE_SETTING_GET_URL,
      params: params
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers}
    );
  }

}
