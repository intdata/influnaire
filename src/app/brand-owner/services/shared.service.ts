import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  public compareInfluncer:any
  public confRedirect:false
  constructor() { }

  //Set if the confirm redirect modal display
  setConfRedirect(data){
    this.confRedirect = data 
  }

  //Get the confirm Redirect modal display or not
  getConfRedirect(){
    return this.confRedirect
  }

}
