import { Injectable, Inject } from '@angular/core';
//import { AppConst } from '../app.constants';
import { Constant } from './../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';

import { CryptoProvider } from "./../../shared/crytoEncrypt.service";
import { FormGroup } from '@angular/forms';

declare var $: any;

@Injectable()
export class UserService {
  private INFLUENCER_INFO_URL: string;
  private BRAND_OWNER_INFO_URL: string;
  private BRAND_OWNER_CHANGE_PASSWORD_URL: string;
  private BRAND_OWNER_CHANGE_PERSONAL_INFO_URL: string;
  private BRAND_OWNER_CHANGE_COMPANY_INFO_URL: string;
  constructor(
    private httpClient: HttpClient,
    private cryptoProvider: CryptoProvider
  ) {
    this.INFLUENCER_INFO_URL = '/users/get-influencer-by-id';
    this.BRAND_OWNER_INFO_URL = '/users/get-brand-owner-by-id';
    this.BRAND_OWNER_CHANGE_PASSWORD_URL = '/users/change-brand-owner-password';
    this.BRAND_OWNER_CHANGE_PERSONAL_INFO_URL = '/users/change-brand-personal-info';
    this.BRAND_OWNER_CHANGE_COMPANY_INFO_URL = '/users/change-brand-company-info';
  }

  /**
   * Service used to post influncer sign up form
   *
   * @returns {Observable<any>}
   * @memberof UserService
   */
  getBrandOwnerInfo(): Observable<any> {
    
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + this.BRAND_OWNER_INFO_URL,
      params: ''
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }
  
  /**
   * Method to change personal info
   * @param changePersonalInfoForm 
   */
  changePersonalInfo(changePersonalInfoForm: FormGroup): Observable<any> {
    
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + this.BRAND_OWNER_CHANGE_PERSONAL_INFO_URL,
      params: changePersonalInfoForm
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }

  /**
   * Method to change company info
   * @param changeCompanyInfoForm 
   */
  changeCompanyInfo(changeCompanyInfoForm: FormGroup): Observable<any> {
    
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + this.BRAND_OWNER_CHANGE_COMPANY_INFO_URL,
      params: changeCompanyInfoForm
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }

  /**
   * Method to chage password
   * @param changePasswordForm 
   */
  changePassword(changePasswordForm: FormGroup): Observable<any> {
    
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + this.BRAND_OWNER_CHANGE_PASSWORD_URL,
      params: changePasswordForm
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }
  
}
