import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignTermsComponent } from './campaign-terms.component';

describe('CampaignTermsComponent', () => {
  let component: CampaignTermsComponent;
  let fixture: ComponentFixture<CampaignTermsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignTermsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignTermsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
