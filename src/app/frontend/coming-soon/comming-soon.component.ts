import { Component, OnInit, ViewChild } from '@angular/core';
import { CountdownTimerModule } from 'ngx-countdown-timer';
import { AllDataService } from "./../services/all_data.service";

@Component({
  selector: 'app-comming-soon',
  templateUrl: './comming-soon.component.html',
  styleUrls: ['./comming-soon.component.scss']
})
export class CommingSoonComponent implements OnInit {

  message ='';
  successMessage =false;
  public endDate:any='';

  constructor(private AllDataService:AllDataService) { }

  ngOnInit() {
    this.getTimerEnd()
  }
  getTimerEnd(url=''){
    this.AllDataService.getTimerEnd().subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
              this.endDate = response.data;
              this.message = '..... to go'
              //console.log()
          } else {
              console.log('get page error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get page error: ', error);
      }
  );
  }

}
