import { Component, OnInit } from '@angular/core';
import { PageService } from "./../services/page.service";

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.scss']
})
export class ContactusComponent implements OnInit {

  pageData: any = [];
  cmaLang: any = [];
  public loading: boolean = true;

  constructor(private PageService: PageService) { }

  ngOnInit() {
    this.getPageData('contact-us')
  }

  /**
 * Method to get particular page data 
 */
getPageData(slug = ''){
  this.PageService.getPageData(slug).subscribe(
    (response: any) => {
        //console.log(response);
        
        if (response.responseCode === 200) {
            this.pageData = response.page;
            this.cmaLang = response.page.CmsLangs[0]
            this.loading = false
            //console.log()
        } else {
            console.log('get page error: ', response.responseDetails);
        }
    },
    error => {
        console.log('get page error: ', error);
    }
);
}

}
