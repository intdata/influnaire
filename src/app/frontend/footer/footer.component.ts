import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from "./../services/user.service";
import { ToastrService } from 'ngx-toastr';

import { TranslateService } from '@ngx-translate/core';

const REGEX_DOT = /^([a-zA-Z0-9]+((\.|\_|\-|\+)[a-zA-Z0-9]+)*)@(?:[a-zA-Z](?:[a-zA-Z0-9-].{0,252}[a-zA-Z0-9])?(\.|\_|\-|\+))+([a-zA-Z]{2,})$/;
const REGEX_COUNTER = /^[a-zA-Z0-9+._-][a-zA-Z0-9+._-]{0,63}@(?:[a-zA-Z](?:[a-zA-Z0-9+-_].{0,252}[a-zA-Z0-9])?\.)+([a-zA-Z]{2,})$/;
const REGEX_MAIN = /^(([^<>()\[\]\.,';:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  newsletterForm: FormGroup;
  submitted:boolean = false;
  isSignUpSuccess:boolean = false;
  successMessage: string = '';

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private toastr: ToastrService,
    private translate: TranslateService,
  ) { 
    this.translate.setDefaultLang('en');
  }

  ngOnInit() {
    this.newsletterForm = this.formBuilder.group({        
        email: ['', [Validators.required, Validators.pattern(REGEX_DOT), Validators.pattern(REGEX_COUNTER), Validators.pattern(REGEX_MAIN)]],        
    });
  }

  get i() { return this.newsletterForm.controls; }
  
  newsletterFormSubmit(){
    this.submitted = true;

    // stop here if form is invalid
    if (this.newsletterForm.invalid) {
        return;
    }

    let email = this.newsletterForm.value.email;
    console.log(email);
    // call service to sign up
    this.userService.newsletterSubscriptionForm(this.newsletterForm.value).subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
              this.isSignUpSuccess = true;
              //this.scrollTop();
              this.toastr.success(response.responseDetails, 'Success!');
              this.newsletterForm.reset();
              // set the default values for the fields
              //this.newsletterForm.controls["email"].setValue(false);
              this.successMessage = 'User Subscribed successfully.';
              localStorage.setItem('SignupSuccess', this.successMessage);
              this.router.navigate(['/success']);
          } else {
              //this.scrollTop();
              //this.successMessage = response.responseDetails;
              this.toastr.info(response.responseDetails, 'Info!');
          }
          this.submitted = false;
      },
      error => {
          //this.scrollTop();
          this.toastr.error('Something went wrong.', 'Error!');
          //this.successMessage = 'Something went wrong.';
          this.submitted = false;
      }
    );
  }

}
