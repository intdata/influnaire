import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute, RoutesRecognized } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

// services
import { UserService } from "./../services/user.service";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  forgotBrandOwnerForm: FormGroup;
  forgotInfluencerForm: FormGroup;
  submitted = false;
  userType:number = 0;
  successMessage: string = 'A email has been sent to your registered email address.';
  isSubmitSuccess:boolean = false;
  forgotBrandOwnerSubmitted: boolean = false;
  forgotInfluencerSubmitted: boolean = false;

  constructor( 
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder, 
    private userService: UserService
  ) {}

  ngOnInit() {
    this.forgotBrandOwnerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern(/^[a-z0-9!#$%&'*+/=?^_`{|}~-].{0,63}(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]{1})*@(?:[a-z](?:[a-z0-9-].{0,252}[a-z0-9])?\.)+([a-z]{2,})$/)]]
    });
    this.forgotInfluencerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern(/^[a-z0-9!#$%&'*+/=?^_`{|}~-].{0,63}(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]{1})*@(?:[a-z](?:[a-z0-9-].{0,252}[a-z0-9])?\.)+([a-z]{2,})$/)]]
    });

    this.activatedRoute
            .queryParams
            .subscribe(params => {
                if(params['type'] === "influencer"){
                    this.userType = 1;
                }
            });
  }

  // convenience getter for easy access to form fields
  get b() { return this.forgotBrandOwnerForm.controls; }
  get i() { return this.forgotInfluencerForm.controls; }

  forgotBrandOwnerSubmit() {
    this.forgotBrandOwnerSubmitted = true;

    // stop here if form is invalid
    if (this.forgotBrandOwnerForm.invalid) {
        return;
    }

    // call service to sign up
    this.userService.postBrandOwnerForgotPasswordForm(this.forgotBrandOwnerForm.value).subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
              this.successMessage = response.responseDetails;
              this.isSubmitSuccess = true;
              this.forgotBrandOwnerForm.reset();
          } else {
            this.successMessage = response.responseDetails;
            this.isSubmitSuccess = true;
          }
          this.forgotBrandOwnerSubmitted = false;
      },
      error => {
          this.successMessage = 'Something went wrong.';
          this.isSubmitSuccess = true;
          this.forgotBrandOwnerSubmitted = false;
      }
    );
  }

  forgotInfluencerSubmit() {
    this.forgotInfluencerSubmitted = true;

    // stop here if form is invalid
    if (this.forgotInfluencerForm.invalid) {
        return;
    }

    // call service to sign up
    this.userService.postInfluenceForgotPasswordForm(this.forgotInfluencerForm.value).subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
              this.successMessage = response.responseDetails;
              this.isSubmitSuccess = true;
              this.forgotInfluencerForm.reset();
          } else {
            this.successMessage = response.responseDetails;
            this.isSubmitSuccess = true;
          }
          this.forgotInfluencerSubmitted = false;
      },
      error => {
          this.successMessage = 'Something went wrong.';
          this.isSubmitSuccess = true;
          this.forgotInfluencerSubmitted = false;
      }
    ); 
  }
}
