import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { HomeLayoutComponent } from './home-layout/home-layout.component';
import { HomeComponent } from './home/home.component';
import { HomeTwoComponent } from './home-two/home-two.component';
import { HomeThreeComponent } from './home-three/home-three.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ResourcesComponent } from './resources/resources.component';
import { EbookComponent } from './resources/ebook/ebook.component';
import { CaseStudiesComponent } from './resources/case-studies/case-studies.component';
import { PricingComponent } from './pricing/pricing.component';
import { BlogComponent } from './blog/blog.component';
import { PagesComponent } from './pages/pages.component';
import { InfluencerComponent } from './influencer/influencer.component';
import { BlogDetailsComponent } from './blog-details/blog-details.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { LoginInfluencerComponent } from './login-influencer/login-influencer.component';
import { ContactusComponent } from './contactus/contactus.component';
import { TestimonialsComponent } from './testimonials/testimonials.component';
import { SitemapComponent } from './sitemap/sitemap.component';
import { SuccessComponent } from './success/success.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { AuthGuardLogin } from "./../shared/guard/common/auth.guard.login";
import { CommingSoonComponent } from './coming-soon/comming-soon.component';
import { CampaignTermsComponent } from './campaign-terms/campaign-terms.component';

const routes: Routes = [

  
  { path: '', component: HomeLayoutComponent, 
    children:[
      { path: '', component: HomeTwoComponent},
      { path: 'login', component: LoginComponent, canActivate: [AuthGuardLogin]},
      //{ path: 'login', component: CommingSoonComponent},
      { path: 'signup', component: SignupComponent, canActivate: [AuthGuardLogin]},
      { path: 'forgot-password', component: ForgotPasswordComponent},
      { path: 'reset-password', component: ResetPasswordComponent},
      { path: 'resources', component: ResourcesComponent},
      { path: 'resources/ebook', component: EbookComponent},
      { path: 'resources/case-studies', component: CaseStudiesComponent},
      { path: 'pricing', component: PricingComponent},
      { path: 'blog', component: CommingSoonComponent},
      { path: 'coming-soon', component: CommingSoonComponent},
      { path: 'pages/:slug', component: PagesComponent},
      { path: 'influencer', component: InfluencerComponent},
      { path: 'blog-details', component: BlogDetailsComponent},
      { path: 'about-us', component: AboutUsComponent},
      { path: 'login-influencer', component: CommingSoonComponent},
      //{ path: 'login-influencer', component: LoginInfluencerComponent},
      { path: 'contact-us', component: ContactusComponent},
      { path: 'testimonials', component: TestimonialsComponent},
      { path: 'sitemap', component: SitemapComponent},
      { path: 'success', component: SuccessComponent},
      { path: 'privacy-policy', component: PrivacyPolicyComponent},
      { path: 'terms-conditions', component: CampaignTermsComponent },

    ] 
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class FrontendRoutingModule { }
