import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule,HttpClient } from "@angular/common/http";

import { FrontendRoutingModule } from './frontend-routing.module';
import { HomeLayoutComponent } from './home-layout/home-layout.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CountoModule }  from 'angular2-counto';
import { HomeTwoComponent } from './home-two/home-two.component';
import { HomeThreeComponent } from './home-three/home-three.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

import { ReactiveFormsModule , FormsModule } from '@angular/forms';
import { ResourcesComponent } from './resources/resources.component';
import { PricingComponent } from './pricing/pricing.component';
import { BlogComponent } from './blog/blog.component';
import { InfluencerComponent } from './influencer/influencer.component';
import { PagesComponent } from './pages/pages.component';

import { OwlModule } from 'ngx-owl-carousel';
import { BlogDetailsComponent } from './blog-details/blog-details.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { LoginInfluencerComponent } from './login-influencer/login-influencer.component';
import { ContactusComponent } from './contactus/contactus.component';
import { TestimonialsComponent } from './testimonials/testimonials.component';
import { SitemapComponent } from './sitemap/sitemap.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { EbookComponent } from './resources/ebook/ebook.component';
import { CaseStudiesComponent } from './resources/case-studies/case-studies.component';


import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { CountdownTimerModule } from 'ngx-countdown-timer';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/lang/frontend/', '.json');
}

// services
import { UserService } from "./services/user.service";
import { CountryService } from "./services/country.service";
import { IndustryService } from "./services/industry.service";
import { PageService } from "./services/page.service";
import { AllDataService } from "./services/all_data.service";
import {
  SocialLoginModule,
  AuthServiceConfig,
  GoogleLoginProvider,
  FacebookLoginProvider,
} from "angular-6-social-login";

// constants
import { Constant } from './../constant';
import { SuccessComponent } from './success/success.component';
import { CommingSoonComponent } from './coming-soon/comming-soon.component';
import { CampaignTermsComponent } from './campaign-terms/campaign-terms.component';

// Configs 
export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
      [
        {
          id: FacebookLoginProvider.PROVIDER_ID,
          provider: new FacebookLoginProvider(Constant.FACEBOOKAPPID)
        },
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider(Constant.GOOGLEAPPID)
        }
      ]
  );
  return config;
}

@NgModule({
  imports: [
    CommonModule,
    FrontendRoutingModule,
    CountoModule,
    FormsModule,
    ReactiveFormsModule,
    OwlModule,
    HttpClientModule,
    SocialLoginModule,
    CountdownTimerModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [ 
    HomeLayoutComponent, 
    HomeComponent, 
    HeaderComponent, 
    FooterComponent, 
    HomeTwoComponent, 
    HomeThreeComponent, 
    LoginComponent,
    SignupComponent, 
    ForgotPasswordComponent, 
    ResetPasswordComponent,
    ResourcesComponent, 
    PricingComponent, 
    BlogComponent, 
    InfluencerComponent, 
    BlogDetailsComponent, 
    AboutUsComponent, 
    LoginInfluencerComponent, 
    ContactusComponent, 
    TestimonialsComponent, 
    SitemapComponent, 
    PrivacyPolicyComponent, 
    EbookComponent, 
    CaseStudiesComponent,
    PagesComponent,
    SuccessComponent,
    CommingSoonComponent,
    CampaignTermsComponent
  ],
  providers: [
    UserService,
    CountryService,
    IndustryService,
    PageService,
    AllDataService,
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    }
  ]
})
export class FrontendModule { }
