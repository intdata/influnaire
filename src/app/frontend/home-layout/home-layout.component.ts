import { Component, OnInit } from '@angular/core';
import { AllDataService } from "./../services/all_data.service";

@Component({
  selector: 'app-home-layout',
  templateUrl: './home-layout.component.html',
  styleUrls: ['./home-layout.component.scss']
})
export class HomeLayoutComponent implements OnInit {

  testimonial:any;
  constructor(private AllDataService:AllDataService) { }

  ngOnInit() {
    //this.getTestimonialData('get_last_testimoneal')
  }
  getTestimonialData(url=''){
    this.AllDataService.getLastData(url,{}).subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
              this.testimonial = response.data;
              //console.log()
          } else {
              console.log('get page error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get page error: ', error);
      }
  );
  }

}
