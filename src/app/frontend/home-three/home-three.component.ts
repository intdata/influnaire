import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-three',
  templateUrl: './home-three.component.html',
  styleUrls: ['./home-three.component.scss']
})
export class HomeThreeComponent implements OnInit {

  counto1;
  counto2;
  counto3;
  showTestimonial = false;
  constructor() { }

  ngOnInit() {
    window.onscroll = function() {myFunction()};

    var header = document.getElementById("header");
    var sticky = header.offsetTop;

    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("fixed-header");
      } else {
        header.classList.remove("fixed-header");
      }
    }

  }

}
