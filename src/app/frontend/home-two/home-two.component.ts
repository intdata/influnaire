import { Component, OnInit } from '@angular/core';
import { AllDataService } from "./../services/all_data.service";

@Component({
  selector: 'app-home-two',
  templateUrl: './home-two.component.html',
  styleUrls: ['./home-two.component.scss']
})
export class HomeTwoComponent implements OnInit {
  counto1;
  counto2;
  counto3;
  testimonial:any = [];
  imagepath:string;
  showTestimonial = false;
  
  constructor(private AllDataService:AllDataService) {

  }

  ngOnInit() {
    this.getTestimonialData('get_last_testimoneal')
  }
  getTestimonialData(url=''){
    this.AllDataService.getLastData(url,{}).subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
              this.testimonial = response.data;
              this.imagepath = response.imagepath;
          } else {
              console.log('get page error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get page error: ', error);
      }
  );
  }


  scroll(el: HTMLElement) {
    el.scrollIntoView({behavior: 'smooth'});
  }
}
