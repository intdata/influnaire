import { Component, OnInit } from '@angular/core';
import { AllDataService } from "./../services/all_data.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  counto1;
  counto2;
  counto3;
  testimonial:any;
  showTestimonial = false;

  constructor(private AllDataService:AllDataService) { }
  
  ngOnInit() {
    this.getTestimonialData('get_last_testimoneal')
  }

  getTestimonialData(url=''){
    this.AllDataService.getLastData(url,{}).subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
              this.testimonial = response.page;
              //console.log()
          } else {
              console.log('get page error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get page error: ', error);
      }
  );
  }

  scroll(el: HTMLElement) {
    el.scrollIntoView();
  }
}
