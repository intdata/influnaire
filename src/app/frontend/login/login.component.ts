import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { FormGroup , FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";
import {
    AuthService,
    FacebookLoginProvider,
    GoogleLoginProvider
} from 'angular-6-social-login';
//import { Subject } from 'rxjs';

// services
//import { AuthService } from './../../influencers/services/auth.service';
import { UserService } from "./../services/user.service";

import { SharedVariable } from './../../shared/shared-variable.service';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loginBrandOwnerForm: FormGroup;
    submitted = false;
    submittedBrandOwner = false;
    userType:number = 0;
    isSignUpSuccess:boolean = false;
    //public getLoggedInDetails = new Subject();

    constructor(
        private formBuilder: FormBuilder, 
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private userService: UserService,
        //private authService: AuthService,
        private toastr: ToastrService,
        private cryptoProvider: CryptoProvider,
        private socialAuthService: AuthService,
        private sharedVariable: SharedVariable,
        private translate: TranslateService
    ) {
        this.translate.setDefaultLang('en');
    }

    ngOnInit() {

        this.isSignUpSuccess = false;
        // to get the param and select registration form
        this.activatedRoute
            .queryParams
            .subscribe(params => {
                if(params['type'] === "influencer"){
                    this.userType = 1;
                }
            });

        this.loginBrandOwnerForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.pattern(/^[a-z0-9!#$%&'*+/=?^_`{|}~-].{0,63}(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]{1})*@(?:[a-z](?:[a-z0-9-].{0,252}[a-z0-9])?\.)+([a-z]{2,})$/)]],
            password: ['', [Validators.required]]
        });    

        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.pattern(/^[a-z0-9!#$%&'*+/=?^_`{|}~-].{0,63}(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]{1})*@(?:[a-z](?:[a-z0-9-].{0,252}[a-z0-9])?\.)+([a-z]{2,})$/)]],
            password: ['', [Validators.required]]
        });
    }
    
    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }
    get f1() { return this.loginBrandOwnerForm.controls; }

    /**
     * Log in method for influencers
     */
    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        //return;
        // call service to sign up
        this.userService.postInfluenceLoginForm(this.loginForm.value).subscribe(
            (response: any) => {
                //console.log(response);
                
                if (response.responseCode === 200) {
                    //this.loginForm.reset();       // set the default values for the fields
                    localStorage.setItem("isLoggedin", "true");
                    localStorage.setItem("authToken", this.cryptoProvider.encrypt(response.token));
                    localStorage.setItem("name", this.cryptoProvider.encrypt(response.userInfo.name));
                    localStorage.setItem("profileImage", this.cryptoProvider.encrypt(response.userInfo.profileImage));
                    this.toastr.success(this.translate.instant('successfully_logged_in'), 'Success!');
                    this.router.navigate(["influencers"]);
                } else if (response.responseCode === 202) {

                    let successMessage = this.translate.instant('email_not_verified_resend_link');
                    localStorage.setItem('SignupSuccess', successMessage);
                    this.router.navigate(['/success']);
                } else {                   

                    if(response.responseCode == 201){
                        this.toastr.info(this.translate.instant('password_not_correct'), 'Info!');
                    } else if(response.responseCode == 404){
                        this.toastr.info(this.translate.instant('user_not_found'), 'Info!');
                    } else {
                        this.toastr.info(response.responseDetails, 'Info!');
                    }
                }
                this.submitted = false;
            },
            error => {
                this.toastr.error('Something went wrong.', 'Error!');
                this.submitted = false;
            }
        );
    }

    /**
     * Log in method for brand owner
     */
    onLoginBrandOwnerFormSubmit(){
        this.submittedBrandOwner = true;

        // stop here if form is invalid
        if (this.loginBrandOwnerForm.invalid) {
            return;
        }
        //return;
        // call service to sign up
        this.userService.postBrandOwnerLoginForm(this.loginBrandOwnerForm.value).subscribe(
            (response: any) => {
                //console.log(response);
                
                if (response.responseCode === 200) {
                    //this.loginForm.reset();       // set the default values for the fields
                    localStorage.setItem("isLoggedinBrandOwner", "true");
                    localStorage.setItem("authToken", this.cryptoProvider.encrypt(response.token));
                    localStorage.setItem("name", this.cryptoProvider.encrypt(response.userInfo.name));
                    localStorage.setItem("profileImage", this.cryptoProvider.encrypt(response.userInfo.profileImage));
                    this.toastr.success(this.translate.instant('successfully_logged_in'), 'Success!');
                    this.router.navigate(["brand-owner"]);
                } else if (response.responseCode === 202) {
                    let successMessage = this.translate.instant('email_not_verified_resend_link');
                    localStorage.setItem('SignupSuccess', successMessage);
                    this.router.navigate(['/success']);
                } else {
                    if(response.responseCode == 201){
                        this.toastr.info(this.translate.instant('password_not_correct'), 'Info!');
                    } else if(response.responseCode == 404){
                        this.toastr.info(this.translate.instant('user_not_found'), 'Info!');
                    }  else if(response.responseCode == 203){
                        this.toastr.info(this.translate.instant('You can login after admin approval'), 'Info!');
                    }else {
                        this.toastr.info(response.responseDetails, 'Info!');
                    }                    
                }
                this.submittedBrandOwner = false;
            },
            error => {
                this.toastr.error('Something went wrong.', 'Error!');
                this.submittedBrandOwner = false;
            }
        );
    }

    /**
     * Method to connect with social media
     * @param socialPlatform 
     */
    public socialLoginIn(socialPlatform : string) {
        let socialPlatformProvider;
        if(socialPlatform == "facebook"){
          socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
        }else if(socialPlatform == "google"){
          socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
        }
        //return;
        this.socialAuthService.signIn(socialPlatformProvider).then(
          (userData: any) => {
            if(socialPlatform == "google"){
                userData.token = userData.idToken;
            }
            userData.doLogin = true;
            userData.sendMail = false;
             console.log(socialPlatform+" sign in data : " , userData);
            // return;
            // call service to sign up
            this.userService.postInfluenceSocialData(userData).subscribe(
                (response: any) => {
                    //console.log(response);
                    if (response.responseCode === 200) {
                        let token = this.cryptoProvider.encrypt(response.token);
                        let name = this.cryptoProvider.encrypt(response.userInfo.name);
                        let profileImage = this.cryptoProvider.encrypt(response.userInfo.profileImage)
                        localStorage.setItem("isLoggedin", "true");
                        localStorage.setItem("authToken", token);
                        localStorage.setItem("name", name);
                        localStorage.setItem("profileImage", profileImage);
                        let userInfo = {
                            name: name,
                            profileImage: profileImage,
                            isLoggedin: "true",
                            authToken: token
                        }
                        this.sharedVariable.updateLoggedinUserInfo(userInfo);
                        this.toastr.success(response.responseDetails, 'Success!');
                        this.router.navigate(["influencers"]);
                    } else {
                        this.toastr.info(response.responseDetails, 'Info!');
                    }
                },
                error => {
                    this.toastr.error('Something went wrong.', 'Error!');
                }
            );
          }
        );
    }
}


