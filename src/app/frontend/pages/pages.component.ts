import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute, RoutesRecognized } from '@angular/router';
import { PageService } from "./../services/page.service";
import { Constant } from './../../constant';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {

  apiUrl:any = '';
  pageData: any = [];
  cmaLang: any = [];
  slug: string;
  public loading: boolean = true;


  constructor(private PageService: PageService, private route: ActivatedRoute) { 
    this.apiUrl = Constant.API_BASE_URL;    
    
    
  }

  ngOnInit() {   
    this.route.params.subscribe( params => {
      this.slug = params['slug'];
      this.getPageData(this.slug)
    } );
  }
/**
 * Method to get particular page data 
 */
  getPageData(slug = ''){
    this.PageService.getPageData(slug).subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
              this.pageData = response.page;
              this.cmaLang = response.page.CmsLangs[0]
              this.loading = false
              //console.log()
          } else {
              console.log('get page error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get page error: ', error);
      }
  );
  }

}
