import { Component, OnInit } from '@angular/core';
import { PageService } from "./../services/page.service";

@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.scss']
})
export class PricingComponent implements OnInit {

  pageData: any = [];
  cmaLang: any = [];
  public loading: boolean = true;

  constructor(private PageService: PageService) { }

  ngOnInit() {
    this.getPageData('pricing')
  }

/**
 * Method to get particular page data 
 */
getPageData(slug = ''){
  this.PageService.getPageData(slug).subscribe(
    (response: any) => {
        //console.log(response);
        
        if (response.responseCode === 200) {
            this.pageData = response.page;
            this.cmaLang = response.page.CmsLangs[0]
            this.loading = false
            //console.log()
        } else {
            console.log('get page error: ', response.responseDetails);
        }
    },
    error => {
        console.log('get page error: ', error);
    }
);
}

}
