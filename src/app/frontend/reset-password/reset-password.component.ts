import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute, RoutesRecognized } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MustMatch } from '../must-match.validator';

import 'rxjs/add/operator/pairwise';
import 'rxjs/add/operator/filter';
// import { ToastrService } from 'ngx-toastr';

// import { Constant } from './../../constant';

// services
import { UserService } from "./../services/user.service";

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

    resetBrandOwnerForm: FormGroup;
    resetInfluencerForm: FormGroup;
    submitted = false;
    userType:number = 0;
    successMessage: string = 'Your Password has been reset successfully.';
    isSubmitSuccess:boolean = false;
    resetBrandOwnerSubmitted: boolean = false;
    resetInfluencerSubmitted: boolean = false;

    constructor( 
        private activatedRoute: ActivatedRoute,
        private formBuilder: FormBuilder, 
        private userService: UserService
    ) {}

    ngOnInit() {
        this.resetBrandOwnerForm = this.formBuilder.group({
            newPassword: ['', [Validators.required, Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]).{8,20}$/)]],
            confirmPassword: ['', Validators.compose([Validators.required])],
            email: ['', Validators.compose([Validators.required])],
            token: ['', Validators.compose([Validators.required])],
        }, {
            // check whether our password and confirm password match
            validator: MustMatch('newPassword', 'confirmPassword')
        });
        this.resetInfluencerForm = this.formBuilder.group({
            newPassword: ['', [Validators.required, Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]).{8,20}$/)]],
            confirmPassword: ['', Validators.compose([Validators.required])],
            email: ['', Validators.compose([Validators.required])],
            token: ['', Validators.compose([Validators.required])],
        }, {
            // check whether our password and confirm password match
            validator: MustMatch('newPassword', 'confirmPassword')
        });

        this.activatedRoute
            .queryParams
            .subscribe(params => {
                if(params['type'] === "influencer"){
                    this.userType = 1;
                    this.resetInfluencerForm.controls["email"].setValue(
                        params['email']
                    );
                    this.resetInfluencerForm.controls["token"].setValue(
                        params['verication_token']
                    );
                } else{
                    this.userType = 0;
                    this.resetBrandOwnerForm.controls["email"].setValue(
                        params['email']
                    );
                    this.resetBrandOwnerForm.controls["token"].setValue(
                        params['verication_token']
                    );
                }
            });
    }

    // convenience getter for easy access to form fields
    get b() { return this.resetBrandOwnerForm.controls; }
    get i() { return this.resetInfluencerForm.controls; }

    resetBrandOwnerSubmit() {
        this.resetBrandOwnerSubmitted = true;

        // stop here if form is invalid
        if (this.resetBrandOwnerForm.invalid) {
            return;
        }

        // call service to sign up
        this.userService.postBrandOwnerResetPasswordForm(this.resetBrandOwnerForm.value).subscribe(
        (response: any) => {
            //console.log(response);
            
            if (response.responseCode === 200) {
                this.successMessage = response.responseDetails;
                this.isSubmitSuccess = true;
                this.resetBrandOwnerForm.reset();
            } else {
                this.successMessage = response.responseDetails;
                this.isSubmitSuccess = true;
            }
            this.resetBrandOwnerSubmitted = false;
        },
        error => {
            this.successMessage = 'Something went wrong.';
            this.isSubmitSuccess = true;
            this.resetBrandOwnerSubmitted = false;
        }
        );
    }

    resetInfluencerSubmit() {
        this.resetInfluencerSubmitted = true;

        // stop here if form is invalid
        if (this.resetInfluencerForm.invalid) {
            return;
        }

        // call service to sign up
        this.userService.postInfluenceResetPasswordForm(this.resetInfluencerForm.value).subscribe(
        (response: any) => {
            //console.log(response);
            
            if (response.responseCode === 200) {
                this.successMessage = response.responseDetails;
                this.isSubmitSuccess = true;
                this.resetInfluencerForm.reset();
            } else {
                this.successMessage = response.responseDetails;
                this.isSubmitSuccess = true;
            }
            this.resetInfluencerSubmitted = false;
        },
        error => {
            this.successMessage = 'Something went wrong.';
            this.isSubmitSuccess = true;
            this.resetInfluencerSubmitted = false;
        }
        ); 
    }
}
