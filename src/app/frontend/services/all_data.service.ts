import { Injectable } from '@angular/core';
import { Constant } from './../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AllDataService {

  
  constructor(private httpClient: HttpClient) { 

  }

  getData(url:any, params: any): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY
      },
      url: Constant.API_BASE_URL + url
    };
    return this.httpClient.get<any>(
      request.url, 
      { headers: request.headers, params:params }
    );
  }

  getLastData(url:any, params: any): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY
      },
      url: Constant.API_BASE_URL + '/'+url
    };
    return this.httpClient.get<any>(
      request.url, 
      { headers: request.headers, params:params }
    );
  }
  //Fetch All Data 
  getAlllData(url:any, params: any){
    const request = {
      headers: {
        apikey: Constant.APIKEY
      },
      url: Constant.API_BASE_URL + '/'+url
    };
    return this.httpClient.get<any>(
      request.url, 
      { headers: request.headers, params:params }
    );
  }

  /**
   * Get Comming Soon End Time
   */
  getTimerEnd(){
    const request = {
      headers: {
        apikey: Constant.APIKEY
      },
      url: Constant.API_BASE_URL + '/timer-end'
    };
    return this.httpClient.get<any>(
      request.url, 
      { headers: request.headers}
    );
  }

}
