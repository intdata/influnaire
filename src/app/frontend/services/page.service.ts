import { Injectable } from '@angular/core';
import { Constant } from './../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PageService {

  private GET_PAGE_DATA_URL:string; 
  constructor(private httpClient: HttpClient) { 
    this.GET_PAGE_DATA_URL = '/get_page_data'
  }

  getPageData(slug: any): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY
      },
      url: Constant.API_BASE_URL + this.GET_PAGE_DATA_URL
    };
    return this.httpClient.get<any>(
      request.url, 
      { headers: request.headers, params:{slug:slug} }
    );
  }
}
