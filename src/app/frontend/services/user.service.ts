import { Injectable, Inject } from '@angular/core';
//import { AppConst } from '../app.constants';
import { Constant } from './../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';

declare var $: any;

@Injectable()
export class UserService {
  private INFLUENCER_SIGNUP_POST_URL:string;  
  private BRAND_OWNER_SIGNUP_POST_URL:string;
  private INFLUENCER_LOGIN_POST_URL:string;
  private BRAND_OWNER_LOGIN_POST_URL:string;
  private INFLUENCER_EMAIL_VERIFY_POST_URL:string;
  private BRAND_OWNER_EMAIL_VERIFY_POST_URL:string;
  private INFLUENCER_FORGOT_PASSWORD_POST_URL:string;
  private BRAND_OWNER_FORGOT_PASSWORD_POST_URL:string;
  private INFLUENCER_RESET_PASSWORD_POST_URL:string;
  private BRAND_OWNER_RESET_PASSWORD_POST_URL:string;
  private INFLUENCER_SOCIAL_SIGNUP_POST_URL:string;
  private NEWSLETTER_SUBSCRIPTION_POST_URL:string;
  constructor(
    private httpClient: HttpClient,
  ) {
    this.INFLUENCER_SIGNUP_POST_URL = '/users/influencer_signup';
    this.BRAND_OWNER_SIGNUP_POST_URL = '/users/brand_owner_signup';
    this.INFLUENCER_LOGIN_POST_URL = '/auth/connect/local';
    this.INFLUENCER_LOGIN_POST_URL = '/users/influencer_login';
    this.BRAND_OWNER_LOGIN_POST_URL = '/users/brand_owner_login';
    this.INFLUENCER_EMAIL_VERIFY_POST_URL = '/verify/influencer_email';
    this.BRAND_OWNER_EMAIL_VERIFY_POST_URL = '/verify/brand_owner_email';
    this.INFLUENCER_FORGOT_PASSWORD_POST_URL = '/users/influencer_forgot_password';
    this.BRAND_OWNER_FORGOT_PASSWORD_POST_URL = '/users/brand_owner_forgot_password';
    this.INFLUENCER_RESET_PASSWORD_POST_URL = '/users/influencer_reset_password';
    this.BRAND_OWNER_RESET_PASSWORD_POST_URL = '/users/brand_owner_reset_password';
    this.INFLUENCER_SOCIAL_SIGNUP_POST_URL = '/users/influencer_social_connect';
    this.NEWSLETTER_SUBSCRIPTION_POST_URL = '/users/newsletter_subscription';
  }

  /**
   * Service used to post influncer sign up form
   *
   * @param {influencerFormRequest} influencerFormRequest
   * @returns {Observable<any>}
   * @memberof UserService
   */
  postInfluenceForm(
    influencerFormRequest: any
  ): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY
      },
      url: Constant.API_BASE_URL + this.INFLUENCER_SIGNUP_POST_URL,
      params: influencerFormRequest
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }

  /**
   * Service used to post influncer social data
   *
   * @param {influencerSocialData} influencerSocialData
   * @returns {Observable<any>}
   * @memberof UserService
   */
  postInfluenceSocialData(
    influencerSocialData: any
  ): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY
      },
      url: Constant.API_BASE_URL + this.INFLUENCER_SOCIAL_SIGNUP_POST_URL,
      params: influencerSocialData
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }

  /**
   * Service used to post brand owner sign up form
   *
   * @param {brandOwnerFormRequest} brandOwnerFormRequest
   * @returns {Observable<any>}
   * @memberof UserService
   */
  postBrandOwnerForm(
    brandOwnerFormRequest: any
  ): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY
      },
      url: Constant.API_BASE_URL + this.BRAND_OWNER_SIGNUP_POST_URL,
      params: brandOwnerFormRequest
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }

  /**
   * Service used to post brand owner log in form
   *
   * @param {influenceLoginFormRequest} influenceLoginFormRequest
   * @returns {Observable<any>}
   * @memberof UserService
   */
  postInfluenceLoginForm(
    influenceLoginFormRequest: any
  ): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY
      },
      url: Constant.API_BASE_URL + this.INFLUENCER_LOGIN_POST_URL,
      params: influenceLoginFormRequest
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }

  /**
   * Service used to post brand owner log in form
   *
   * @param {influenceLoginFormRequest} influenceLoginFormRequest
   * @returns {Observable<any>}
   * @memberof UserService
   */
  postBrandOwnerLoginForm(
    influenceLoginFormRequest: any
  ): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY
      },
      url: Constant.API_BASE_URL + this.BRAND_OWNER_LOGIN_POST_URL,
      params: influenceLoginFormRequest
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }

  /**
   * Service used to verify email for Influencer
   *
   * @param {paramData} any
   * @returns {Observable<any>}
   * @memberof UserService
   */
  postInfluenceEmailVerify(
    paramData: any
  ): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY
      },
      url: Constant.API_BASE_URL + this.INFLUENCER_EMAIL_VERIFY_POST_URL,
      params: paramData
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }

  /**
   * Service used to verify email for Brand Owner
   *
   * @param {paramData} any
   * @returns {Observable<any>}
   * @memberof UserService
   */
  postBrandOwnerEmailVerifym(
    paramData: any
  ): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY
      },
      url: Constant.API_BASE_URL + this.BRAND_OWNER_EMAIL_VERIFY_POST_URL,
      params: paramData
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }

  /**
   * Service used to forgot password 
   * send email for Influencer
   *
   * @param {paramData} any
   * @returns {Observable<any>}
   * @memberof UserService
   */
  postInfluenceForgotPasswordForm(
    paramData: any
  ): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY
      },
      url: Constant.API_BASE_URL + this.INFLUENCER_FORGOT_PASSWORD_POST_URL,
      params: paramData
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }

  /**
   * Service used to forgot password 
   * send email for Brand Owner
   *
   * @param {paramData} any
   * @returns {Observable<any>}
   * @memberof UserService
   */
  postBrandOwnerForgotPasswordForm(
    paramData: any
  ): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY
      },
      url: Constant.API_BASE_URL + this.BRAND_OWNER_FORGOT_PASSWORD_POST_URL,
      params: paramData
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }

  /**
   * Service used to reset password for Influencer
   *
   * @param {paramData} any
   * @returns {Observable<any>}
   * @memberof UserService
   */
  postInfluenceResetPasswordForm(
    paramData: any
  ): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY
      },
      url: Constant.API_BASE_URL + this.INFLUENCER_RESET_PASSWORD_POST_URL,
      params: paramData
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }

  /**
   * Service used to reset password for Brand Owner
   *
   * @param {paramData} any
   * @returns {Observable<any>}
   * @memberof UserService
   */
  postBrandOwnerResetPasswordForm(
    paramData: any
  ): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY
      },
      url: Constant.API_BASE_URL + this.BRAND_OWNER_RESET_PASSWORD_POST_URL,
      params: paramData
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }

  /**
   * Service used to post newletter subscription
   *
   * @param {newsletterFormRequest} newsletterFormRequest
   * @returns {Observable<any>}
   * @memberof UserService
   */
  newsletterSubscriptionForm(
    newsletterFormRequest: any
  ): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY
      },
      url: Constant.API_BASE_URL + this.NEWSLETTER_SUBSCRIPTION_POST_URL,
      params: newsletterFormRequest
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }

  fbData(): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY
      },
      url: Constant.API_BASE_URL + '/users/facebook-search',
      params: {id:134328181245619}
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }
}
