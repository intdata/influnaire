import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// import custom validator to validate that password and confirm password fields match
import { MustMatch } from '../must-match.validator';
import { Router, NavigationEnd, ActivatedRoute, RoutesRecognized } from '@angular/router';
import { filter } from 'rxjs/operators';
import 'rxjs/add/operator/pairwise';
import 'rxjs/add/operator/filter';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

import {
    AuthService,
    FacebookLoginProvider,
    GoogleLoginProvider
} from 'angular-6-social-login';

//import { AppConst } from '../app.constants';
import { Constant } from './../../constant';

// services
import { UserService } from "./../services/user.service";
import { CountryService } from "./../services/country.service";
import { IndustryService } from "./../services/industry.service";

// const REGEX_DOT = /^([a-zA-Z0-9]+(\.[a-zA-Z0-9]+)*)@(?:[a-zA-Z](?:[a-zA-Z0-9-].{0,252}[a-zA-Z0-9])?\.)+([a-zA-Z]{2,})$/;
// const REGEX_COUNTER = /^[a-zA-Z0-9.][a-zA-Z0-9.]{0,63}@(?:[a-zA-Z](?:[a-zA-Z0-9-].{0,252}[a-zA-Z0-9])?\.)+([a-zA-Z]{2,})$/;
// const REGEX_MAIN = /^(([^<>()\[\]\.,';:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;

const REGEX_DOT = /^([a-zA-Z0-9]+((\.|\_|\-|\+)[a-zA-Z0-9]+)*)@(?:[a-zA-Z](?:[a-zA-Z0-9-].{0,252}[a-zA-Z0-9])?(\.|\_|\-|\+))+([a-zA-Z]{2,})$/;
const REGEX_COUNTER = /^[a-zA-Z0-9+._-][a-zA-Z0-9+._-]{0,63}@(?:[a-zA-Z](?:[a-zA-Z0-9+-_].{0,252}[a-zA-Z0-9])?\.)+([a-zA-Z]{2,})$/;
const REGEX_MAIN = /^(([^<>()\[\]\.,';:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
    influenceRegisterForm: FormGroup;
    brandRegForm: FormGroup;
    submitted:boolean = false;
    brandSubmitted:boolean = false;
    userType:number = 0;
    isSignUpSuccess:boolean = false;
    successMessage: string = '';
    isBrandOwnerSignUpSuccess:boolean = false;
    apiUrl:any = '';
    allCountries: any = [];
    allIndustries: any = [];
    public validate:boolean = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private toastr: ToastrService,
        private userService: UserService,
        private countryService: CountryService,
        private industryService: IndustryService,
        private socialAuthService: AuthService,
        private translate: TranslateService
    ) {
        this.apiUrl = Constant.API_BASE_URL;
        this.translate.setDefaultLang('en');
    }


    ngOnInit() {
        this.isSignUpSuccess = false;
        // to get the param and select registration form
        this.activatedRoute
            .queryParams
            .subscribe(params => {
                if(params['type'] === "influencer"){
                    this.userType = 1;
                }
                if(params['do'] === "email_verification"){
                    let email = params['email'];
                    let token = params['verication_token'];
                    this.validate = true;
                    switch(params['type']){
                        case "influencer":
                            this.verifyInfluencerEmail(email, token);     break;
                        case "brand_owner":
                            this.verifyBrandOwnerEmail(email, token);     break;
                    }
                }
            });
      
        this.influenceRegisterForm = this.formBuilder.group({
            name: ['', Validators.required],
            //email: ['', [Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
            //email: ['', [Validators.required, Validators.pattern(/^[a-z0-9!#$%&'*+/=?^_`{|}~-].{0,63}(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]{1})*@(?:[a-z](?:[a-z0-9-].{0,252}[a-z0-9])?\.)+([a-z]{2,})$/)]],
            email: ['', [Validators.required, Validators.pattern(REGEX_DOT), Validators.pattern(REGEX_COUNTER), Validators.pattern(REGEX_MAIN)]],
            password: ['', [Validators.required, Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]).{8,20}$/)]],
            confirmPassword: ['', Validators.compose([Validators.required])],
            influnaireAgree: ['']
        }, {
            // check whether our password and confirm password match
            validator: MustMatch('password', 'confirmPassword')
        });

        this.brandRegForm = this.formBuilder.group({
            companyName: ['', Validators.required],
            countryName: ['', [Validators.required]],
            industryName: ['', [Validators.required]],
            phoneNumber: ['', [Validators.required, Validators.pattern(/^[\+]?[0-9]{10,15}$/)]],
            firstName: ['', [Validators.required]],
            lastName: ['', [Validators.required]],
            title: ['', [Validators.required]],
            email: ['', [Validators.required, Validators.pattern(REGEX_DOT), Validators.pattern(REGEX_COUNTER), Validators.pattern(REGEX_MAIN)]],
            password: ['', [Validators.required, Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]).{8,20}$/)]],
            confirmPassword: ['', Validators.compose([Validators.required])],
            brandAgree: ['']
        }, {
            // check whether our password and confirm password match
            validator: MustMatch('password', 'confirmPassword')
        });
        // set the default values for the fields
        this.influenceRegisterForm.controls["influnaireAgree"].setValue(false);
        this.brandRegForm.controls["countryName"].setValue('');
        this.brandRegForm.controls["industryName"].setValue('');
        this.brandRegForm.controls["brandAgree"].setValue(false);

        // get all countries
        this.getAllCountries();
        // get all industries
        this.getAllIndustries();
        this.getFBData()
    }

    /**
     * Get Social Media Data Graph API 
     */
    getFBData(){
        console.log('Get facebook Data')
        this.userService.fbData().subscribe(
            (response: any) => {
                //console.log(response);
                
                if (response.responseCode === 200) {

                } else {
                    console.log('get page error: ', response.responseDetails);
                }
            },
            error => {
                console.log('get page error: ', error);
            }
        );
    }
    /**
     * Method to connect with social media
     * @param socialPlatform 
     */
    public socialSignIn(socialPlatform : string) {
        if(!this.influenceRegisterForm.get('influnaireAgree').value){
            this.toastr.info('You have to agree privacy policy', 'Info!');
            return;
        }
        let socialPlatformProvider;
        if(socialPlatform == "facebook"){
          socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
        }else if(socialPlatform == "google"){
          socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
        }
        
        this.socialAuthService.signIn(socialPlatformProvider).then(
          (userData) => {
            if(socialPlatform == "google"){
                userData.token = userData.idToken;
            }
            console.log(socialPlatform+" sign in data : " , userData);
            // call service to sign up
            this.userService.postInfluenceSocialData(userData).subscribe(
                (response: any) => {
                    //console.log(response);
                    
                    if (response.responseCode === 200) {
                        this.isSignUpSuccess = true;
                        //this.scrollTop();
                        //this.toastr.success(response.responseDetails, 'Success!');
                        this.influenceRegisterForm.reset();
                        // set the default values for the fields
                        this.influenceRegisterForm.controls["influnaireAgree"].setValue(false);
                        this.successMessage = this.translate.instant('influencer_data_saved_successfully');
                        localStorage.setItem('SignupSuccess', this.successMessage);
                        this.router.navigate(['/success']);
                    } else {
                        //this.scrollTop();
                        this.toastr.info(response.responseDetails, 'Info!');
                    }
                },
                error => {
                    //this.scrollTop();
                    this.toastr.error('Something went wrong.', 'Error!');
                }
            ); 
          }
        );
    }

    // convenience getter for easy access to form fields
    get i() { return this.influenceRegisterForm.controls; }
    get b() { return this.brandRegForm.controls; }

    /**
     * Scroll at the top of the page
     */
    scrollTop(){
        let scrollToTop = window.setInterval(() => {
            let pos = window.pageYOffset;
            if (pos > 0) {
                window.scrollTo(0, pos - 20); // how far to scroll on each step
            } else {
                window.clearInterval(scrollToTop);
            }
        }, 16);
    }

    /**
     * Method to sign up as influncer
     */
    influenceSubmit() {
    
        this.submitted = true;

        // stop here if form is invalid
        if (this.influenceRegisterForm.invalid) {
            return;
        }
        let password = this.influenceRegisterForm.value.password;
        let name = this.influenceRegisterForm.value.name;

        if(password.match(new RegExp(name,"i"))){
            this.toastr.info(this.translate.instant('if_name_used_in_the_password'), 'Info!');
            return;
        }

        //console.log(this.influenceRegisterForm.value);
        
        if(!this.influenceRegisterForm.get('influnaireAgree').value){
            this.toastr.info(this.translate.instant('mandatory_privacy_policy'), 'Info!');
            return;
        }

        // call service to sign up
        this.userService.postInfluenceForm(this.influenceRegisterForm.value).subscribe(
            (response: any) => {
                //console.log(response);
                
                if (response.responseCode === 200) {
                    this.isSignUpSuccess = true;
                    //this.scrollTop();
                    //this.toastr.success(this.translate.instant('influencer_data_saved_successfully'), 'Success!');
                    this.influenceRegisterForm.reset();
                    // set the default values for the fields
                    this.influenceRegisterForm.controls["influnaireAgree"].setValue(false);
                    this.successMessage = this.translate.instant('influencer_data_saved_successfully');
                    localStorage.setItem('SignupSuccess', this.successMessage);
                    this.router.navigate(['/success']);
                } else {
                    //this.scrollTop();
                    //this.successMessage = response.responseDetails;
                    if(response.responseCode == 202){
                        this.toastr.info(this.translate.instant('email_already_exists'), 'Info!');
                    } else {
                        this.toastr.info(response.responseDetails, 'Info!');
                    }
                }
                this.submitted = false;
            },
            error => {
                //this.scrollTop();
                this.toastr.error('Something went wrong.', 'Error!');
                //this.successMessage = 'Something went wrong.';
                this.submitted = false;
            }
          );
    }

    /**
     * Method to sign up as brand owner
     */
    brandSubmit() {
        this.brandSubmitted = true;

        // stop here if form is invalid
        if (this.brandRegForm.invalid) {
            return;
        }

        let password = this.brandRegForm.value.password;
        let firstName = this.brandRegForm.value.firstName;
        let lastName = this.brandRegForm.value.lastName;
        let companyName = this.brandRegForm.value.companyName;
        
        if(password.match(new RegExp(firstName,"i"))){
            this.toastr.info(this.translate.instant('if_first_name_used_in_the_password'), 'Info!');
            return;
        }

        if(password.match(new RegExp(lastName,"i"))){
            this.toastr.info(this.translate.instant('if_last_name_used_in_the_password'), 'Info!');
            return;
        }

        if(password.match(new RegExp(companyName,"i"))){
            this.toastr.info(this.translate.instant('if_company_name_used_in_the_password'), 'Info!');
            return;
        }

        if(!this.brandRegForm.get('brandAgree').value){
            this.toastr.info(this.translate.instant('mandatory_privacy_policy'), 'Info!');
            return;
        }

        // call service to sign up
        this.userService.postBrandOwnerForm(this.brandRegForm.value).subscribe(
            (response: any) => {
                //console.log(response);
                
                if (response.responseCode === 200) {
                    this.isSignUpSuccess = true;
                    //this.scrollTop();
                    //this.toastr.success(this.translate.instant('brand_owner_data_save_successfully'), 'Success!');
                    this.brandRegForm.reset();
                    // set the default values for the fields
                    this.brandRegForm.controls["countryName"].setValue('');
                    this.brandRegForm.controls["industryName"].setValue('');
                    this.brandRegForm.controls["brandAgree"].setValue(false);
                    this.successMessage = this.translate.instant('brand_owner_data_save_successfully');
                    localStorage.setItem('SignupSuccess', this.successMessage);
                    this.router.navigate(['/success']);
                } else {
                    //this.scrollTop();
                    if(response.responseCode == 202){
                        this.toastr.info(this.translate.instant('email_already_exists'), 'Info!');
                    } else {
                        this.toastr.info(response.responseDetails, 'Info!');
                    }
                    
                    //this.successMessage = response.responseDetails;
                }
                this.brandSubmitted = false;
            },
            error => {
                //this.scrollTop();
                this.toastr.error('Something went wrong.', 'Error!');
                //this.successMessage = 'Something went wrong.';
                this.brandSubmitted = false;
            }
        );
    }

    /**
     * Method to verify email for Influencer
     * @param email 
     * @param token 
     */
    verifyInfluencerEmail(email: any, token: any){
        this.isSignUpSuccess = true;
        let paramData = {
            email: email,
            verication_token: token
        }
        // call service to verify email
        this.userService.postInfluenceEmailVerify(paramData).subscribe(
            (response: any) => {
                //console.log(response);
                if (response.responseCode === 200) {
                    
                    this.successMessage = response.responseDetails;
                    localStorage.setItem('SignupSuccess', this.successMessage);
                    this.router.navigate(['/success']);
                } else {
                    this.successMessage = response.responseDetails;
                    localStorage.setItem('SignupSuccess', this.successMessage);
                    this.router.navigate(['/success']);
                }
                
            },
            error => {
                this.successMessage = "Something went wrong.";
                localStorage.setItem('SignupSuccess', this.successMessage);
                this.router.navigate(['/success']);
            }
        );
    }

    /**
     * Method to verify email for Brand Owner
     * @param email 
     * @param token 
     */
    verifyBrandOwnerEmail(email: any, token: any){
        let paramData = {
            email: email,
            verication_token: token
        }
        // call service to verify email
        this.userService.postBrandOwnerEmailVerifym(paramData).subscribe(
            (response: any) => {
                //console.log(response);
                this.isSignUpSuccess = true;
                console.log(4)
                if (response.responseCode === 200) {
                    this.successMessage = response.responseDetails;
                    localStorage.setItem('SignupSuccess', this.successMessage);
                    this.router.navigate(['/success']);
                } else {
                    this.successMessage = response.responseDetails;
                    localStorage.setItem('SignupSuccess', this.successMessage);
                    this.router.navigate(['/success']);
                }
            },
            error => {
                this.successMessage = "Something went wrong.";
                localStorage.setItem('SignupSuccess', this.successMessage);
                this.router.navigate(['/success']);
            }
        );
    }

    /**
     * Method to get all active countries
     */
    getAllCountries(){
        // call service to get country list
        this.countryService.getAllCountries().subscribe(
            (response: any) => {
                //console.log(response);
                
                if (response.responseCode === 200) {
                    this.allCountries = response.countries;
                } else {
                    console.log('get country list error: ', response.responseDetails);
                }
            },
            error => {
                console.log('get country list error: ', error);
            }
        );
    }

    /**
     * Method to get all active industries
     */
    getAllIndustries(){
        // call service to get industry list
        this.industryService.getAllIndustries().subscribe(
            (response: any) => {
                //console.log(response);
                
                if (response.responseCode === 200) {
                    this.allIndustries = response.industries;
                } else {
                    console.log('get industry list error: ', response.responseDetails);
                }
            },
            error => {
                console.log('get industry list error: ', error);
            }
        );
    }

}
