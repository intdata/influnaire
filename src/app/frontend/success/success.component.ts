import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.scss']
})
export class SuccessComponent implements OnInit {

  successMessage: string = 'User Sign up successfully.';
  constructor(private router: Router) { }

  ngOnInit() {
    this.successMessage = localStorage.getItem('SignupSuccess')
  }

}
