import { Component, OnInit } from '@angular/core';
import { AllDataService } from "./../services/all_data.service";

@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.scss']
})
export class TestimonialsComponent implements OnInit {

  testimonials:any=[]
  imagepath:string;

  constructor(private AllDataService:AllDataService) { 

  }

  ngOnInit() {
    this.getTestimonials('get_all_testimoneal')
  }
  getTestimonials(url=''){
    this.AllDataService.getAlllData(url,{}).subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
              this.testimonials = response.data;
              this.imagepath = response.imagepath;
              //console.log(response.data)
          } else {
              console.log('get page error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get page error: ', error);
      }
  );
  }

}
