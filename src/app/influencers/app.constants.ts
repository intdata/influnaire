import { Injectable } from '@angular/core';

export class AppConst {
  //public static API_BASE_URL = 'http://altabooking.indusnet.cloud/altabookingdev/backend/public/api/v1';
  public static API_BASE_URL = 'http://10.0.4.235:8000/api/v1';
  //public static API_BASE_URL = 'http://localhost/altabooking/backend/public/api/v1';
  //public static BASE_URL = 'https://uat.altabooking.com/';
  //public static BASE_URL = 'http://altabooking.indusnet.cloud/altabookingdev/ui/';
  public static APIKEY = 'indusIntHubR2PSM';
  public static EXECEPTION_TITLE = 'Info';
  public static EXECEPTION_ERROR = 'Something unexpected happened. Try again later.';
  public static EXECEPTION_TYPE = 'info';
  public static EXECEPTION_TYPE_ERROR = 'error';
  public static EXECEPTION_TYPE_ERROR_TITLE = 'Error';
  //public static GOOGLE_MAP_API_KEY = 'AIzaSyCYYOiP4vE';
  //public static GOOGLE_MAP_API_KEY = '';
  //public static AWS_BUCKET_URL = 'https://staging-alta-booking.s3.eu-west-2.amazonaws.com/';
  //public static TERMS_URL = 'https://uat.altabooking.com';
  //public static START_DATEOFBIRTH = 18;
}
