import { FormGroup } from '@angular/forms';

// custom validator to check if checkbox is true from array of checkboxes
export function CheckboxVallidation(controlName: string) {

    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        let flag = false;
        control['controls'].forEach((o, i) => {
            if(o.value){
                flag = true;
            }
        });

        if(flag){
            control.setErrors(null);
        } else{
            control.setErrors({ checkboxInvalid: true });
        }
        
        console.log(control);
        
    }
}
