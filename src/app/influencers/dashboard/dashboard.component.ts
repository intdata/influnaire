import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { JobsService } from "./../services/jobs.service";
import { DashboardService } from "./../services/dashboard.service";
import { PlatformService } from "./../services/platform.service";
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { SingleDataSet, Label, Color, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import { NgIf } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public ongoingJobs: any = [];
  public pastJobs: any = [];
  public allPlatforms: any = [];
  public CampaignAssignData:any = []
  public campaignCountData:any =[]
  public campaignStatusData = []
  public jobStatusData = []
  public paymentData = [];
  public doughnutPer = 0;
  public doughnutPer1 = 0;
  public doughnutPer2 = 0;

  @ViewChild('doughnutCanvas') doughnutCanvas:ElementRef
  @ViewChild('doughnutCanvas1') doughnutCanvas1:ElementRef
  @ViewChild('doughnutCanvas2') doughnutCanvas2:ElementRef


  public filterPlatformOnGoingCampaign: any = '';
  public filterPlatformPastCampaign: any = '';

  // First Chat Data Set
  public doughnutDataColors: any =[{ backgroundColor: ['rgb(64, 168, 242)', 'rgb(237, 140, 36)', 'rgb(223, 223, 223)', 'rgb(0, 205, 135)']}];
  public doughnutChartType: ChartType = 'doughnut';
  public doughnutChartLegend = true;
  public doughnutChartPlugins = [];

  public doughnutChartOptions: ChartOptions = {};
  public doughnutChartLabels: Label[] = ['In progress', ['Content submitted'], 'Content approved', 'Paid'];

  // Second Chat Data Set
  public doughnutDataColors1: any=[{ backgroundColor: ['rgb(64, 168, 242)', 'rgb(237, 140, 36)', 'rgb(0, 205, 135)']}];
  public doughnutChartType1: ChartType = 'doughnut';
  public doughnutChartLegend1 = true;
  public doughnutChartPlugins1 = [];

  public doughnutChartOptions1: ChartOptions = {};
  public doughnutChartLabels1: Label = ['Draft', 'In progress', 'Published'];

  // Third Chat Data Set
  public doughnutDataColors2: any = [{ backgroundColor: ['rgb(64, 168, 242)', 'rgb(237, 140, 36)', 'rgb(0, 205, 135)']}];
  public doughnutChartType2: ChartType = 'doughnut';
  public doughnutChartLegend2 = true;
  public doughnutChartPlugins2 = [];

  public doughnutChartOptions2: ChartOptions = {};
  public doughnutChartLabels2: Label = ['Pending to collect', 'Collected'];

  public campaignDate: any = new Date();
  public onGoingCampaignDate: any = new Date();
  public pastCampaignDate: any = new Date();

  public CampaignNameSearch:any = '';
  public PastCampaignNameSearch: any = '';

  constructor(
    private JobsService: JobsService,
    private DashboardService:DashboardService,
    private PlatformService: PlatformService,
    private translate: TranslateService,
  ) { 
    this.translate.setDefaultLang('en');
  }
  
  ngOnInit() {
    //this.getCampaign('past')
    //this.getCampaign('current')
    this.updateOnGoingCampaign();
    this.updatePastCampaign();
    this.getDashboardData();
    this.getplatforms();
  }

   /**
   * Method to add lebel at the 
   * middle of the chart
   * @param ctx 
   */
  centerLabel(ctx, middleText1, middleText2, textColor1, textColor2, doughnutCanvas, lagendsWidth = 88) {
    var width = doughnutCanvas.nativeElement.clientWidth,
        height = doughnutCanvas.nativeElement.clientHeight;

    var fontSize = (height / 50).toFixed(2);
    ctx.font = fontSize + "em Verdana";
    ctx.textBaseline = "middle";
    ctx.fillStyle = textColor1;

    var text = middleText1,
        textX = Math.round((width - ctx.measureText(text).width) / 2) - lagendsWidth,
        textY = height / 2 - 10;

    ctx.fillText(text, textX, textY, 200);

    var fontSize = (height / 150).toFixed(2);
    ctx.font = fontSize + "em 'Open Sans',sans-serif";
    ctx.textBaseline = "middle";
    ctx.fillStyle = textColor2;

    var text = middleText2,
        textX = Math.round((width - ctx.measureText(text).width) / 2) - lagendsWidth,
        textY = height / 2 + 15;

    ctx.fillText(text, textX, textY, 200);
    ctx.restore();
  }


  /**
   * Method to filter ongoing campaign
   */
  updateOnGoingCampaign(){
    let platformId = this.filterPlatformOnGoingCampaign;
    let monthYear = this.onGoingCampaignDate;
    this.getCampaign('current', platformId, monthYear);
  }

  /**
   * Method to filter past campaign
   */
  updatePastCampaign(){
    let platformId = this.filterPlatformPastCampaign;
    let monthYear = this.pastCampaignDate;
    this.getCampaign('past', platformId, monthYear);
  }

  /**
   * Method to update graph
   */
  updateCampaignGraph(){
    let monthYear = this.campaignDate;
    this.getDashboardData(monthYear);
  }

  /**
   * Get Campaig data
   */
  getCampaign(type: any = 'current', platformId:any = '', monthYear:any = ''){
    this.JobsService.getJobList(type, platformId, monthYear).subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
            if(type == 'current'){
              this.ongoingJobs = response.Jobs;
            }else{
              this.pastJobs = response.Jobs
            }
          } else {
              console.log('get page error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get page error: ', error);
      }
  );
  }

  /**
   * Get Platform List
   */
  getplatforms(){
    this.PlatformService.getMapedPlatforms().subscribe(
      (response: any) => {
                  
          if (response.responseCode === 200) {
            this.allPlatforms = response.data;
          } else {
              console.log('Campaign create error: ', response.responseDetails);
          }
      },
      error => {
          console.log('Campaign Create error: ', error);
      }
     );
  }


  /**
   * Get Dashboard Data 
   */
  getDashboardData(monthYear: any = ''){

    this.DashboardService.getData(monthYear).subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
            this.CampaignAssignData = (response.data && response.data.campaign_assign_status) ? response.data.campaign_assign_status:[]
            this.campaignCountData = (response.data && response.data.campaign_status) ? response.data.campaign_status:[]
            this.pushCampaign(this.CampaignAssignData)
            this.pushJob(this.campaignCountData)              
          } else {
              console.log('get page error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get page error: ', error);
      }
  );
  }

  pushCampaign(assignData){

    var totalAssign = 0;
    let pending = 0;
    let pending_payment = 0;
    var paymentPercent = 0;
    this.campaignStatusData[0] = 0;
    this.campaignStatusData[1] = 0;
    this.campaignStatusData[2] = 0;
    this.campaignStatusData[3] = 0;
    this.paymentData[0] = 0; 
    this.paymentData[1] = 0; 
    this.doughnutPer =0;
    if(assignData.length >0){     
      
      for(var i=0; i<assignData.length; i++ ){
        if(assignData[i].status == "CONTENT_SUBMITTED"){
            this.campaignStatusData[1] = assignData[i].campaignCnt;
            pending_payment += assignData[i].campaignCnt;
            totalAssign += assignData[i].campaignCnt;
        }
        else if(assignData[i].status == "CONTENT_APPROVED"){
            this.campaignStatusData[2] = assignData[i].campaignCnt;
            pending_payment += assignData[i].campaignCnt;
            totalAssign += assignData[i].campaignCnt;
        }
        else if(assignData[i].status == "PAID"){
            this.campaignStatusData[3] = assignData[i].campaignCnt;
            this.paymentData[1] = assignData[i].campaignCnt;
            totalAssign += assignData[i].campaignCnt;
        } else {
          pending += assignData[i].campaignCnt;
          pending_payment += assignData[i].campaignCnt;
          totalAssign += assignData[i].campaignCnt;
        }
      }
      this.doughnutPer = this.campaignStatusData[1]?Math.round(((this.campaignStatusData[1]) * 100) / totalAssign):0;
      paymentPercent = this.paymentData[1] ? Math.round(((this.paymentData[1]) * 100) / totalAssign):0;
      this.campaignStatusData[0] = pending;
      this.paymentData[0] = pending_payment;    
    }

    //Dashboarf Graph initialize
    let me = this;
    this.doughnutChartOptions = {
      responsive: false,
      rotation: Math.PI / 2,
      cutoutPercentage: 95,
      tooltips:{
        enabled: true
      },
      hover:{
        animationDuration: 0
      },
      legend: {
        position: 'right',
        labels: {
            //fontColor: "white",
            boxWidth: 7,
            padding: 10,
            usePointStyle: true
        }
      }
    };
    var ctx = this.doughnutCanvas.nativeElement.getContext("2d");
    if(totalAssign){
      this.doughnutChartOptions.animation = { 
        //duration: 0,
        onComplete: function() {
          me.centerLabel(ctx, me.doughnutPer + "%", "OF 100%", "#ed8c24", "#000", me.doughnutCanvas, 62);
        }
      }
    }

    //For third Graph
      this.doughnutChartOptions2 = {
      responsive: true,
      rotation: Math.PI / 2,
      cutoutPercentage: 95,
      tooltips:{
        enabled: true
      },
      hover:{
        animationDuration: 0
      },
      legend: {
        position: 'right',
        labels: {
            //fontColor: "white",
            boxWidth: 7,
            padding: 10,
            usePointStyle: true
        }
      }
    };
    var ctx2 = this.doughnutCanvas2.nativeElement.getContext("2d");
    if(totalAssign){
      this.doughnutChartOptions2.animation = { 
        //duration: 0,
        onComplete: function() {
          me.centerLabel(ctx2, paymentPercent + "%", "OF 100%", "#ed8c24", "#000", me.doughnutCanvas2, 60);
        }
      }
    }
    
  }

  pushJob(jobData){

    var total =0;
    var totalInprogress = 0;
    this.jobStatusData[0] =0;
    this.jobStatusData[1] =0;
    this.jobStatusData[2]=0;

    if(jobData.length >0){
      
      for(var i=0; i<jobData.length;i++){
        if(jobData[i].status == "DRAFT"){
          this.jobStatusData[0] = jobData[i].campaignCnt;
          total += jobData[i].campaignCnt;
          totalInprogress++;
        }
        if(jobData[i].status == "IN_PROGRESS"){
          this.jobStatusData[1] = jobData[i].campaignCnt;
          total += jobData[i].campaignCnt;
          totalInprogress++;
        }
        if(jobData[i].status == "PUBLISHED"){
          this.jobStatusData[2] = jobData[i].campaignCnt;
          total += jobData[i].campaignCnt;
        }
      }
      this.doughnutPer2 = this.jobStatusData[1]?Math.round((this.jobStatusData[1] * 100) / total):0;
    }
    //Graph Data initialize
    let me = this;

    this.doughnutChartOptions1 = {
      responsive: true,
      rotation: Math.PI / 2,
      cutoutPercentage: 95,
      tooltips:{
        enabled: true
      },
      hover:{
        animationDuration: 0
      },
      legend: {
        position: 'right',
        labels: {
            //fontColor: "white",
            boxWidth: 7,
            padding: 10,
            usePointStyle: true
        }
      }
    };
    var ctx1 = this.doughnutCanvas1.nativeElement.getContext("2d");
    if(total){
      this.doughnutChartOptions1.animation = { 
        //duration: 0,
        onComplete: function() {
          me.centerLabel(ctx1, me.doughnutPer2 + "%", "OF 100%", "#ed8c24", "#000", me.doughnutCanvas1, 45);
        }
      }
    }
    
  }

}
