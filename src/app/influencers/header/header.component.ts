import { Component, OnInit, Output, EventEmitter, HostListener } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";

// services
import { SharedVariable } from "./../../shared/shared-variable.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public profileImage: any = '';
  public userName: any = '';
  constructor(
    private cryptoProvider: CryptoProvider,
    private sharedVariable: SharedVariable,

    private router: Router,
    private activateRote :ActivatedRoute
  ) {
    router.events.subscribe((val) => {
      // see also 
      if(val instanceof NavigationEnd){
        // this.cryptoProvider.toggle();
        this.cryptoProvider.hide();
      }
    }); 
  }

  @HostListener('click')
  click() {
    this.cryptoProvider.toggle(); 
  }

  ngOnInit() {
    setTimeout(() => {
      this.sharedVariable.loggedinUserInfoListender.subscribe(
        data => {
          if(data.profileImage && data.profileImage != ''){
            this.profileImage = this.cryptoProvider.decrypt(data.profileImage);   // get shared variable data
          } else{
            this.profileImage = this.cryptoProvider.decrypt(localStorage.getItem('profileImage'));    // get session data
          }
        },
        err => {
          this.profileImage = this.cryptoProvider.decrypt(localStorage.getItem('profileImage'));    // get session data
        }
      );
      this.userName = this.cryptoProvider.decrypt(localStorage.getItem('name'));    // get session data
    }, 2000);
  }

}
