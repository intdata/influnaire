import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfluencersLayoutComponent } from './influencers-layout.component';

describe('InfluencersLayoutComponent', () => {
  let component: InfluencersLayoutComponent;
  let fixture: ComponentFixture<InfluencersLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfluencersLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfluencersLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
