import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InfluencersLayoutComponent } from './influencers-layout/influencers-layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { InvitationsComponent } from './invitations/invitations.component';
import { OpportunitiesComponent } from './opportunities/opportunities.component';
import { JobsComponent } from './jobs/jobs.component';
import { MessagesComponent } from './messages/messages.component';
import { HelpComponent } from './help/help.component';
import { CompareInfluencersComponent } from './compare-influencers/compare-influencers.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { InvitationDetailsComponent } from './invitation-details/invitation-details.component';
import { JobDetailsComponent } from './job-details/job-details.component';
import { MyaccountComponent } from './myaccount/myaccount.component';
import { MessageDetailsComponent } from './message-details/message-details.component';
import { LogoutComponent } from './logout/logout.component';

const routes: Routes = [
  { path: '', component: InfluencersLayoutComponent, 
    children:[
      { path: '', component: DashboardComponent},
      { path: 'logout', component: LogoutComponent},
      { path: 'invitations', component: InvitationsComponent},
      { path: 'opportunities', component: OpportunitiesComponent},
      { path: 'jobs', component: JobsComponent},
      { path: 'messages', component: MessagesComponent},
      { path: 'help', component: HelpComponent},
      { path: 'compare', component: CompareInfluencersComponent},
      { path: 'notifications', component: NotificationsComponent},
      { path: 'invitation-details/:id', component: InvitationDetailsComponent},
      { path: 'job-details/:id', component: JobDetailsComponent},
      { path: 'myaccount', component: MyaccountComponent},
      { path: 'message-detail/:campaign/:influncer/:brandowner', component: MessageDetailsComponent},

    ]

  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InfluencersRoutingModule { }
