import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HttpClient } from "@angular/common/http";

import { InfluencersRoutingModule } from './influencers-routing.module';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { InfluencersLayoutComponent } from './influencers-layout/influencers-layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { InvitationsComponent } from './invitations/invitations.component';
import { OpportunitiesComponent } from './opportunities/opportunities.component';
import { JobsComponent } from './jobs/jobs.component';
import { MessagesComponent } from './messages/messages.component';
import { HelpComponent } from './help/help.component';
import { CompareInfluencersComponent } from './compare-influencers/compare-influencers.component';
import { NgbTabsetModule, NgbModalModule, NgbDatepicker, NgbDatepickerModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { NotificationsComponent } from './notifications/notifications.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InvitationDetailsComponent } from './invitation-details/invitation-details.component';
import { JobDetailsComponent } from './job-details/job-details.component';
import { MyaccountComponent } from './myaccount/myaccount.component';
import { MessageDetailsComponent } from './message-details/message-details.component';
import { LogoutComponent } from './logout/logout.component';
import { ChartsModule } from 'ng2-charts';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { SharedPipesModule } from "./pipes/shared-pipes.module";
import { NgxPaginationModule } from 'ngx-pagination';
import { FlatpickrModule } from 'angularx-flatpickr';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/lang/influencers/', '.json');
}

// services
import { UserService } from "./services/user.service";
import { IndustryService } from "./services/industry.service";
import { CountryService } from "./services/country.service";
import { HelpCenterService } from "./services/help-center.service";
import { InvitationsService } from "./services/invitations.service";
import { PlatformService } from "./services/platform.service";
import { ProposalService } from "./services/proposals.service";
import { JobsService } from "./services/jobs.service";
import { MessageService } from "./services/message.service";
import { DashboardService } from "./services/dashboard.service";
import { NotificationsService } from "./services/notifications.service";
import { ImagePreloadDirective } from './../shared/imagePreloadDirective';

@NgModule({
  imports: [
    CommonModule,
    InfluencersRoutingModule,
    NgbTabsetModule,
    NgbModalModule,
    NgbDatepickerModule,
    NgbDropdownModule,
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    HttpClientModule,
    AccordionModule.forRoot(),
    SharedPipesModule,
    NgxPaginationModule,
    FlatpickrModule.forRoot(),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    HeaderComponent, 
    SidebarComponent, 
    InfluencersLayoutComponent, 
    DashboardComponent, 
    InvitationsComponent, 
    OpportunitiesComponent, 
    JobsComponent, 
    MessagesComponent, 
    HelpComponent, 
    CompareInfluencersComponent,
    NotificationsComponent,
    InvitationDetailsComponent,
    JobDetailsComponent,
    MyaccountComponent,
    MessageDetailsComponent,
    LogoutComponent,
    ImagePreloadDirective
  ],
  providers:[
    UserService,
    IndustryService,
    CountryService,
    HelpCenterService,
    InvitationsService,
    PlatformService,
    ProposalService,
    JobsService,
    MessageService,
    DashboardService,
    NotificationsService
  ]
})
export class InfluencersModule { }
