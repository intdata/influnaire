import { Component, OnInit, ElementRef, TemplateRef, ViewChild} from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute, RoutesRecognized } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ToastrService } from 'ngx-toastr';

// services
import { InvitationsService } from "./../services/invitations.service";
import { PlatformService } from "./../services/platform.service";
import { MessageService } from "./../services/message.service";
import { CountryService } from "./../services/country.service";

@Component({
  selector: 'app-invitation-details',
  templateUrl: './invitation-details.component.html',
  styleUrls: ['./invitation-details.component.scss']
})
export class InvitationDetailsComponent implements OnInit {
  @ViewChild('fileInput') fileInput;
  public modalRef: BsModalRef;
  public invitationDetails: any = {};
  public campaignId: any = '';
  private invitationId: any;
  public submitProposalForm: FormGroup;
  public submittedSubmitProposalForm: boolean = false;
  public allPlatforms: any = [];
  public selectedPlatformIds: any = [];
  public selectedPlatformIdCount: any = 0;
  public messages: any = [];
  public filePath: any;
  public msubmitted = false;
  messageForm: FormGroup;
  campaign:any;
  public campaignFilePath:any;
  influncer:any;
  brandowner:any;
  public status:any ='';
  public imageFile = ['jpg', 'jpeg', 'png']
  public docFile = ['txt', 'csv', 'xls', 'doc', 'docx']
  public pdfFile = ['pdf']
  public allowStatus = ['CONFIRM', 'CONTENT_SUBMITTED', 'CONTENT_APPROVED', 'PAID']
  public allCountries ={}

  constructor(
    private toastr: ToastrService,
    private modalService: BsModalService,
    private activatedRoute: ActivatedRoute,
    private invitationsService: InvitationsService,
    private formBuilder: FormBuilder,
    private MessageService: MessageService,
    private countryService: CountryService,
    private platformService: PlatformService
  ) {}

  ngOnInit() {
    // to get the param and select registration form
    this.activatedRoute
    .params
    .subscribe(params => {
        this.invitationId = params['id'];
    });
    this.submitProposalForm = this.formBuilder.group({
      subject: ['', [Validators.required]],
      brief: ['', [Validators.required]],
      description: ['', [Validators.required]],
      delivary_date: ['', [Validators.required]],
      price: ['', [Validators.required, Validators.pattern(/^[^0|\D]\d{0,9}(\.\d{1,2})?$/)]],
      platforms: new FormArray([]),
      required_resources: ['', [Validators.required]],
      invitation_id: ['', [Validators.required]],
      campaign_id: ['', [Validators.required]],
   });
//Message form
   this.messageForm = this.formBuilder.group({
      message: ['', [Validators.required]],
      files: ['']
  });

    this.getInvitationDetails(this.invitationId);    // get invitation details
    this.getAllPlatform();  // method to get platform list
    this.getAllCountries(); 
  }

  /**
     * Method to open submit proposal
     * @param submitProposalModal 
     * @param invitationId 
     */
    submitProposalModal(submitProposalModal: TemplateRef<any>, invitationId:any, campaignId:any){
      this.closeModal();
      this.invitationId = invitationId;
      this.campaignId = campaignId;
      this.submitProposalForm.controls['invitation_id'].setValue(invitationId);
      this.submitProposalForm.controls['campaign_id'].setValue(campaignId);
      this.openModal(submitProposalModal, "faq_popup modal-dialog modal-lg modal-dialog-centered");
  }

  /**
   * Method to get 
   * invitation details by id
   */
  getInvitationDetails(invitationId){
    // call service to get invitations
    this.invitationsService.getInvitationDetails(invitationId).subscribe(
        (response: any) => {
            //console.log(response);
            
            if (response.responseCode === 200) {
              this.invitationDetails = response.Invitation;  
              this.campaign = this.invitationDetails.campaign_id;
              this.influncer = this.invitationDetails.influencer_id;
              this.brandowner = this.invitationDetails.Campaign.brand_owner_id;
              this.status = this.invitationDetails.status;
              this.campaignFilePath = response.campaignImagePath;
              this.getMessage(this.campaign); 
            } else {
                this.toastr.info(response.responseDetails, 'Info!');
            }
        },
        error => {
            this.toastr.error('Something went wrong.', 'Error!');
        }
    );
  }
   /**
   * Get message by influncer
   */
  getMessage(campaign_id){
    this.MessageService.getInfluncerMessage(campaign_id, '').subscribe(
      (response: any) => {
                  
          if (response.responseCode === 200) {
            this.messages = response.data;
            this.filePath = response.filePath;
            
          } else {
              console.log('Campaign create error: ', response.responseDetails);
          }
      },
      error => {
          console.log('Campaign Create error: ', error);
      }
     );
  }

  /**
   * Method to get all active platform
   */
  getAllPlatform(){
    // call service to get country list
    this.platformService.getPlatforms().subscribe(
        (response: any) => {
            if (response.responseCode === 200) {
                this.allPlatforms = response.data;
                this.addCheckboxes();
            } else {
                console.log('get platform list error: ', response.responseDetails);
            }
        },
        error => {
            console.log('get platform list error: ', error);
        }
    );
  }

  /**
   * Get all countries list
   */
  /**
     * Method to get all active countries
     */
    getAllCountries(){
      // call service to get country list
      this.countryService.getAllCountries().subscribe(
          (response: any) => {
              //console.log(response);
              
              if (response.responseCode === 200) {
                  for(let i=0; i<response.countries.length;i++ ){
                    this.allCountries[response.countries[i].code] = response.countries[i].countryname
                  }
                 // console.log(this.allCountries)
              } else {
                  console.log('get country list error: ', response.responseDetails);
              }
          },
          error => {
              console.log('get country list error: ', error);
          }
      );
  }

// convenience getter for easy access to form fields
get f() {
  return this.submitProposalForm.controls;
}
//Message form control
get m() { return this.messageForm.controls; }

/**
   * Save message after message enter
   */
  save_message(){
    this.msubmitted=true
    if (this.messageForm.invalid) {
      return;
    }
    const formData = new FormData();
    const fileBrowser = this.fileInput.nativeElement;
      formData.append('up_file', '');
    if (fileBrowser.files && fileBrowser.files[0]) {
      formData.append('up_file', fileBrowser.files[0]);
    }
    formData.append('message',this.messageForm.value.message);
    formData.append('campaign_id',this.campaign);
    formData.append('influencer_id',this.influncer);
    formData.append('brand_owner_id',this.brandowner);
    formData.append('message_from','Influencer');
    //console.log(this.messageForm.value)
    this.MessageService.saveMessage(formData, this.status).subscribe(
      (response: any) => {
                  
          if (response.responseCode === 200) {
            //this.influncerMessage(this.campaign, this.influncer)
            this.msubmitted=false
            this.messageForm.controls["message"].setValue('');
            this.messageForm.controls["files"].setValue('');
            this.getMessage(this.campaign)
            console.log(response.data)
          } else {
              console.log('Campaign create error: ', response.responseDetails);
          }
      },
      error => {
          console.log('Campaign Create error: ', error);
      }
     ); 
  }

  /**
   * Method to add platforms in control
   */
  private addCheckboxes() {
    this.allPlatforms.forEach((o, i) => {
    //const control = new FormControl(i === 0); // if first item set to true, else false
    const control = new FormControl(false);
    (this.f.platforms as FormArray).push(control);
    });
  }

  /**
     * Medthod to open modal function
     * @param template
     * @param classR
     */
    openModal(template: TemplateRef<any>, classR: string) {
      this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: classR })
      );
  }

  /**
   * Method to close modal
   */
  closeModal() {
      this.modalService._hideModal(1);
  }

}
