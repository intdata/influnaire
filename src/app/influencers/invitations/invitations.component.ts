import { Component, OnInit, ElementRef, TemplateRef } from '@angular/core';
import { Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { Observable, Subject } from 'rxjs';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { CheckboxVallidation } from './../checkboxes-validation.validator';

// services
import { InvitationsService } from "./../services/invitations.service";
import { PlatformService } from "./../services/platform.service";
import { ProposalService } from "./../services/proposals.service";
import { NgbDatepickerConfig, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateFRParserFormatter } from "./ngb-date-fr-parser-formatter"

@Component({
    selector: 'app-invitations',
    templateUrl: './invitations.component.html',
    styleUrls: ['./invitations.component.scss'],
    providers: [{provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter}]
})
export class InvitationsComponent implements OnInit {
    public social = {
        fbChecked : false,
        twChecked : false,
        uChecked : false,
        inChecked : false,
    };
    objectKeys = Object.keys;
    public modalRef: BsModalRef;
    public pendingInvitations: any = [];
    public acceptInvitations: any = [];
    public rejectInvitations: any = [];
    public invitationId: any = '';
    public campaignId: any = '';
    public proposalDueDate: any = '';
    public submitProposalForm: FormGroup;
    public submittedSubmitProposalForm: boolean = false;
    //public allPlatforms: any = [{id:1, name:'Facebook', icon: '', class_name:'facebook'}];
    public statusList = {'PENDING' : 'Pending', 'ACCEPT' : 'Request Accepted', 'REJECT' : 'Request Not Accepted', 'SUBMITTED' : 'Proposal Send', 'CONFIRM' : 'Proposal Accepted', 'REFUSE' : 'Proposal Not Accepted', 'CONTENT_SUBMITTED' : 'Content Send', 'CONTENT_APPROVED' :'Content Approved', 'PAID': 'Paid'};
    public allPlatforms: any = [];
    public selectedPlatformIds: any = [];
    public selectedPlatformIdCount: any = 0;

    constructor(
        private router: Router,
        private toastr: ToastrService,
        private modalService: BsModalService,
        private formBuilder: FormBuilder, 
        private invitationsService: InvitationsService,
        private platformService: PlatformService,
        private proposalService: ProposalService
    ) {}

    ngOnInit() {
        this.submitProposalForm = this.formBuilder.group({
            subject: ['', [Validators.required]],
            brief: ['', [Validators.required]],
            description: ['', [Validators.required]],
            delivary_date: ['', [Validators.required]],
            price: ['', [Validators.required, Validators.pattern(/^[^0|\D]\d{0,9}(\.\d{1,2})?$/)]],
            platforms: new FormArray([]),
            required_resources: ['', [Validators.required]],
            invitation_id: ['', [Validators.required]],
            campaign_id: ['', [Validators.required]],
        });
        
        // {
        //   validator: CheckboxVallidation('platforms')
        // });

        this.getInvitations();  // method to get invitation list
        this.getAllPlatform();  // method to get platform list
    }

    /**
     * Method to get all invitations
     */
    getInvitations(){
        // call service to get invitations
        this.invitationsService.getInvitationList().subscribe(
            (response: any) => {
                //console.log(response);
                
                if (response.responseCode === 200) {
                this.pendingInvitations = response.Invitations.pendingInvitations;   
                this.acceptInvitations = response.Invitations.acceptInvitations;   
                this.rejectInvitations = response.Invitations.rejectInvitations;   
                } else {
                    this.toastr.info(response.responseDetails, 'Info!');
                }
            },
            error => {
                this.toastr.error('Something went wrong.', 'Error!');
            }
        );
    }

    /**
     * Method to open accept modal
     * @param acceptInvitationModal 
     * @param invitationId 
     */
    acceptModal(acceptInvitationModal: TemplateRef<any>){
        this.modalRef.hide()
        let result = this.acceptInvitation(this.invitationId);
        result.subscribe((response: any) => {
            if(response){
            this.openModal(acceptInvitationModal, "faq_popup modal-dialog modal-md modal-dialog-centered");
            }
        });
    }

    /**
     * Method to open submit proposal
     * @param submitProposalModal 
     * @param invitationId 
     */
    submitProposalModal(submitProposalModal: TemplateRef<any>, invitationId:any, campaignId:any){
        this.closeModal();
        this.invitationId = invitationId;
        this.campaignId = campaignId;
        this.submitProposalForm.controls['invitation_id'].setValue(invitationId);
        this.submitProposalForm.controls['campaign_id'].setValue(campaignId);
        this.openModal(submitProposalModal, "faq_popup modal-dialog modal-lg modal-dialog-centered");
    }

    /**
     * Method for accepting invitation
     * @param invitationId 
     */
    acceptInvitation(invitationId: any){
        // call service to update invitations
        return new Observable<boolean>( observer => {
        this.invitationsService.updateInvitation({invitationId: invitationId, status: 'ACCEPT'}).subscribe(
            (response: any) => {
                console.log(response.Invitations);
                
                if (response.responseCode === 200) {
                    this.pendingInvitations = response.Invitations.pendingInvitations;   
                    this.acceptInvitations = response.Invitations.acceptInvitations;   
                    this.rejectInvitations = response.Invitations.rejectInvitations;   
                    observer.next(true);
                } else {
                    this.toastr.info(response.responseDetails, 'Info!');
                    observer.next(false);
                }
            },
            error => {
                this.toastr.error('Something went wrong.', 'Error!');
                observer.next(false);
            }
        );
        });
    }

    /**
     * Method for rejecting invitation
     * @param invitationId 
     */
    rejectInvitation(){
        this.invitationsService.updateInvitation({invitationId: this.invitationId, status: 'REJECT'}).subscribe(
            (response: any) => {
                //console.log(response);
                
                if (response.responseCode === 200) {
                    this.modalRef.hide()
                    this.pendingInvitations = response.Invitations.pendingInvitations;   
                    this.acceptInvitations = response.Invitations.acceptInvitations;   
                    this.rejectInvitations = response.Invitations.rejectInvitations;   
                } else {
                    this.toastr.info(response.responseDetails, 'Info!');
                }
            },
            error => {
                this.toastr.error('Something went wrong.', 'Error!');
            }
        );
    }

    // convenience getter for easy access to form fields
    get f() {
        return this.submitProposalForm.controls;
    }

    /**
     * Method to send talk to us data
     * @param talkToUsFormData 
     */
    submitProposalAction(submitProposalForm: any){
        
        this.submittedSubmitProposalForm = true;

        const selectedOrderIds = this.submitProposalForm.value.platforms
        .map((v, i) => v ? this.allPlatforms[i].id : null)
        .filter(v => v !== null);

        if(this.submitProposalForm.invalid){
            return;
        }
        if(!selectedOrderIds.length){
            this.toastr.info('You have to select at least one platform.', 'Info!');
            return;
        }

        // update platform data
        const platforms = submitProposalForm.platforms.map((v, i) => v ? { proposal_id: 1, platform_id: this.allPlatforms[i].id } : null);
        submitProposalForm.platforms = platforms;


        // call service to sign up
        this.proposalService.submitProposalInfo(submitProposalForm).subscribe(
            (response: any) => {
                //console.log(response);
                
                if (response.responseCode === 200) {
                    this.toastr.success(response.responseDetails, 'Success!');   
                    this.submitProposalForm.reset();
                    this.selectedPlatformIdCount = 0;
                    this.closeModal();
                    this.router
                        .navigateByUrl("/", { skipLocationChange: true })
                        .then(() => this.router.navigate(["influencers/invitations"]));
                } else {
                    this.toastr.info(response.responseDetails, 'Info!');
                }
                this.submittedSubmitProposalForm = false;
            },
            error => {
                this.toastr.error('Something went wrong.', 'Error!');
                this.submittedSubmitProposalForm = false;
            });
    }

    /**
     * Method to get all active platform
     */
    getAllPlatform(){
        // call service to get country list
        this.platformService.getPlatforms().subscribe(
            (response: any) => {
                if (response.responseCode === 200) {
                    this.allPlatforms = response.data;
                    this.addCheckboxes();
                } else {
                    console.log('get platform list error: ', response.responseDetails);
                }
            },
            error => {
                console.log('get platform list error: ', error);
            }
        );
    }

    /**
     * Method to add platforms in control
     */
    private addCheckboxes() {
        this.allPlatforms.forEach((o, i) => {
        //const control = new FormControl(i === 0); // if first item set to true, else false
        const control = new FormControl(false);
        (this.f.platforms as FormArray).push(control);
        });
    }
    
    /**
     * Method on click on checkbox
     * @param t 
     */
    onCheckBoxClick(t,i) {
        this.selectedPlatformIds[i] = t.target.checked ? true : false;
        if(t.target.checked){
            this.selectedPlatformIdCount++;
        } else{
            this.selectedPlatformIdCount--;
        }

        if(this.selectedPlatformIdCount < 0){
            this.selectedPlatformIdCount = 0;
        }
    }

    /**
     * Confirm Acceptance modal before accept
     */
    confirmAcceptModal(confirmAccept, invitationId, proposalDueDate, campaignId){
        this.invitationId = invitationId;
        this.campaignId = campaignId;
        this.proposalDueDate = proposalDueDate;
        this.openModal(confirmAccept, "modal-sm");
      }

    /**
     * Confirm rejected modal before accept
     */
    confirmRejectModal(confirmReject, invitationId, proposalDueDate){
        this.invitationId = invitationId;
        this.openModal(confirmReject, "modal-sm");
      }

    /**
     * Medthod to open modal function
     * @param template
     * @param classR
     */
    openModal(template: TemplateRef<any>, classR: string) {
        this.modalRef = this.modalService.show(
        template,
        Object.assign({}, { class: classR })
        );
    }

    /**
     * Method to close modal
     */
    closeModal() {
        this.modalService._hideModal(1);
    }
}
