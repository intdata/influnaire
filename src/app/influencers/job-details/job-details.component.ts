import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute, RoutesRecognized } from '@angular/router';
import { JobsService } from "./../services/jobs.service";
import { PlatformService } from "./../services/platform.service";
import { MessageService } from "./../services/message.service";
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-job-details',
  templateUrl: './job-details.component.html',
  styleUrls: ['./job-details.component.scss']
})
export class JobDetailsComponent implements OnInit {

  private jobId: any;
  public jobDetail:any =[];
  public campaignHistory: any[] = [];
  public messages: any = [];
  public imageFile = ['jpg', 'jpeg', 'png'];
  public docFile = ['txt', 'csv', 'xls', 'doc', 'docx'];
  public pdfFile = ['pdf'];
  public filePath: any;
  public allPaths: any={};

  constructor(
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private PlatformService: PlatformService,
    private MessageService: MessageService,
    private jobsService: JobsService
    ) { }

  ngOnInit() {
    this.activatedRoute
    .params
    .subscribe(params => {
        this.jobId = params['id'];
    });
    this.getJobDetails(this.jobId);
    this.getMessage(this.jobId)
  }

   /**
   * Get message by influncer
   */
  getMessage(campaign_id){
    this.MessageService.getInfluncerMessage(campaign_id, '').subscribe(
      (response: any) => {
                  
          if (response.responseCode === 200) {
            this.messages = response.data;
            this.campaignHistory = response.campaignHistory;
            this.filePath = response.filePath;
          } else {
              console.log('Campaign create error: ', response.responseDetails);
          }
      },
      error => {
          console.log('Campaign Create error: ', error);
      }
     );
  }

  /**
   * get campaign details by campaign Id
   * @param campaign_id 
   */
  getJobDetails(campaign_id){
    this.jobsService.getJobdetail(campaign_id).subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
            this.jobDetail = response.data;
            this.allPaths = (response.allPaths) ? response.allPaths: {};
          } else {
            this.toastr.info(response.responseDetails, 'Info!');
        }
    },
    error => {
        this.toastr.error('Something went wrong.', 'Error!');
    }  
    ); 
  }

}
