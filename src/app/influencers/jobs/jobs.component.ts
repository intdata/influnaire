import { Component, OnInit, Host } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

// services
import { JobsService } from "./../services/jobs.service";
import { AppComponent } from './../../app.component';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss']
})
export class JobsComponent implements OnInit {
  public ongoingJobs: any = [];
  public pastJobs: any = [];
  public activeTab: any = 'ngb-tab-0';
  private  ongoingJobsPaginationConfig: any = {
    itemsPerPage: 1,
    currentPage: 1,
    totalItems: 0
  };
  private  pastJobsPaginationConfig: any = {
    itemsPerPage: 1,
    currentPage: 1,
    totalItems: 0
  };
  constructor(
    @Host() private app: AppComponent,
    private toastr: ToastrService,
    private jobsService: JobsService,
  ) {}

  ngOnInit() {
    this.getJobs('current','');  // method to get invitation list
    this.getJobs('past','');  // method to get invitation list
  }

  /**
   * Method to get all jobs
   */
  getJobs(type, platformId=''){
    // call service to get invitations
    this.jobsService.getJobList(type, platformId='').subscribe(
        (response: any) => {
            //console.log(response);
            
            if (response.responseCode === 200) {
              if(type == 'current'){
                this.ongoingJobs = response.Jobs
              }else{
                this.pastJobs = response.Jobs
              }   
              
              this.ongoingJobsPaginationConfig = {
                //id: 1,
                itemsPerPage: this.app.getSettingData('pagination'),
                currentPage: 1,
                totalItems: this.ongoingJobs.length
              };
              this.pastJobsPaginationConfig = {
                //id: 2,
                itemsPerPage: this.app.getSettingData('pagination'),
                currentPage: 1,
                totalItems: this.pastJobs.length
              };
            } else {
                this.toastr.info(response.responseDetails, 'Info!');
            }
        },
        error => {
            this.toastr.error('Something went wrong.', 'Error!');
        }
    );
  }

  /**
   * Method to change page no
   * @param event 
   * @param type 
   */
  ongoingJobChangePage(event: number, type: any){ 
    if(type == 'ongoing') {
      this.ongoingJobsPaginationConfig.currentPage = event;
    } else if(type == 'past') {
      this.pastJobsPaginationConfig.currentPage = event;
    }
  }

  /**
   * Method to get tab id
   * @param event 
   */
  tabChange(event){
    this.activeTab = event.nextId;
  }

}
