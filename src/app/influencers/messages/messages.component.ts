import { Component, OnInit, Host } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from "./../services/message.service";
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";
import { AppComponent } from './../../app.component';
@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {

  latestMessages: any=[];
  influncer: any = '';
  influencerImagePath: any = '';
  messageConfig: any;

  constructor(
    @Host() private app: AppComponent,
    private MessageService: MessageService,
    private cryptoProvider: CryptoProvider
    ) { }

  ngOnInit() {

    this.messageConfig = {
      itemsPerPage: this.app.getSettingData('pagination'),
      currentPage: 1,
      totalItems: this.latestMessages.count
    };

    this.getLatestMessage()
  }

   /**
   * pagination page changed
   */
  messagePageChanged(event){
    this.messageConfig.currentPage = event;
  }

  /**
   * Get Latest Message
   */
  getLatestMessage(){
    this.MessageService.getLatestMessage().subscribe(
      (response: any) => {
                  
          if (response.responseCode === 200) {
            this.latestMessages = response.data
            this.influencerImagePath = response.influencerImagePath
            //console.log(response.data)
          } else {
              console.log('Campaign create error: ', response.responseDetails);
          }
      },
      error => {
          console.log('Campaign Create error: ', error);
      }
     );  
  }

}
