import { Component, OnInit, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import { Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { FormBuilder, FormGroup, Validators, FormsModule } from '@angular/forms';
import { MustMatch } from './../../frontend/must-match.validator';
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { SingleDataSet, Label, Color, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';

// services
import { UserService } from "./../services/user.service";
import { IndustryService } from "./../services/industry.service";
import { CountryService } from "./../services/country.service";
import { PlatformService } from "./../services/platform.service";
const url = 'https://connect.facebook.net/es_US/sdk.js';
declare var window: any;
declare var FB: any;

@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.component.html',
  styleUrls: ['./myaccount.component.scss']
})
export class MyaccountComponent implements OnInit {

  @ViewChild('doughnutCanvas') doughnutCanvas: ElementRef;
  @ViewChild('doughnutCanvas1') doughnutCanvas1: ElementRef;
  loadAPI: Promise<any>;
  public access_token: any;
  public new_access_token: any;
  public client_id: any;
  public client_secret: any;
  public page_id: any;
  public i: any;
  public total_like: any;
  public total_comment: any;
  public fbData: any[];
  
  
  // doughnut
  public doughnutDataColors: any[];
  public doughnutChartType: ChartType = 'doughnut';
  public doughnutChartLegend = false;
  public doughnutChartPlugins = [];

  public doughnutChartOptions: ChartOptions = {};
  public doughnutChartLabels: Label[];
  public doughnutChartData: SingleDataSet;

  public doughnutChartOptions1: ChartOptions = {};
  public doughnutChartData1: SingleDataSet;

  // area line chart configuration
  public lineChartData: ChartDataSets[];
  public lineChartData1: ChartDataSets[];
  public lineChartLabels: Label[];
  public lineChartOptions: (ChartOptions) = {
    responsive: true,
    tooltips:{
      enabled: true
    },
    elements: {
        line: {
            tension: 0
        }
    },
    scales:{
      yAxes: [{
        gridLines: {
          display: false,
        },
        ticks: {
          display: true,
          beginAtZero: false
        }
      }],
    }
  };
  public lineChartColors: Color[] = [
    {
      borderColor: '#f4725d',
      backgroundColor: 'rgba(242, 89, 64, 0.1)',
    },
  ];
  public lineChartColors1: Color[] = [
    {
      borderColor: '#c91e9a',
      backgroundColor: 'rgba(201, 30, 154, 0.1)',
    },
  ];
  public lineChartLegend = false;
  public lineChartType = 'line';
  public lineChartPlugins = [];

  public modalRef: BsModalRef;
  public changePasswordForm: FormGroup;
  public changePersonalInfoForm: FormGroup;
  public changeDetailsInfoForm: FormGroup;
  public submittedChangePasswordForm: boolean = false;
  public showPersonalEditForm:boolean = false;
  public showDetailsEditForm:boolean = false;
  public submittedPersonalInfoForm: boolean = false;
  public submittedDetailsInfoForm: boolean = false;
  public allIndustries: any[] = [];
  public allCountries: any[] = [];
  public personalInfo: any;
  public platforms: any[] = [];
  public activePlatforms: any[] = [];
  public inactivePlatforms: any[] = [];
  public personalEmail: any = 'NA';
  private filesToUpload: Array<File> = [];
  public picture;
  public name;
  public associatedPlatforms:any =[];
  public readyMadeContentPrice = {}
  public generateContentPrice = {}
  public editReadymadePrice =false;
  public editGeneratePrice =false;
  public sourceValue: string;
  public showData: boolean;
  public paymentTypeOption: any[] = [];
  
  public totalFollowers:any;

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private modalService: BsModalService,
    private cryptoProvider: CryptoProvider,
    private formBuilder: FormBuilder, 
    private formsModule:FormsModule,
    private userService: UserService,
    private industryService: IndustryService,
    private countryService: CountryService,
    private platformService:PlatformService
  ) {}

  ngOnInit() {

    this.paymentTypeOption = [
      { key: "Auto", value: "Auto" },
      { key: "Manual", value: "Manual" }
    ]
    this.changePasswordForm = this.formBuilder.group({
        oldPassword: ['', [Validators.required]],
        newPassword: ['', [Validators.required, Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]).{8,20}$/)]],
        confirmPassword: ['', Validators.compose([Validators.required])],
    }, {
        // check whether our password and confirm password match
        validator: MustMatch('newPassword', 'confirmPassword')
    });


    this.changePersonalInfoForm = this.formBuilder.group({
        profilePicture: [],
        name: ['', [Validators.required]],
        phoneNumber: ['', [Validators.required, Validators.pattern(/^[\+]?[0-9]{10,15}$/)]],
        email: ['', [Validators.required, Validators.pattern(/^[a-z0-9!#$%&'*+/=?^_`{|}~-].{0,63}(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]{1})*@(?:[a-z](?:[a-z0-9-].{0,252}[a-z0-9])?\.)+([a-z]{2,})$/)]],
        industry: ['', [Validators.required]],
        paymentType: ['', [Validators.required]],
        payoutMail: ['', [Validators.required]],
    });

    this.changeDetailsInfoForm = this.formBuilder.group({
        isAgreeToPerform: [],
        country: ['', [Validators.required]],
        nationality: ['', [Validators.required]],
        description: ['', [Validators.required]]
    });

    this.getProfile();  // get profile info
    this.getAllCountries();  // get all countries
    this.getAllIndustries();  // get all industries
    this.setDounatTotalFollow();
    this.setDounatTotalEngage();
    this.setLineYotubeProgress();
    this.setLineInstagramProgress();
    this.assignedPlatform();
    
  }

  //show hide payment email box
  showPaymentEmail(event: any){
    let value = event;
    this.sourceValue=value;
    if(this.sourceValue==='Auto')
    {
      this.showData=true;
      this.changePersonalInfoForm.controls["payoutMail"].setValidators([Validators.required,Validators.pattern(/^[a-z0-9!#$%&'*+/=?^_`{|}~-].{0,63}(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]{1})*@(?:[a-z](?:[a-z0-9-].{0,252}[a-z0-9])?\.)+([a-z]{2,})$/)]);
    }
    else{
      this.showData=false;
      this.changePersonalInfoForm.controls["payoutMail"].setValidators([]);
    }
    
  }
  // convenience getter for easy access to form fields
  get fChangePassword() {
    return this.changePasswordForm.controls;
  }
  get fPersonalInfo() {
    return this.changePersonalInfoForm.controls;
  }
  get fDetailsInfo() {
    return this.changeDetailsInfoForm.controls;
  }

  /**
   * Method to get profile info
   */
  getProfile(){
    // call service to get profile info
    this.userService.getInfluencerInfo().subscribe(
        (response: any) => {
            if (response.responseCode === 200) {
              this.personalInfo = response.data.personalData;
              this.getTotalFollowers(this.personalInfo.userPlatform)
              this.platforms = response.data.platforms;
              this.activePlatforms = response.data.personalData.userPlatform
              .map(v => { 
                return { 
                  name: this.platforms.filter(v1 => v1.id == v.platform_id).map(v1 => v1.name),
                  class: this.platforms.filter(v1 => v1.id == v.platform_id).map(v1 => v1.class_name), 
                  id: v.platform_id
                }
              })
              .filter(v => v.class.length );
              //console.log(this.activePlatforms);
              this.inactivePlatforms = response.data.platforms.filter(v => !this.activePlatforms.map(v => v.id).includes(v.id));
              //console.log(this.changePersonalInfoForm.controls);
              // set personal edit form data
              //console.log(this.changePersonalInfoForm.controls)
              if(this.personalInfo.paymentType=='Auto')
              {
                this.showData = true;
                this.changePersonalInfoForm.controls["payoutMail"].setValidators([Validators.required,Validators.pattern(/^[a-z0-9!#$%&'*+/=?^_`{|}~-].{0,63}(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]{1})*@(?:[a-z](?:[a-z0-9-].{0,252}[a-z0-9])?\.)+([a-z]{2,})$/)]);
              }
              else
              {
                this.showData = false;
                this.changePersonalInfoForm.controls["payoutMail"].setValidators([]);
              }
                
              this.changePersonalInfoForm.controls["name"].setValue(
                this.personalInfo.firstName + ' ' + this.personalInfo.lastName
              );
              this.changePersonalInfoForm.controls["email"].setValue(
                this.personalInfo.email
              );
              this.personalEmail = this.personalInfo.email;
              this.changePersonalInfoForm.controls["phoneNumber"].setValue(
                this.personalInfo.phoneNumber
              );
              this.changePersonalInfoForm.controls["industry"].setValue(
                this.personalInfo.industry
              );
              this.changeDetailsInfoForm.controls["country"].setValue(
                this.personalInfo.country
              );
              this.changeDetailsInfoForm.controls["nationality"].setValue(
                this.personalInfo.nationality
              );
              this.changeDetailsInfoForm.controls["description"].setValue(
                this.personalInfo.description
              );
              this.changeDetailsInfoForm.controls["isAgreeToPerform"].setValue(
                this.personalInfo.isAgreeToPerform == '1' ? true : false
              );
              this.picture = this.personalInfo.profilePicture;
              this.name = this.personalInfo.firstName;
              
            
              console.log('payment type'+this.showData);
            } else {
                this.toastr.info(response.responseDetails, 'Info!');
            }
        },
        error => {
            this.toastr.error('Something went wrong.', 'Error!');
        }
    );
  }

  /**
   * Method to show hide edit form
   */
  editPersonalInfo(){
    this.showPersonalEditForm = !this.showPersonalEditForm;
    if(!this.showPersonalEditForm){
      this.picture = this.personalInfo.profilePicture;
      this.changePersonalInfoForm.controls['profilePicture'].setValue('');
    }
    this.changePersonalInfoForm.controls["name"].setValue(
      this.personalInfo.firstName + ' ' + this.personalInfo.lastName
    );
    this.changePersonalInfoForm.controls["email"].setValue(
      this.personalInfo.email
    );
    this.changePersonalInfoForm.controls["phoneNumber"].setValue(
      this.personalInfo.phoneNumber
    );
    this.changePersonalInfoForm.controls["industry"].setValue(
      this.personalInfo.industry
    );
    this.changePersonalInfoForm.controls["paymentType"].setValue(
      this.personalInfo.paymentType
    );
    this.changePersonalInfoForm.controls["payoutMail"].setValue(
      this.personalInfo.payoutMail
    );
    
  }

  /**
   * Method to show hide details edit form
   */
  editDetailsInfo(){
    this.showDetailsEditForm = !this.showDetailsEditForm;
    this.changeDetailsInfoForm.controls["country"].setValue(
      this.personalInfo.country
    );
    this.changeDetailsInfoForm.controls["nationality"].setValue(
      this.personalInfo.nationality
    );
    this.changeDetailsInfoForm.controls["description"].setValue(
      this.personalInfo.description
    );
    this.changeDetailsInfoForm.controls["isAgreeToPerform"].setValue(
      (this.personalInfo.isAgreeToPerform == 1)?true:false
    );
    this.changeDetailsInfoForm.controls["paymentType"].setValue(
      this.personalInfo.paymentType
    );
    this.changeDetailsInfoForm.controls["payoutMail"].setValue(
      this.personalInfo.payoutMail
    );
    
  }

  /**
   * Method to change personal info
   * @param changePersonalInfoForm 
   */
  savePersonalInfoFormAction(changePersonalInfoForm: FormGroup){
    this.submittedPersonalInfoForm = true;
    console.log(this.changeDetailsInfoForm.invalid);
    if(this.changePersonalInfoForm.invalid){
      return;
    }
    let profileData: any = new FormData();
    const files: Array<File> = this.filesToUpload;
    for (let key in changePersonalInfoForm) {
      if (key == "profilePicture") {
        if (files.length > 0) {
          profileData.append(key, files[0], files[0]['name']);
        } else {
          profileData.append(key, "");
        }
      } else {
        profileData.append(key, changePersonalInfoForm[key]);
      }
    }

    // call service to sign up
    this.userService.changePersonalInfo(profileData).subscribe(
        (response: any) => {
            //console.log(response);
            
            if (response.responseCode === 200) {
                this.toastr.success(response.responseDetails, 'Success!');   
                this.personalInfo = response.data.personalData;
                // set personal edit form data
                this.changePersonalInfoForm.controls["name"].setValue(
                  this.personalInfo.firstName + ' ' + this.personalInfo.lastName
                );
                this.changePersonalInfoForm.controls["email"].setValue(
                  this.personalInfo.email
                );
                this.changePersonalInfoForm.controls["phoneNumber"].setValue(
                  this.personalInfo.phoneNumber
                );
                this.changePersonalInfoForm.controls["industry"].setValue(
                  this.personalInfo.industry
                );
                this.picture = this.personalInfo.profilePicture;
                this.name = this.personalInfo.firstName;
                this.showPersonalEditForm = false;
                // update local storage
                localStorage.setItem("name", this.cryptoProvider.encrypt(this.personalInfo.firstName));
                localStorage.setItem("profileImage", this.cryptoProvider.encrypt(this.personalInfo.profilePicture));
                this.router
                    .navigateByUrl("/", { skipLocationChange: true })
                    .then(() => this.router.navigate(["influencers/myaccount"]));
            } else {
                this.toastr.info(response.responseDetails, 'Info!');
            }
            this.submittedPersonalInfoForm = true;
        },
        error => {
            this.toastr.error('Something went wrong.', 'Error!');
            this.submittedPersonalInfoForm = true;
        }
    );
  }

  /**
   * Method to change details info
   * @param changeDetailsInfoForm 
   */
  saveDetailsInfoFormAction(changeDetailsInfoForm: FormGroup){
    this.submittedDetailsInfoForm = true;

    if(this.changeDetailsInfoForm.invalid){
      return;
    }

    // call service to sign up
    this.userService.changeDetailsInfo(changeDetailsInfoForm).subscribe(
        (response: any) => {
            //console.log(response);
            
            if (response.responseCode === 200) {
                this.toastr.success(response.responseDetails, 'Success!');   
                this.personalInfo = response.data.detailsData;
                // set personal edit form data
                this.changeDetailsInfoForm.controls["country"].setValue(
                  this.personalInfo.country
                );
                this.changeDetailsInfoForm.controls["nationality"].setValue(
                  this.personalInfo.nationality
                );
                this.changeDetailsInfoForm.controls["description"].setValue(
                  this.personalInfo.description
                );
                this.changeDetailsInfoForm.controls["isAgreeToPerform"].setValue(
                  this.personalInfo.isAgreeToPerform
                );
                this.showDetailsEditForm = false;

            } else {
                this.toastr.info(response.responseDetails, 'Info!');
            }
            this.submittedDetailsInfoForm = false;
        },
        error => {
            this.toastr.error('Something went wrong.', 'Error!');
            this.submittedDetailsInfoForm = false;
        }
    );
  }

  /**
   * Method to get control on browse
   * @param $event 
   * @param type 
   */
  changeListener($event: any, type = "logo"): void {
    if ($event && $event.target) this.readThis($event.target, type);
  }

  /**
   * Method to instant image load on browse
   * @param inputValue 
   * @param type 
   */
  readThis(inputValue: any, type): void {
    try{
      var file: File = inputValue.files[0];
      this.filesToUpload = <Array<File>>inputValue.files;
      var myReader: FileReader = new FileReader();

      myReader.onloadend = e => {
          if (type == "image") {
            this.picture = myReader.result;
            //this.personalInfo.photo = this.image; //for image browes preview
          }
      };
      //if(myReader.result)
      myReader.readAsDataURL(file);
    } catch(e){
      this.picture = this.personalInfo.profilePicture;
    }
  }

  /**
   * set doughnut chart for
   * Total Follow
   */
  setDounatTotalFollow(data = [20, 80, 50], dataLabels = ['Facebook', 'Twitter', 'Youtube'], total=0) {
    this.doughnutChartOptions = {
      responsive: true,
      rotation: Math.PI / 2,
      cutoutPercentage: 85,
      tooltips: {
        enabled: false
      },
      hover: {
        animationDuration: 0 
      },
      legend: {
        position: 'left',
        labels: {
          //fontColor: "white",
          boxWidth: 7,
          padding: 5,
          usePointStyle: true
        }
      }
    };
    this.doughnutDataColors = [{ backgroundColor: ['#308ee0', '#1da1f2', '#e52d27'] }];

    // doughnut chart for Total Amount Invested
    var ctx = this.doughnutCanvas.nativeElement.getContext("2d");
    this.doughnutChartData = data;
    this.doughnutChartLabels = dataLabels;
    let _this = this;
    this.doughnutChartOptions.animation = {
      onComplete: function () {
        _this.centerLabel(ctx, total, "#2b2b2b", _this.doughnutCanvas);
      }
    }
  }

  /**
   * set doughnut chart for
   * Average Engagement Rate
   */
  setDounatTotalEngage() {
    this.doughnutChartOptions1 = JSON.parse(JSON.stringify(this.doughnutChartOptions));
    // doughnut chart for Total Amount Invested
    var ctx = this.doughnutCanvas1.nativeElement.getContext("2d");
    this.doughnutChartData1 = [20, 80, 50];
    let _this = this;
    this.doughnutChartOptions1.animation = {
      onComplete: function () {
        _this.centerLabel(ctx, "76%", "#2b2b2b", _this.doughnutCanvas1);
      }
    }
  }

   /**
   * set line chart for
   * Yotube Progress
   */
  setLineYotubeProgress(){
    // line chart labels
    this.lineChartLabels = ['Week 1', 'Week 2', 'Week 3', 'Week 4', 'Week 5'];

    // line chart for followers reached
    this.lineChartData = [
      { data: [169, 226, 190, 223, 225], label: 'Youtube Progress' },
    ];
  }

   /**
   * set line chart data for
   * Instagram Progress
   */
  setLineInstagramProgress(){
    // line chart for followers reached
    this.lineChartData1 = [
      { data: [160, 225, 180, 220, 222], label: 'Instagram Progress' },
    ];
  }


  /**
   * Method to add lebel at the 
   * middle of the chart
   * @param ctx 
   */
  centerLabel(ctx, middleText1, textColor1, doughnutCanvas) {
    var width = doughnutCanvas.nativeElement.clientWidth,
      height = doughnutCanvas.nativeElement.clientHeight;

    var fontSize = (height / 80).toFixed(2);
    ctx.font = fontSize + "em Verdana";
    ctx.textBaseline = "middle";
    ctx.fillStyle = textColor1;

    var text = middleText1,
      textX = Math.round((width - ctx.measureText(text).width) / 2),
      textY = height / 2;

    ctx.fillText(text, textX, textY, 200);
    ctx.restore();
  }

  /**
   * Method to open change password modal
   * @param changePasswordModal 
   */
  changePasswordModal(changePasswordModal: TemplateRef<any>){
    this.openModal(changePasswordModal, "customModal modal-md");
  }

  /**
   * Method to save new password
   * @param changePasswordForm 
   */
  saveChnagePasswordAction(changePasswordForm: FormGroup){
    this.submittedChangePasswordForm = true;

    //console.log(this.changePasswordForm);
    
    if(this.changePasswordForm.invalid){
      return;
    }
    
    // call service to sign up
    this.userService.changePassword(changePasswordForm).subscribe(
        (response: any) => {
            //console.log(response);
            
            if (response.responseCode === 200) {
                this.toastr.success(response.responseDetails, 'Success!');
                this.changePasswordForm.reset();    
                this.closeModal();
            } else {
                this.toastr.info(response.responseDetails, 'Info!');
            }
            this.submittedChangePasswordForm = false;
        },
        error => {
            this.toastr.error('Something went wrong.', 'Error!');
            this.submittedChangePasswordForm = false;
        }
    );
    
  }

  /**
   * Method to connect with social media
   * @param socialMediaName 
   */
  connectSocial(socialMediaName: any){
    switch(socialMediaName){
      case 'Facebook':
        this.connectFacebook();  break;
      case 'YouTube':
        this.connectYoutube();  break;
      default:
        this.toastr.error('Something went wrong.', 'Error!');
    }
  }

  /**
   * Method to connect Facebook
   * through graph api
   */
  connectFacebook(){
    let self = this;
    (window as any).fbAsyncInit = function() {
      console.log('qqq')
      FB.init({
        appId      : '997901807224381',
        cookie     : true,
        xfbml      : true,
        version    : 'v3.1'
      }); 
      
      FB.login(function(response) {
        //console.log(response);
        
        this.access_token = response.authResponse.accessToken; 
        //console.log(this.access_token);
        this.client_id = '997901807224381';
        this.client_secret = 'a7856751cf149237f8347cc1407a0981'; 
        this.page_id = '112184200262968'; 
        
        FB.api('/'+this.page_id+'?access_token='+this.access_token+'&fields=access_token', function(response) {
          console.log("access token=",response);
        });
        
        FB.api('/oauth/access_token?grant_type=fb_exchange_token&client_id='+this.client_id+'&client_secret='+this.client_secret+'&fb_exchange_token='+this.access_token, function(response) {
          this.new_access_token =  response.access_token;
          this.total_like = 0;
          this.total_comment = 0; 
          //this.new_access_token = "EAAOLlh69hj0BAIuP2TnKkAHN9SRg7ix5btIaZCKPbEQMXclqWS1RrUldwhruRPf2NUvLEubzkax3DWcI3DmFJTOV81tNyUZCoZCcCU8ZBeeSMqZCG12MfJNelwyqRmYb90GTkoxLH2MLpwtoJlEoXzMyktsvgBOmXrLIXfmZAqUvI3n1Wcjn8T5a6UM5pQARuKxLUZB1uZCC8LAbf3tM1fHW";

              FB.api('/'+this.page_id+'?access_token='+this.new_access_token+'&fields=fan_count,country_page_likes,posts{likes.summary(true),comments.summary(true)},engagement', function(response) {
                for(this.i=0;this.i<response.posts.data.length;this.i++){
                  this.total_like = this.total_like + response.posts.data[this.i].likes.summary.total_count;
                  this.total_comment = this.total_comment + response.posts.data[this.i].comments.summary.total_count;
                  //console.log(response.posts.data[this.i].likes.summary.total_count);
                }
                console.log(response);
                this.fbData = {
                  total_like :        this.total_like,
                  total_comment :     this.total_comment,
                  page_likes:         response.fan_count,  
                  country_page_likes: response.gender,
                  social_media_id:    response.id,
                  social_media_token: this.new_access_token,
                  engagement :        response.engagement.count,
                  total_post:         response.posts.data.length,
                  platform_id:        '1'                }
                //self.test(this.fbData);
                  self.userService.connectFacebook(this.fbData).subscribe(
                      (response: any) => {                        
                          if (response.responseCode === 200) {
                            self.getProfile()
                            self.toastr.success(response.responseDetails, 'Success!');
                            
                          } else {
                            self.toastr.info(response.responseDetails, 'Info!');
                          }
                      },
                      error => {
                        self.toastr.error('Something went wrong.', 'Error!');
                      }
                  );
                
              });                
        });

        // FB.api('/me?fields=id,name,first_name,posts{likes.summary(true)}', function(response) {
        //     console.log(response);
        // });       
        
      });
    };    

    (function(d, s, id){
       var js, fjs = d.getElementsByTagName(s)[0];
       if (d.getElementById(id)) {return;}
       js = d.createElement(s); js.id = id;
       js.src = "https://connect.facebook.net/en_US/sdk.js";
       fjs.parentNode.insertBefore(js, fjs);
     }(document, 'script', 'facebook-jssdk'));

     


  }


  /**
   * Method to connect 
   * google youtube service
   */
  connectYoutube(){
    console.log("connect to youtube")
    // call service to connect youtube
    this.userService.connectYoutube().subscribe(
        (response: any) => {
            //console.log(response);
            
            if (response.responseCode === 200) {
              this.toastr.success(response.responseDetails, 'Success!');
              this.router
                    .navigateByUrl("/", { skipLocationChange: true })
                    .then(() => this.router.navigate(["influencers/myaccount"]));
            } else {
              this.toastr.info(response.responseDetails, 'Info!');
            }
        },
        error => {
            this.toastr.error('Something went wrong.', 'Error!');
        }
    );
  }

  /**
   * Method to get all active industries
   */
  getAllIndustries(){
    // call service to get industry list
    this.industryService.getAllIndustries().subscribe(
        (response: any) => {
            //console.log(response);
            
            if (response.responseCode === 200) {
                this.allIndustries = response.industries;
            } else {
                console.log('get industry list error: ', response.responseDetails);
            }
        },
        error => {
            console.log('get industry list error: ', error);
        }
    );
  }

  /**
   * get platform of current influncer
   */
  assignedPlatform(){
    this.platformService.getInfluncerPlatform().subscribe(
      (response: any) => {
         // console.log(response);
          
          if (response.responseCode === 200) {
            var respData = response.data;
            console.log(respData)
            for(let c=0; c < respData.length; c++){
              this.readyMadeContentPrice[respData[c].id] = respData[c]['readymade_content_price']
              this.generateContentPrice[respData[c].id] = respData[c]['generate_content_price']
            }
              this.associatedPlatforms = response.data;
          } else {
              console.log('get industry list error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get industry list error: ', error);
      }
    );
  }
  /**
   * Method to get all active countries
   */
  getAllCountries(){
    // call service to get country list
    this.countryService.getAllCountries().subscribe(
        (response: any) => {
            //console.log(response);
            
            if (response.responseCode === 200) {
                this.allCountries = response.countries;
            } else {
                console.log('get country list error: ', response.responseDetails);
            }
        },
        error => {
            console.log('get country list error: ', error);
        }
    );
  }

  /**
   * Edit Generate Price
   */
  editGeneratePriceData(){
    this.editGeneratePrice = true;
  }
  /**
   * Edit Ready made Price
   */
  editReadymadePriceData(){
    this.editReadymadePrice = true;
  }

  /**
   * Cancel generate Edit
   */
  cancelGenerateEdit(){
    this.editGeneratePrice = false;
  }
  /**
   * Cancel generate Edit
   */
  cancelReadymadeEdit(){
    this.editReadymadePrice = false;
  }

  /**
   * Save Generated Price
   */
  saveReadymadePrice(data){
    this.platformService.savePrice(data.value, 'rm').subscribe(
      (response: any) => {
          //console.log(response);
          
          if (response.responseCode === 200) {
            this.editReadymadePrice = false;
            this.toastr.success(response.responseDetails, 'Success!');
            this.assignedPlatform();
          } else {
              console.log('get country list error: ', response.responseDetails);
          }
      },
      error => {
          console.log('get country list error: ', error);
      }
  );
  }

  /**
   * Get Platform data
   * @param data Assigned Platform Data
   */
  getTotalFollowers(platformAssign){
    var total = 0;
    var platform = {}
    var engagement = {}
    var total_engagement = 0

    for(let i = 0; i< platformAssign.length; i++){
      total_engagement += platformAssign[i].youtube? parseInt(platformAssign[i].youtube) : 0
      if(platformAssign[i].followers >0){
        platform[platformAssign[i].Platform.class_name] = platformAssign[i].followers
        engagement[platformAssign[i].Platform.class_name] = platformAssign[i].engagement_ratio
        total += platformAssign[i].followers

      }else{
        platform[platformAssign[i].Platform.class_name] = 0
        engagement[platformAssign[i].Platform.class_name] = 0
      }
    }
    this.setDounatTotalFollow( Object.values(platform), Object.keys(platform), total)
    console.log(platform)
    console.log(engagement)
    this.totalFollowers = total 
  }

  /**
   * Save Generated Price
   */
  saveGeneratedPrice(data){

    this.platformService.savePrice(data.value, 'gp').subscribe(
        (response: any) => {
            //console.log(response);
            
            if (response.responseCode === 200) {
              this.editGeneratePrice = false;
              this.toastr.success(response.responseDetails, 'Success!');
              this.assignedPlatform();
            } else {
                console.log('get country list error: ', response.responseDetails);
            }
        },
        error => {
            console.log('get country list error: ', error);
        }
    );
  }

  /**
   * Medthod to open modal function
   * @param template
   * @param classR
   */
  openModal(template: TemplateRef<any>, classR: string) {
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: "customModal " + classR })
    );
  }

  /**
   * Method to close modal
   */
  closeModal() {
    this.modalService._hideModal(1);
  }
}
