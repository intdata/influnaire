import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

// services
import { NotificationsService } from './../services/notifications.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  public notificationArr: Array<any> = [];
  public checkdLength: number = 0;
  public notificationPaginationConfig: any = {
    itemsPerPage: 1,
    currentPage: 1,
    totalItems: 0
  };
  constructor(
    private toastr: ToastrService,
    private notificationsService: NotificationsService
  ) {}

  ngOnInit() {
    this.getNotifications();
  }

  /**
   * Method to get all notifications
   */
  getNotifications(){
    let conditions = {
      receiverType: 'Influencer'
    }
    // call service to get notifications
    this.notificationsService.getNotificationList(conditions).subscribe(
        (response: any) => {
            //console.log(response);
            
            if (response.responseCode === 200) {
                this.notificationArr = response.notifications;
                this.notificationPaginationConfig = {
                  itemsPerPage: 1,
                  currentPage: 1,
                  totalItems: this.notificationArr.length
                };
                this.staticValueSet();
            } else {
                this.toastr.info(response.responseDetails, 'Info!');
            }
        },
        error => {
            this.toastr.error('Something went wrong.', 'Error!');
        }
    );
  }

  /**
   * Method to reload the page
   */
  refreshPage(){
      this.getNotifications();
      this.checkdLength = 0;
  }

  /**
   * Method to set initial checked value to false
   */
  staticValueSet() {
    for (let i = 0; i < this.notificationArr.length; i++) {
      this.notificationArr[i].checked = false;
    }
  }

  /**
   * Method to check a checkbox
   * @param indx 
   */
  checkCheckbox(indx) {
    !this.notificationArr[indx].checked;
    this.checkdLength = this.notificationArr.filter(x => x.checked === true).length;
  }

  /**
   * Method to check checkbox for all
   * @param _event 
   */
  checkAllCheckbox(_event) {
    this.notificationArr.map(i=>{
      i.checked = _event.target.checked;
    })
    this.checkdLength = this.notificationArr.filter(x => x.checked === true).length;
  }

  /**
   * Method to delete notifications
   */
  trashNotification(){
    let selectedNotifications = this.notificationArr.filter(v => v.checked === true).map((v, i)=>{ return v.id; });
    let data = {
      userType: 'Receiver',
      notificationIds: selectedNotifications
    }
    // call service to get notifications
    this.notificationsService.deleteNotification(data).subscribe(
        (response: any) => {
            //console.log(response);
            
            if (response.responseCode === 200) {
                this.toastr.success(response.responseDetails, 'Success!');
                this.refreshPage();
            } else {
                this.toastr.info(response.responseDetails, 'Info!');
            }
        },
        error => {
            this.toastr.error('Something went wrong.', 'Error!');
        }
    );
    
    this.checkdLength = 0;
  }

  /**
   * Method to change page no
   * @param event 
   * @param type 
   */
  notificationChangePage(event: number){ 
      this.notificationPaginationConfig.currentPage = event;
  }

}
