import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchPipe } from "./search.pipe";
import { SearchMultiPipe } from "./search-multi.pipe";
import { ReplaceLineBreaks } from "./replace-lineb-breaks.pipe";
import { DateTimeFormatPipe } from "./date-time-format.pipe";

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        SearchPipe,
        SearchMultiPipe,
        ReplaceLineBreaks,
        DateTimeFormatPipe
    ],
    exports:[
        SearchPipe,
        SearchMultiPipe,
        ReplaceLineBreaks,
        DateTimeFormatPipe
    ]
})
export class SharedPipesModule { }
