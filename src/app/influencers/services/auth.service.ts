import { Injectable, Inject } from '@angular/core';

@Injectable()
export class AuthService {

  constructor() {}

  isLoggedIn(){
    let isLoggedIn = localStorage.getItem("isLoggedin");   
    return isLoggedIn == "true" ? true : false;
  }
  
}
