import { Injectable, Inject } from '@angular/core';
import { Constant } from './../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';

declare var $: any;

@Injectable()
export class CountryService {
  private ALL_COUNTRIES_GET_URL:string;
  constructor(
    private httpClient: HttpClient,
  ) {
    this.ALL_COUNTRIES_GET_URL = '/get-countries';
  }

  /**
   * Service used to get all countries
   * @returns {Observable<any>}
   * @memberof CountryService
   */
  getAllCountries(): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY
      },
      url: Constant.API_BASE_URL + this.ALL_COUNTRIES_GET_URL,
    };
    return this.httpClient.get<any>(
      request.url,
      { headers: request.headers }
    );
  }
}
