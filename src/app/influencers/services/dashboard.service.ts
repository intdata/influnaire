import { Injectable, Inject } from '@angular/core';
import { Constant } from './../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";

declare var $: any;

@Injectable()
export class DashboardService {
  constructor(
    private httpClient: HttpClient,
    private cryptoProvider: CryptoProvider
  ) {
  }

  /**
   * Service used to get all countries
   * @returns {Observable<any>}
   * @memberof CountryService
   */
  getData(monthYear: any =''): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + '/influncer/dashboard',
      params: {
        monthYear: monthYear
      }
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }
}
