import { Injectable, Inject } from '@angular/core';
import { Constant } from './../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';
import { FormGroup } from '@angular/forms';
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";

@Injectable()
export class HelpCenterService {
  private ALL_HELP_GET_URL:string;
  private TALK_TO_US_POST_URL:string;
  constructor(
    private httpClient: HttpClient,
    private cryptoProvider: CryptoProvider
  ) {
    this.ALL_HELP_GET_URL = '/help-center/get-questtion-answer?type=influencer';
    this.TALK_TO_US_POST_URL = '/help-center/submit-talk-to-us';
  }

  /**
   * Service used to get all help question answer
   * @returns {Observable<any>}
   * @memberof HelpCenterService
   */
  getAllQuestionAnswer(lang: any): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY
      },
      url: Constant.API_BASE_URL + this.ALL_HELP_GET_URL + '&lang=' + lang,
    };
    return this.httpClient.get<any>(
      request.url,
      { headers: request.headers }
    );
  }

  /**
   * Service used to send talk to us form data
   * @returns {Observable<any>}
   * @memberof HelpCenterService
   */
  talkToUsSubmit(talkToUsFormData: FormGroup): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + this.TALK_TO_US_POST_URL,
      params: talkToUsFormData
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }
}
