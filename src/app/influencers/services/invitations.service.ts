import { Injectable, Inject } from '@angular/core';
import { Constant } from './../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';
import { FormGroup } from '@angular/forms';
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";

@Injectable()
export class InvitationsService {
  private ALL_INVITATIONS_POST_URL:string;
  private GET_INVITATION_DETAILS_BY_ID_POST_URL:string;
  private UPDATE_INVITATION_DETAILS_BY_ID_POST_URL:string;
  constructor(
    private httpClient: HttpClient,
    private cryptoProvider: CryptoProvider
  ) {
    this.ALL_INVITATIONS_POST_URL = '/invitation/get-all-invitation';
    this.GET_INVITATION_DETAILS_BY_ID_POST_URL = '/invitation/get-invitation-by-id';
    this.UPDATE_INVITATION_DETAILS_BY_ID_POST_URL = '/invitation/update-invitation-by-id';
  }

  /**
   * Service used to get all invitation 
   * for influnaire from brand owner
   * @returns {Observable<any>}
   * @memberof InvitationsService
   */
  getInvitationList(): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + this.ALL_INVITATIONS_POST_URL,
      params: {}
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }

  /**
   * Service used to get invitation details by id
   * @returns {Observable<any>}
   * @memberof InvitationsService
   */
  getInvitationDetails(invitationId): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + this.GET_INVITATION_DETAILS_BY_ID_POST_URL,
      params: { invitationId: invitationId }
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }

  
  /**
   * Service used to get invitation details by id
   * @param condition 
   * @returns {Observable<any>}
   * @memberof InvitationsService
   */
  updateInvitation(condition): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + this.UPDATE_INVITATION_DETAILS_BY_ID_POST_URL,
      params: condition
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }

}
