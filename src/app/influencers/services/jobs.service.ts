import { Injectable, Inject } from '@angular/core';
import { Constant } from './../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';
import { FormGroup } from '@angular/forms';
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";

@Injectable()
export class JobsService {
  private ALL_JOBS_POST_URL:string;
  constructor(
    private httpClient: HttpClient,
    private cryptoProvider: CryptoProvider
  ) {
    this.ALL_JOBS_POST_URL = '/job/get-all-jobs';
  }

  /**
   * Service used to get all job 
   * for influnaire from brand owner
   * @returns {Observable<any>}
   * @memberof InvitationsService
   */
  getJobList(type = 'current', platformId = '', monthYear = ''): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + this.ALL_JOBS_POST_URL,
      params: {
        type:type, 
        platformId:platformId,
        monthYear: monthYear
      }
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }

  /**
   * Get single job by id
   */
  getJobdetail(job_id): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + '/job/get-job-by-id',
      params: {job_id:job_id}
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }
  
  /**
   * get platform List
   */
  getCampaignAssignStatus(campaign, influncer){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{campaign:campaign, influncer:influncer},
      url: Constant.API_BASE_URL + '/campaign-assign/status'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }

}
