import { Injectable, Inject } from '@angular/core';
import { Constant } from './../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';
import { FormGroup } from '@angular/forms';
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";

@Injectable()
export class PlatformService {
  private ALL_PLATFORM_GET_URL:string;
  constructor(
    private httpClient: HttpClient,
    private cryptoProvider: CryptoProvider
  ) {
    this.ALL_PLATFORM_GET_URL = '/platform/list';
  }

  /**
   * Service used to get platform List
   * @returns {Observable<any>}
   * @memberof PlatformService
   */
  getPlatforms(){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + this.ALL_PLATFORM_GET_URL
    };
    return this.httpClient.get<any>(
      request.url,
      { headers: request.headers }
    );
  }

  /**
   * get platform List
   */
  getMapedPlatforms(){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{},
      url: Constant.API_BASE_URL + '/platform/getDist'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }

  /**
   * Get platform of currently loggedin influncer
   */
  getInfluncerPlatform(){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{},
      url: Constant.API_BASE_URL + '/platform/logged-in-influncer-platform'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }
  /**
   * Update platform price
   */
  savePrice(data, type){
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:{'data': data, 'type': type},
      url: Constant.API_BASE_URL + '/platform/save-price'
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers }
    );
  }
}
