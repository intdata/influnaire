import { Injectable, Inject } from '@angular/core';
import { Constant } from './../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';
import { FormGroup } from '@angular/forms';
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";

@Injectable()
export class ProposalService {
  private SUBMIT_PROPOSAL_POST_URL:string;
  constructor(
    private httpClient: HttpClient,
    private cryptoProvider: CryptoProvider
  ) {
    this.SUBMIT_PROPOSAL_POST_URL = '/proposal/submit-proposal';
  }

  /**
   * Service used to post submit info
   * @returns {Observable<any>}
   * @param submitProposalForm
   * @memberof ProposalService
   */
  submitProposalInfo(submitProposalForm: FormGroup): Observable<any> {
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + this.SUBMIT_PROPOSAL_POST_URL,
      params: submitProposalForm
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }

}
