import { Injectable, Inject } from '@angular/core';
//import { AppConst } from '../app.constants';
import { Constant } from './../../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';

import { CryptoProvider } from "./../../shared/crytoEncrypt.service";
import { FormGroup } from '@angular/forms';

declare var $: any;

@Injectable()
export class UserService {
  private INFLUENCER_INFO_URL: string;
  private INFLUENCER_CHANGE_PASSWORD_URL: string;
  private INFLUENCER_CHANGE_PERSONAL_INFO_URL: string;
  private INFLUENCER_CHANGE_DETAILS_INFO_URL: string;
  private INFLUENCER_CONNECT_YOUTUBE_URL: string;
  private UPDATE_FACEBOOK_DATA_COUNT_URL: string;
  constructor(
    private httpClient: HttpClient,
    private cryptoProvider: CryptoProvider
  ) {
    this.INFLUENCER_INFO_URL = '/users/get-influencer-by-id';
    this.INFLUENCER_CHANGE_PASSWORD_URL = '/users/change-influencer-password';
    this.INFLUENCER_CHANGE_PERSONAL_INFO_URL = '/users/change-influencer-personal-info';
    this.INFLUENCER_CHANGE_DETAILS_INFO_URL = '/users/change-influencer-details-info';
    this.INFLUENCER_CONNECT_YOUTUBE_URL = '/google_connect/youtube';
    this.UPDATE_FACEBOOK_DATA_COUNT_URL = '/users/facebookDataCount';
    this.INFLUENCER_CONNECT_YOUTUBE_URL = '/users/youtube-api';
  }

  /**
   * Service used to post influncer sign up form
   *
   * @returns {Observable<any>}
   * @memberof UserService
   */
  getInfluencerInfo(): Observable<any> {
    
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + this.INFLUENCER_INFO_URL,
      params: ''
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }
  
  /**
   * Method to change personal info
   * @param changePersonalInfoForm 
   */
  changePersonalInfo(changePersonalInfoForm: FormGroup): Observable<any> {
    
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + this.INFLUENCER_CHANGE_PERSONAL_INFO_URL,
      params: changePersonalInfoForm
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }

  /**
   * Method to change personal info
   * @param changeDetailsInfoForm 
   */
  changeDetailsInfo(changeDetailsInfoForm: FormGroup): Observable<any> {
    
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + this.INFLUENCER_CHANGE_DETAILS_INFO_URL,
      params: changeDetailsInfoForm
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }
  /**
   * Method to chage password
   * @param changePasswordForm 
   */
  changePassword(changePasswordForm: FormGroup): Observable<any> {
    
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      url: Constant.API_BASE_URL + this.INFLUENCER_CHANGE_PASSWORD_URL,
      params: changePasswordForm
    };
    return this.httpClient.post<any>(
      request.url,
      request.params,
      { headers: request.headers }
    );
  }

  /**
   * Service used to connect with youtube
   *
   * @returns {Observable<any>}
   * @memberof UserService
   */
  connectYoutube(data={}): Observable<any> {
    
    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:data,
      url: Constant.API_BASE_URL + this.INFLUENCER_CONNECT_YOUTUBE_URL,
    };
    return this.httpClient.get<any>(
      request.url,
      { headers: request.headers }
    );
  }

  connectFacebook(data={}) : Observable<any> {

    const request = {
      headers: {
        apikey: Constant.APIKEY,
        authorization: 'Bearer ' + this.cryptoProvider.decrypt(localStorage.getItem('authToken'))
      },
      params:data,
      url: Constant.API_BASE_URL + this.UPDATE_FACEBOOK_DATA_COUNT_URL
    };
    return this.httpClient.post<any>(
      request.url,
      request.params, 
      { headers: request.headers, params:{} }
    );
  }
}
