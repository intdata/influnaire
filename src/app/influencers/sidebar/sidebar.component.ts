import { Component, OnInit, HostBinding, Input } from '@angular/core';
import { CryptoProvider } from "./../../shared/crytoEncrypt.service";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @HostBinding('class.is-open')
  isOpen = false;
  hide(){
    this.isOpen = false;
    // this.change.emit(this.isOpen);
     }

  constructor(
    private cryptoProvider: CryptoProvider
  ) { }

  ngOnInit() {
    this.cryptoProvider.change.subscribe(isOpen => {
      this.isOpen = isOpen;
    });
  }

}
