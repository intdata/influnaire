import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from './../../../influencers/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardLogin implements CanActivate {
  constructor(
    private authService: AuthService, 
    private router: Router,
    private toastr: ToastrService,
  ){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean | UrlTree {
      if(!this.authService.isLoggedIn()){
        //this.toastr.success("Welcome to Influencer Dashboard", 'Success!');
        return true;
      }
      else{
        //this.toastr.error('You are already logged in.', 'Error!');
        return this.router.parseUrl("/influencers");
      }
  }
}
