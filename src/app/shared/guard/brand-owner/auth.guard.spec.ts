import { TestBed, async, inject } from '@angular/core/testing';

import { AuthGuardBrandOwner } from './auth.guard';

describe('AuthGuardBrandOwner', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthGuardBrandOwner]
    });
  });

  it('should ...', inject([AuthGuardBrandOwner], (guard: AuthGuardBrandOwner) => {
    expect(guard).toBeTruthy();
  }));
});
