import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { AuthServiceBrandOwner } from './../../../brand-owner/services/auth.service';
import { ToastrService } from 'ngx-toastr';


@Injectable({
  providedIn: 'root'
})
export class DeactivateGuard implements CanDeactivate<any>{
  constructor(
    private toastr:ToastrService,
    private authService: AuthServiceBrandOwner,
    ){

  }
  
  canDeactivate(component: any) {
    return this.authService.isBlock()
    
  }
}