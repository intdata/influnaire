import { Injectable } from '@angular/core';
import { Constant } from './../constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SiteSettingsService {

  constructor(private httpClient: HttpClient) { }

  getSettings(): Observable<any>{
    const request = {
      headers: {
      },
      url: Constant.API_BASE_URL + '/setting/settingList'
    };
    return this.httpClient.get<any>(
      request.url, 
      { headers: request.headers}
    );
  }
}
