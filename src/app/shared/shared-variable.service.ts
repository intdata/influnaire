import {
  Component,
  Injectable,
  Input,
  Output,
  EventEmitter
} from "@angular/core";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class SharedVariable {
  loggedinUserInfo = new BehaviorSubject<any>(0);
  loggedinUserInfoListender = this.loggedinUserInfo.asObservable();

  public getCampaingProgress= {'DRAFT':0, 'ANNOUNCED':33,'IN_PROGRESS':67,'PUBLISHED':100}

  //@Output() cartCountEmit: EventEmitter<any> = new EventEmitter();

  constructor() {
    //console.log('shared service started');
  }

  // changeCartCount(cartCount) {
  //   //console.log('cart items', cartCount);
  //   this.cartCountEmit.emit(cartCount);
  // }

  // getCartCount() {
  //   return this.cartCountEmit;
  // }

  /** 
   * Share the campaign status for components in(%)
  */
  // getCampaingProgress(){
  //   return 
  // }

  updateLoggedinUserInfo(data: any) {
    this.loggedinUserInfo.next(data);
  }

}
